<?php

/**
 * @file
 * Functions for rules integration.
 */

use Drupal\wirecard\Client\Exception\WCBackendOperationException;
use Drupal\wirecard\Client\Exception\WCErrorException;
use Drupal\wirecard\Client\Exception\WCExceptionBase;
use \Drupal\wirecard\Controller\WCPaymentControllerInterface;

/**
 * Implements hook_rules_action_info().
 */
function wirecard_rules_rules_action_info() {
  $defaults = array(
    'access callback' => 'wirecard_rules_payment_method_update_access',
    'group' => t('Wirecard'),
    'parameter' => array(
      'payment' => array(
        'type' => 'payment',
        'label' => t('Payment'),
        'description' => t('Payment entity.'),
      ),
    ),
  );
  $actions = array(
    'wirecard_deposit' => $defaults + array(
      'label' => t('Deposit'),
      'base' => 'wirecard_rules_deposit',
    ),
    'wirecard_approve_reversal' => $defaults + array(
      'label' => t('Approve reversal'),
      'base' => 'wirecard_rules_approve_reversal',
    ),
  );

  return $actions;
}

/**
 * Payment method integration access callback.
 */
function wirecard_rules_payment_method_update_access($type, $name) {
  return payment_method_access('update');
}

/**
 * Rules action callback; causes an approval to be converted into a deposit.
 */
function wirecard_rules_deposit(\Payment $payment) {
  $controller = payment_method_controller_load($payment->method->controller->name);
  if (!$controller instanceof WCPaymentControllerInterface) {
    return;
  }
  /* @var $client \Drupal\wirecard\Client\WCSeamlessAPIClient */
  $client = $controller->getClient($payment->method);
  $order_number = $controller->getConnector($payment)->getOrderNumber();

  try {
    $client->deposit($order_number, $payment->totalAmount(TRUE), $payment->currency_code);
    $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
    entity_save('payment', $payment);
  }
  catch (WCBackendOperationException $e) {
    drupal_set_message(t($e->getLogMessage(), $e->getLogContext()), 'error');
    $controller->getLogger()->error($e->getLogMessage(), $e->getLogContext());
    throw new RulesEvaluationException('Unable to converted approval into a deposit": ' . $e->getMessage());
  }
  catch (WCErrorException $e) {
    drupal_set_message(t($e->getLogMessage(), $e->getLogContext()), 'error');
    $controller->getLogger()->error($e->getLogMessage(), $e->getLogContext());
    throw new RulesEvaluationException('Unable to converted approval into a deposit": ' . $e->getMessage());
  }
  catch (WCExceptionBase $e) {
    $controller->getLogger()->error($e->getLogMessage(), $e->getLogContext());
    throw new RulesEvaluationException('Unable to converted approval into a deposit": ' . $e->getMessage());
  }
}

/**
 * Rules action callback; cancels payment approval.
 */
function wirecard_rules_approve_reversal(\Payment $payment) {
  $controller = payment_method_controller_load($payment->method->controller->name);
  if (!$controller instanceof WCPaymentControllerInterface) {
    return;
  }
  /* @var $client \Drupal\wirecard\Client\WCSeamlessAPIClient */
  $client = $controller->getClient($payment->method);
  $order_number = $controller->getConnector($payment)->getOrderNumber();

  try {
    $client->approveReversal($order_number);
    $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_CANCELLED));
    entity_save('payment', $payment);
  }
  catch (WCBackendOperationException $e) {
    drupal_set_message(t($e->getLogMessage(), $e->getLogContext()), 'error');
    $controller->getLogger()->error($e->getLogMessage(), $e->getLogContext());
    throw new RulesEvaluationException('Unable to cancel payment approval": ' . $e->getMessage());
  }
  catch (WCErrorException $e) {
    drupal_set_message(t($e->getLogMessage(), $e->getLogContext()), 'error');
    $controller->getLogger()->error($e->getLogMessage(), $e->getLogContext());
    throw new RulesEvaluationException('Unable to cancel payment approval": ' . $e->getMessage());
  }
  catch (WCExceptionBase $e) {
    $controller->getLogger()->error($e->getLogMessage(), $e->getLogContext());
    throw new RulesEvaluationException('Unable to cancel payment approval": ' . $e->getMessage());
  }
}
