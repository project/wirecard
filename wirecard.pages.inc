<?php

/**
 * @file
 * Wirecard pages.
 */

/**
 * Menu callback; Wirecard checkout finish.
 */
function _wirecard_finish($state, $pid) {
  $payment = entity_load_single('payment', $pid);
  return $payment->method->controller->finishPage($payment);
}

/**
 * Menu callback: Confirm page.
 */
function _wirecard_confirm_page($state, $pid) {
  $payment = entity_load_single('payment', $pid);
  return $payment->method->controller->confirmPage($payment);
}

/**
 * Form callback; builds wirecard mapping settings form.
 */
function _wirecard_mapping_form(array $form, array &$form_state) {
  $form['#tree'] = TRUE;
  $form['payment_contexts'] = array(
    '#type' => 'vertical_tabs',
  );

  foreach (wirecard_connector_info() as $connector_name => $connector_info) {
    /* @var $handler Drupal\wirecard\ContextConnector\ContextConnectorFormInterface */
    $handler = new $connector_info['form_handler']($connector_name);
    $form[$connector_name] = array(
      '#type' => 'fieldset',
      '#title' => $connector_info['name'],
      '#group' => 'payment_contexts',
    );
    $form[$connector_name]['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide the data mapping for %connector.', array('%connector' => $connector_info['name'])),
      '#default_value' => variable_get("wirecard_connector_{$connector_name}_enabled", TRUE),
    );
    $form[$connector_name]['form'] = $handler->buildForm($form_state);
    $form[$connector_name]['#after_build'][] = '_wirecard_mapping_form_enabled_after_build';

    foreach (element_children($form[$connector_name]['form']) as $key) {
      $form[$connector_name]['form'][$key]['#states']['visible'] = array(
        ':input[name="' . $connector_name . '[enabled]"]' => array('checked' => TRUE),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * After build callback that disables required validation if needed.
 */
function _wirecard_mapping_form_enabled_after_build($element, &$form_state) {
  // If not enabled, make fields not required for validation.
  if (!empty($form_state['input']) && empty($element['enabled']['#value'])) {
    foreach (element_children($element['form']) as $key) {
      if (!empty($element['form'][$key]['#required'])) {
        $element['form'][$key]['#required'] = FALSE;
      }
    }
  }
  return $element;
}

/**
 * Form validate callback; validates wirecard mapping settings form.
 */
function _wirecard_mapping_form_validate(array $form, array &$form_state) {
  foreach (wirecard_connector_info() as $connector_name => $connector_info) {
    /* @var $handler Drupal\wirecard\ContextConnector\ContextConnectorFormInterface */
    $handler = new $connector_info['form_handler']($connector_name);
    $handler->validateForm($form[$connector_name], $form_state);
  }
}

/**
 * Form submit callback; submits wirecard mapping settings form.
 */
function _wirecard_mapping_form_submit(array $form, array &$form_state) {
  foreach (wirecard_connector_info() as $connector_name => $connector_info) {
    /* @var $handler Drupal\wirecard\ContextConnector\ContextConnectorFormInterface */
    $handler = new $connector_info['form_handler']($connector_name);
    $handler->submitForm($form[$connector_name], $form_state);
    variable_set("wirecard_connector_{$connector_name}_enabled", $form_state['values'][$connector_name]['enabled']);
  }
  drupal_set_message(t('The configuration options have been saved.'));
}
