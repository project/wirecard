<?php

/**
 * @file
 * Hooks documentation.
 */

/**
 * Defines connectors between Wirecard payment method and Drupal.
 *
 * @return array
 *   Keys are payment method context machine names, values are arrays with the
 *   following string elements:
 *   - name: The human readable connector name.
 *   - connector: Connector class.
 *   - form_handler: Class of connector form with mapping settings.
 */
function hook_wirecard_connector_info() {
  return array(
    'payment' => array(
      'name' => t('Payment'),
      'connector' => 'Drupal\wirecard\ContextConnector\PaymentContextConnector',
      'form_handler' => 'Drupal\wirecard\ContextConnector\PaymentContextConnectorForm',
    ),
  );
}

/**
 * Alters connectors between Wirecard payment method and Drupal.
 *
 * @param array $connectors_info
 *   Keys are payment method context machine names, values are arrays with the
 *   following string elements:
 *   - name: The human readable connector name.
 *   - connector: Connector class.
 *   - form_handler: Class of connector form with mapping settings.
 *
 * @see hook_wirecard_connector_info()
 */
function hook_payment_method_controller_info_alter(array &$connectors_info) {
  // Remvove a payment method connector.
  unset($connectors_info['payment_commerce']);

  // Replace 'payment' connector with another a custom one.
  $connectors_info['payment']['connector'] = 'FooPaymentContextConnector';
}
