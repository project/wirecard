# Wirecard Payment Integration

## General

The module provides integration between Wirecard payment service and the Drupal
Payment framework.

## Requirements

Payment module.

## Installation

* Install the module as well as all required modules.
  - Payment: https://www.drupal.org/project/payment
  - Entity token: https://www.drupal.org/project/entity
  - Token: https://www.drupal.org/project/token
* Install the modules usual, see http://drupal.org/node/895232 for further
  information.
* For usage with Drupal commerce or Ubercart, you have to install the
  respective Payment integration modules:

  - Drupal commerce: https://www.drupal.org/project/payment_commerce
  - Ubercart: https://www.drupal.org/project/payment_ubercart

## Configuration

* You can configure Wirecard payment methods at
  'admin/config/services/payment/method'.
* You can customize the data sent to Wirecard via the data mappings configured
  at 'admin/config/services/payment/wirecard'.

## Force demo mode

If you don't want not allow Wirecard payments in production mode on some
development instances of your website, you can force demo mode for all Wirecard
payment methods by setting the variable 'wirecard_force_demo_mode' to TRUE. For
example, you can do so by adding the following code to your settings.php:

  `$conf['wirecard_force_demo_mode'] = TRUE;`

## Automated tests

Note: This is only applicable for developers.
@todo: Running mink-based simpletests needs some special setup.

To run the automated tests, follow the following steps:

* @todo: Document how to allow for mink-based simpletests.
* Install composer development dependencies via `composer install`.
* Install selenium and launch it.
* Run simpletests as usual, see https://www.drupal.org/node/30036.
