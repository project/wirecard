<?php

/**
 * @file
 * Module file.
 */

/**
 * Payment status. Payment is approved and the funds are held.
 */
define('WIRECARD_PAYMENT_STATUS_APPROVED', 'wirecard_payment_status_approved');

/**
 * Payment status. Max retries reached.
 */
define('WIRECARD_PAYMENT_STATUS_MAX_RETRIES', 'wirecard_payment_status_max_retries');

// @todo Could we integrate with xautoload for optimized class loading.
/**
 * Autoloads and registers class.
 *
 * After registering this custom autoload function with, the following line
 * would cause the function to attempt to load the \Drupal\wirecard\Qux class
 * from /path/to/project/src/Qux.php: "new \Drupal\wirecard\Qux;".
 *
 * @param string $class The fully-qualified class name.
 *
 * @return void
 */
spl_autoload_register(function ($class) {
  // Project-specific namespace prefix.
  $prefix = 'Drupal\\wirecard\\';
  // Base directory for the namespace prefix.
  $base_dir = __DIR__ . '/src/';
  // Check if the class uses the namespace prefix.
  $len = strlen($prefix);
  if (strncmp($prefix, $class, $len) !== 0) {
    // If no, move to the next registered autoloader.
    return;
  }
  // Get the relative class name.
  $relative_class = substr($class, $len);
  // Replace the namespace prefix with the base directory, replace namespace
  // separators with directory separators in the relative class name, append
  // with '.php'.
  $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
  if (file_exists($file)) {
    // If the file exists, require it.
    require $file;
  }
});

/**
 * Implements hook_permission().
 */
function wirecard_permission() {
  return array(
    'administer wirecard' =>  array(
      'title' => t('Administer Wirecard'),
      'description' => t('Provides access to administer Wirecard settings.'),
    ),
    'view wirecard order data' =>  array(
      'title' => t('View Wirecard order data'),
      'description' => t('Allows viewing Wirecard order details such as the order number and gateway reference.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function wirecard_menu() {
  $items = array();

  $defaults = array(
    'title' => 'Wirecard checkout finish',
    'page arguments' => array(1, 2),
    'access callback' => '_wirecard_return_access',
    'access arguments' => array(1, 2, 3),
    'type' => MENU_CALLBACK,
    'file' => 'wirecard.pages.inc',
  );

  foreach (array('return', 'success', 'cancel', 'failure', 'service', 'pending') as $state) {
    $items["wirecard/$state/%/%"] = array(
      'page callback' => '_wirecard_finish',
    ) + $defaults;
  }

  $items['wirecard/confirm/%/%'] = array(
    'page callback' => '_wirecard_confirm_page',
  ) + $defaults;

  $items['admin/config/services/payment/wirecard'] = array(
    'title' => 'Wirecard data mapping',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_wirecard_mapping_form'),
    'access arguments' => array('administer wirecard'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'wirecard.pages.inc',
    'weight' => 20,
  );

  return $items;
}

/**
 * Implements hook_field_extra_fields().
 */
function wirecard_field_extra_fields() {
  $field['payment']['payment'] = array(
    'display' => array(
      'wirecard_order_number' => array(
        'label' => t('Wirecard order number'),
        'weight' => 0,
      ),
      'wirecard_gateway_reference_number' => array(
        'label' => t('Wirecard gateway reference number'),
        'weight' => 0,
      ),
    ),
  );

  return $field;
}

/**
 * Implements hook_payment_view_alter().
 */
function wirecard_payment_view_alter(&$build) {
  $payment = $build['#entity'];
  if (user_access('view wirecard order data')) {
    $build['wirecard_order_number'] = wirecard_order_number($payment);
    $build['wirecard_gateway_reference_number'] = wirecard_gateway_reference_number($payment);
  }
}

/**
 * Return a render array containing a Wirecard order number.
 *
 * @param Payment $payment
 *   The payment entity.
 *
 * @return array
 *   A render array containing a Wirecard order number.
 */
function wirecard_order_number(Payment $payment) {
  if (!empty($payment->context_data['wirecard_order_number'])) {
    return array(
      '#type' => 'item',
      '#title' => t('Wirecard order number'),
      '#markup' => check_plain($payment->context_data['wirecard_order_number']),
    );
  }
}

/**
 * Return a render array containing a Wirecard gateway reference number.
 *
 * @param Payment $payment
 *   The payment entity.
 *
 * @return array
 *   A render array containing a Wirecard gateway reference number.
 */
function wirecard_gateway_reference_number(Payment $payment) {
  if (!empty($payment->context_data['gateway_reference_number'])) {
    return array(
      '#type' => 'item',
      '#title' => t('Wirecard gateway reference number'),
      '#markup' => check_plain($payment->context_data['gateway_reference_number']),
    );
  }
}

/**
 * Implements hook_payment_status_info().
 */
function wirecard_payment_status_info() {
  return array(
    new PaymentStatusInfo(array(
      'parent' => PAYMENT_STATUS_PENDING,
      'status' => WIRECARD_PAYMENT_STATUS_APPROVED,
      'title' => t('Approved'),
    )),
    new PaymentStatusInfo(array(
      'parent' => PAYMENT_STATUS_CANCELLED,
      'status' => WIRECARD_PAYMENT_STATUS_MAX_RETRIES,
      'title' => t('Cancelled (Max retries reached)'),
    )),
  );
}

/**
 * Menu access callback; checks access to Wirecard checkout finish page.
 */
function _wirecard_return_access($state, $pid, $order_id) {
  /** @var Payment $payment */
  if ($payment = entity_load_single('payment', $pid)) {
    $controller = $payment->method->controller;
    if ($controller instanceof \Drupal\wirecard\Controller\WCPaymentControllerBase) {
      if ($payment->context_data['wirecard_orderIdent'] != $order_id) {
        return FALSE;
      }
      // For non confirm-URLs only allow access to the user who has initiated
      // the payment also. Confirm-Response are protected by Wirecard
      // finger-printing based on the secret, in addition.
      return $state == 'confirm' || $controller->getCurrentOrderIdent() == $order_id;
    }
  }
  return FALSE;
}

/**
 * Returns information about Wirecard payment method connectors.
 *
 * @param bool $enabled_only
 *   (optional) Whether to return enabled connectors only. Defaults to FALSE.
 *
 * @return array
 *   Keys are payment method context machine names, values are arrays with next
 *   string elements:
 *   - title (string): connector human name.
 *   - connector (string): connector class.
 *   - form_handler (string): class of connector form with mapping settings.
 */
function wirecard_connector_info($enabled_only = FALSE) {
  if (!$connectors_info = &drupal_static(__FUNCTION__)) {
    // Get all available connectors.
    $connectors_info = module_invoke_all('wirecard_connector_info');
    // Allow other module to alter Wirecard connectors.
    drupal_alter('wirecard_connector_info', $connectors_info);
  }
  if ($enabled_only) {
    $result = array();
    foreach ($connectors_info as $connector_name => $info) {
      if (variable_get("wirecard_connector_{$connector_name}_enabled", TRUE)) {
        $result[$connector_name] = $info;
      }
    }
    return $result;
  }
  else {
    return $connectors_info;
  }
}

/**
 * Implements hook_wirecard_connector_info().
 */
function wirecard_wirecard_connector_info() {
  $connectors = array(
    'payment' => array(
      'name' => t('Payment'),
      'connector' => 'Drupal\wirecard\ContextConnector\PaymentContextConnector',
      'form_handler' => 'Drupal\wirecard\ContextConnector\PaymentContextConnectorForm',
    ),
  );
  if (module_exists('payment_commerce')) {
    $connectors['payment_commerce'] = array(
      'name' => t('Drupal Commerce'),
      'connector' => 'Drupal\wirecard\ContextConnector\DrupalCommerceContextConnector',
      'form_handler' => 'Drupal\wirecard\ContextConnector\DrupalCommerceContextConnectorForm',
    );
  }
  if (module_exists('payment_ubercart')) {
    $connectors['payment_ubercart'] = array(
      'name' => t('Ubercart'),
      'connector' => 'Drupal\wirecard\ContextConnector\UbercartContextConnector',
      'form_handler' => 'Drupal\wirecard\ContextConnector\UbercartContextConnectorForm',
    );
  }
  return $connectors;
}

/**
 * Implements hook_payment_method_controller_info().
 */
function wirecard_payment_method_controller_info() {
  return array(
    'Drupal\wirecard\Controller\WCPaymentBancontactMistercash',
    'Drupal\wirecard\Controller\WCPaymentCreditCard',
    'Drupal\wirecard\Controller\WCPaymentCreditCardMoto',
    'Drupal\wirecard\Controller\WCPaymentEkonto',
    'Drupal\wirecard\Controller\WCPaymentEpayBg',
    'Drupal\wirecard\Controller\WCPaymentEps',
    'Drupal\wirecard\Controller\WCPaymentGiropay',
    'Drupal\wirecard\Controller\WCPaymentIdl',
    'Drupal\wirecard\Controller\WCPaymentPayolutionInstallment',
    'Drupal\wirecard\Controller\WCPaymentPayolutionInvoice',
    'Drupal\wirecard\Controller\WCPaymentMoneta',
    'Drupal\wirecard\Controller\WCPaymentMpass',
    'Drupal\wirecard\Controller\WCPaymentPaybox',
    'Drupal\wirecard\Controller\WCPaymentPaypal',
    'Drupal\wirecard\Controller\WCPaymentPoli',
    'Drupal\wirecard\Controller\WCPaymentPrzelewy24',
    'Drupal\wirecard\Controller\WCPaymentPsc',
    'Drupal\wirecard\Controller\WCPaymentQuick',
    'Drupal\wirecard\Controller\WCPaymentRatepayInstallment',
    'Drupal\wirecard\Controller\WCPaymentRatepayInvoice',
    'Drupal\wirecard\Controller\WCPaymentSepa',
    'Drupal\wirecard\Controller\WCPaymentSkrillDirect',
    'Drupal\wirecard\Controller\WCPaymentSkrillWallet',
    'Drupal\wirecard\Controller\WCPaymentSofort',
    'Drupal\wirecard\Controller\WCPaymentTatraPay',
    'Drupal\wirecard\Controller\WCPaymentTrustly',
    'Drupal\wirecard\Controller\WCPaymentVoucher',
  );
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 *
 * Gets payment method configuration.
 */
function wirecard_payment_method_load(array $entities) {
  foreach ($entities as $payment_method) {
    if (in_array($payment_method->controller->name, wirecard_payment_method_controller_info())) {
      $query = db_select($payment_method->controller->schema, 'pm')
        ->fields('pm')
        ->condition('pmid', $payment_method->pmid);
      $data = $query->execute()->fetchAssoc();
      if (!empty($data)) {
        $payment_method->controller_data = (array) $data;
        unset($payment_method->controller_data['pmid']);
        if (!empty($data['settings'])) {
          $payment_method->controller_data['settings'] = unserialize($data['settings']);
        }
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 *
 * Creates payment method configuration.
 */
function wirecard_payment_method_insert(PaymentMethod $payment_method) {
  if (in_array($payment_method->controller->name, wirecard_payment_method_controller_info())) {
    $payment_method->controller_data += $payment_method->controller->controller_data_defaults;
    $query = db_insert($payment_method->controller->schema);
    $values = array_merge($payment_method->controller_data, array(
      'pmid' => $payment_method->pmid,
    ));
    if (!empty($values['settings'])) {
      $values['settings'] = serialize($values['settings']);
    }
    $query->fields($values);
    $query->execute();
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 *
 * Saves payment method configuration.
 */
function wirecard_payment_method_update(PaymentMethod $payment_method) {
  if (in_array($payment_method->controller->name, wirecard_payment_method_controller_info())) {
    $query = db_update($payment_method->controller->schema);
    $values = array_merge($payment_method->controller_data, array(
      'pmid' => $payment_method->pmid,
    ));
    if (!empty($values['settings'])) {
      $values['settings'] = serialize($values['settings']);
    }
    $query->fields($values);
    $query->condition('pmid', $payment_method->pmid);
    $query->execute();
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 *
 * Deletes payment method configuration.
 */
function wirecard_payment_method_delete(PaymentMethod $payment_method) {
  if (in_array($payment_method->controller->name, wirecard_payment_method_controller_info())) {
    db_delete($payment_method->controller->schema)
      ->condition('pmid', $payment_method->pmid)
      ->execute();
  }
}

/**
 * Implements
 * PaymentMethodController::payment_method_configuration_form_elements_callback.
 */
function wirecard_payment_method_configuration_form_elements(array $form, array &$form_state) {
  return $form_state['payment_method']->controller->getConfigurationForm($form, $form_state);
}

/**
 * Implements form validate callback for
 * wirecard_payment_method_configuration_form_elements().
 */
function wirecard_payment_method_configuration_form_elements_validate(array $form, array &$form_state) {
  $form_state['payment_method']->controller->validateConfigurationForm($form, $form_state);
}

/**
 * Implements
 * PaymentMethodController::payment_configuration_form_elements_callback.
 */
function wirecard_payment_configuration_form_elements(array $form, array &$form_state) {
  return $form_state['payment']->method->controller->getPaymentForm($form, $form_state);
}

/**
 * Implements form validate callback for
 * wirecard_payment_configuration_form_elements().
 */
function wirecard_payment_configuration_form_elements_validate(array $form, array &$form_state) {
  $form_state['payment']->method->controller->validatePaymentForm($form, $form_state);
}


/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Adds additional post-submit to payment_form_payment_method form.
 */
function wirecard_form_payment_form_payment_method_alter(array &$form, array &$form_state) {
  $form['#submit'][] = '_wirecard_payment_method_submit';
}

/**
 * Form submit callback; submits Wirecard payment method form.
 */
function _wirecard_payment_method_submit(array $form, array &$form_state) {
  // Check if there are missing mappings.
  if (!empty($form_state['storage']['wirecard_missing_mappings'])) {
    // Check if we are at method add form.
    if (!empty($form_state['storage']['is_new'])) {
      drupal_set_message(t('This payment method requires some data mappings that are currently not provided. In order to use the payment method, you have to provide them first.'));
      // We can not add redirect during regular form because there is a redirect
      // in payment_form_payment_method_submit().
      $form_state['redirect'] = 'admin/config/services/payment/wirecard';
    }
  }
}

/**
 * Submit function that just rebuilds the form.
 */
function wirecard_payment_form_ajax_rebuild_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Configuration form ajax callback.
 */
function wirecard_payment_configuration_form_restriction_amounts($form, $form_state) {
  // Do not show drupal messages.
  drupal_get_messages(NULL, TRUE);
  return $form['controller_form']['restrictions'];
}

/**
 * Implements hook_element_info_alter().
 */
function wirecard_element_info_alter(&$elements) {
  // Add process callback to Wirecard handles javascript settings.
  $elements['payment_form_context']['#process'][] = '_wirecard_form_process_context';
}

/**
 * Element process callback; handles Wirecard javascript settings.
 */
function _wirecard_form_process_context(array $element, array &$form_state, array $form) {
  if (empty($form_state['payment']->method->controller->requiresSensitiveData)) {
    // Remove javascript settings for payment methods which do not require
    // data storage. We should do this here general for all the methods, not
    // only Wirecard.
    // @see WCPaymentControllerBase::getPaymentForm()
    $element['#attached']['js'][] = array(
      'data' => array(
        'wirecardMethodData' => array(),
      ),
      'type' => 'setting',
    );
  }
  return $element;
}
