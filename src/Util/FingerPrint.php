<?php
/**
 * @file
 * Contains \Drupal\wirecard\Util\FingerPrrint
 */

namespace Drupal\wirecard\Util;

use Drupal\wirecard\Client\Response\FingerPrintedResponseInterface;
use Drupal\wirecard\Client\Exception\ResponseException;
use Drupal\wirecard\Client\WCClientConfig;

/**
 * Helpers for dealing with HTTP message fingerprinting.
 *
 * If we could use traits, this should be one.
 */
class FingerPrint {

  /**
   * Verifies the fingerprint and throws an exception if it's invalid.
   *
   * @param WCClientConfig $config
   *   The wirecard config.
   *
   * @throws ResponseException
   */
  public static function verifyFingerPrint(FingerPrintedResponseInterface $response, WCClientConfig $config) {
    $property_names = explode(',', $response->responseFingerprintOrder);

    if (!in_array('secret', $property_names)) {
      throw ResponseException::create("Unsecure fingerprint orderring requested.", $response);
    }

    if (static::calculateFingerPrint($property_names, $response, $config) != $response->responseFingerprint) {
      throw ResponseException::create("Error checking the fingerprint of the response.", $response);
    }
  }

  /**
   * Calculates a fingerprint given the properties and some order.
   *
   * @param string[] $property_names
   *   The fingerprinted properties.
   * @param stdClass $data
   *   The object to read the fingerprinted data from.
   * @param WCClientConfig $config
   *   The wirecard config.
   *
   * @return string
   *   The fingerprint.
   */
  public static function calculateFingerPrint($property_names, $data, WCClientConfig $config) {
    $seed = '';
    foreach ($property_names as $name) {
      $seed .= $name != 'secret' ? (isset($data->{$name}) ? $data->{$name} : '') : $config->{$name};
    }
    return hash("sha512", $seed);
  }

}
