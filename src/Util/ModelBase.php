<?php

/**
 * @file
 * Contains Drupal\wirecard\Util\Modelbase
 */

namespace Drupal\wirecard\Util;

/**
 * Base class for model classes.
 */
abstract class ModelBase {

  /**
   * Constructs the object.
   *
   * @param array $values
   *   (optional) Initialize from an array of values.
   */
  public function __construct(array $values = array()) {
    // Set properties from config.
    foreach ($values as $key => $value) {
      if (property_exists(get_called_class(), $key)) {
        $this->{$key} = $value;
      }
      else {
        $key = check_plain($key);
        throw new \InvalidArgumentException("Unknown key $key for request class " . get_called_class());
      }
    }
  }

  /**
   * Returns an array representation of the model.
   *
   * @return array
   */
  public function toArray() {
    return get_object_vars($this);
  }

  /**
   * Convert string to in camel-case, useful for class name patterns.
   *
   * @param $string
   *   Target string.
   *
   * @return string
   *   Camel-case string.
   */
  public static function toCamelCase($string){
    $string = str_replace('-', ' ', $string);
    $string = str_replace('_', ' ', $string);
    $string = ucwords(strtolower($string));
    $string = str_replace(' ', '', $string);
    return $string;
  }

}
