<?php
/**
 * @file
 * Contains \Drupal\wirecard\Util\WirecardMetadata
 */

namespace Drupal\wirecard\Util;

/**
 * Helper for dealing with plugin version string generation.
 *
 * If we could use traits, this should be one.
 */
class WirecardMetadata {

  /**
   * Get Wirecard module dependencies.
   *
   * @param \Payment $payment
   *   Payment entity.
   *
   * @return string[]
   *   Array with module versions keyed by module names.
   */
  public static function getWirecardDependencies(\Payment $payment) {
    $files = system_rebuild_module_data();
    $dependencies = array();
    foreach (array('wirecard', $payment->context) as $dependency) {
      $dependencies = array_merge($dependencies, static::getModuleDependencies($dependency));
    }
    // Always add our own module as well.
    $dependencies[] = 'wirecard';

    $modules = array();
    foreach ($dependencies as $module) {
      if (!empty($files[$module])) {
        $modules[$module] = !empty($files[$module]->info['version']) ? $files[$module]->info['version'] : 'dev';
      }
    }
    return $modules;
  }

  /**
   * Get all module dependencies.
   *
   * @param string $module
   *   Module name.
   *
   * @return string[]
   *   Dependency module names.
   */
  public static function getModuleDependencies($module) {
    $files = system_rebuild_module_data();
    $dependencies = array();
    if (!empty($files[$module]->requires)) {
      foreach ($files[$module]->requires as $required_module => $required_value) {
        $dependencies[$required_module] = $required_module;
      }
    }
    return $dependencies;
  }

  /**
   * Calculates a plugin version string given the payment entity.
   *
   * @param \Payment $payment
   *   Payment entity.
   *
   * @return string
   *   Computed plugin version string of the used modules.
   */
  public static function generatePluginVersion(\Payment $payment) {
    $plugin_string = array();
    $dependencies = static::getWirecardDependencies($payment);

    // Reorder plugin version string. Shop modules should go in the first place.
    $shop_modules = array('uc_store', 'commerce', 'payment');
    foreach ($shop_modules as $shop_module) {
      if (!empty($dependencies[$shop_module])) {
        $plugin_string[] = $shop_module . ';' . $dependencies[$shop_module];
        unset($dependencies[$shop_module]);
      }
    }

    // Concat all the required modules using ";" symbol.
    foreach ($dependencies as $module => $version) {
      $plugin_string[] = $module . ';' . $version;
    }
    return base64_encode(implode(';', $plugin_string));
  }

}
