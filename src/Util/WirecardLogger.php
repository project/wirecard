<?php
/**
 * @file
 * Contains \Drupal\wirecard\Util\WirecardLogger.
 */

namespace Drupal\wirecard\Util;

/**
 * Class for logging Wirecard related actions.
 */
class WirecardLogger {

  const ERROR = 'error';
  const WARNING = 'warning';
  const NOTICE = 'notice';
  const INFO = 'info';
  const DEBUG = 'debug';

  /**
   * Whether debug mode is enabled.
   *
   * @var bool
   */
  protected $debugMode = FALSE;

  /**
   * Creates a logger with payment module debug mode configuration.
   *
   * @return static
   */
  public static function create() {
    return new static(variable_get('payment_debug', TRUE));
  }

  /**
   * Constructs the object.
   *
   * @param bool $debug_mode
   *   (optional) Whether to enable the debug mode.
   */
  public function __construct($debug_mode = FALSE) {
    $this->debugMode = $debug_mode;
  }

  /**
   * Returns the watchdog severity.
   */
  protected function getSeverity($log_level) {
    switch ($log_level) {
      default:
      case static::ERROR:
        return WATCHDOG_ERROR;

      case static::WARNING:
        return WATCHDOG_WARNING;

      case static::NOTICE:
        return WATCHDOG_NOTICE;

      case static::INFO:
        return WATCHDOG_INFO;

      case static::DEBUG:
        return WATCHDOG_DEBUG;
    }
  }

  /**
   * Returns the message type.
   */
  protected function getMessageType($log_level) {
    switch ($log_level) {
      case static::ERROR:
        return 'error';

      case static::WARNING:
        return 'warning';

      default:
      case static::INFO:
      case static::DEBUG:
      case static::NOTICE:
        return 'success';
    }
  }

  /**
   * Runtime errors that do not require immediate action but should typically
   * be logged and monitored.
   *
   * @param string $message
   *   The message to log.
   * @param array $context
   *   Context containing message replacements.
   * @param $link
   *   A link to associate with the log message.
   * @param bool $show_message
   *   (optional) Whether to show the log message on the website if debug mode
   *   is enabled. If debug mode is disabled, it will never be shown.
   */
  public function error($message, array $context = array(), $link = NULL, $show_message = TRUE) {
    $this->log(static::ERROR, $message, $context, $link, $show_message);
  }

  /**
   * Exceptional occurrences that are not errors.
   *
   * Example: Use of deprecated APIs, poor use of an API, undesirable things
   * that are not necessarily wrong.
   *
   * @param string $message
   *   The message to log.
   * @param array $context
   *   Context containing message replacements.
   * @param $link
   *   A link to associate with the log message.
   * @param bool $show_message
   *   (optional) Whether to show the log message on the website if debug mode
   *   is enabled. If debug mode is disabled, it will never be shown.
   */
  public function warning($message, array $context = array(), $link = NULL, $show_message = TRUE) {
    $this->log(static::WARNING, $message, $context, $link, $show_message);
  }

  /**
   * Normal but significant events.
   *
   * @param string $message
   *   The message to log.
   * @param array $context
   *   Context containing message replacements.
   * @param $link
   *   A link to associate with the log message.
   * @param bool $show_message
   *   (optional) Whether to show the log message on the website if debug mode
   *   is enabled. If debug mode is disabled, it will never be shown.
   */
  public function notice($message, array $context = array(), $link = NULL, $show_message = TRUE) {
    $this->log(static::NOTICE, $message, $context, $link, $show_message);
  }

  /**
   * Interesting events.
   *
   * @param string $message
   *   The message to log.
   * @param array $context
   *   Context containing message replacements.
   * @param $link
   *   A link to associate with the log message.
   * @param bool $show_message
   *   (optional) Whether to show the log message on the website.
   */
  public function info($message, array $context = array(), $link = NULL, $show_message = TRUE) {
    $this->log(static::INFO, $message, $context, $link, $show_message);
  }

  /**
   * Detailed debug information.
   *
   * @param string $message
   *   The message to log.
   * @param array $context
   *   Context containing message replacements.
   * @param $link
   *   A link to associate with the log message.
   * @param bool $show_message
   *   (optional) Whether to show the log message on the website.
   */
  public function debug($message, array $context = array(), $link = NULL, $show_message = TRUE) {
    if ($this->debugMode) {
      $this->log(static::DEBUG, $message, $context, $link, $show_message);
    }
  }

  /**
   * Logs with an arbitrary level.
   *
   * @param mixed $level
   *   The level to log.
   * @param string $message
   *   The message to log.
   * @param array $context
   *   Context containing message replacements.
   * @param $link
   *   A link to associate with the log message.
   * @param bool $show_message
   *   (optional) Whether to show the log message on the website if debug mode
   *   is enabled. If debug mode is disabled, it will never be shown.
   */
  public function log($level, $message, array $context = array(), $link = NULL, $show_message = TRUE) {

    if ($level == static::DEBUG && !$this->debugMode) {
      // Nothing to do.
      return;
    }

    // @see payment_debug()

    // Get the backtrace as fast as possible.
    if (version_compare(phpversion(), '5.3.6') == -1) {
      $backtrace = debug_backtrace(FALSE);
    }
    elseif (version_compare(phpversion(), '5.4.0') > -1) {
      $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 0);
    }
    else {
      $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    }
    while ($backtrace[0]['class'] == __CLASS__) {
      array_shift($backtrace);
    }
    // Distill the caller's file and line information from the backtrace.
    $caller = _drupal_get_last_caller($backtrace);

    $hide = l(t('Do not display Payment debugging messages'), 'admin/config/services/payment/global');

    $string = '@level: !message on line !line in @file (!hide).';
    $string_arguments = array(
      '!message' => format_string($message, $context),
      '!line' => $caller['line'],
      '@file' => $caller['file'],
      '!hide' => $hide,
      '@level' => drupal_ucfirst($level),
    );
    watchdog('Wirecard', $string, $string_arguments, $this->getSeverity($level), $link);

    if ($this->debugMode && $show_message) {
      drupal_set_message(t($string, $string_arguments), $this->getMessageType($level), FALSE);
    }
  }

}
