<?php
/**
 * @file
 * Contains \Drupal\wirecard\Util\DataMapping
 */

namespace Drupal\wirecard\Util;

/**
 * Helpers for dealing with Wirecard data mapping.
 */
class DataMapping {

  /**
   * Gets missing mappings from Wirecard connector for all payment controllers.
   *
   * Checks all the required parameters for every Wirecard payment controller
   * against Wirecard properties mapping.
   *
   * @return array[]
   *   Array of missing mappings:
   *   - Key (string): connector name.
   *   - Value (string[]): strings array with properties names that miss mapping.
   */
  public static function getMissingMappings(){
    $missing_mappings = array();
    // We need to get controllers of each created payment method.
    foreach ($payment_methods = entity_load('payment_method') as $payment_method) {
      $controller = $payment_method->controller;
      $reflector = new \ReflectionClass($controller);
      if ($reflector->isSubclassOf('Drupal\wirecard\Controller\WCPaymentControllerBase')) {
        /* @var  $controller \Drupal\wirecard\Controller\WCPaymentControllerBase */
        if ($mappings = $controller->getMissingMappings()) {
          // Merge missing mappings and missing mappings from controller.
          foreach ($mappings as $connector => $fields) {
            foreach ($fields as $field) {
              if (empty($missing_mappings[$connector][$field])) {
                $missing_mappings[$connector][$field] = $field;
              }
            }
          }
        }
      }
    }
    return $missing_mappings;
  }

  /**
   * Gets required mappings by Wirecard connector for all payment controllers.
   *
   * @param string $connector_name
   *   Connector name.
   *
   * @return array[]
   *   Array of required mappings:
   *   - Key (string): connector name.
   *   - Value (string[]): strings array with properties names that miss mapping.
   */
  public static function getRequiredMappings($connector_name) {
    $required_mappings = array();
    // We need to get controllers of each created payment method.
    foreach (entity_load('payment_method') as $payment_method) {
      $controller = $payment_method->controller;
      $reflector = new \ReflectionClass($controller);
      if ($reflector->isSubclassOf('Drupal\wirecard\Controller\WCPaymentControllerBase')) {
        /* @var  $controller \Drupal\wirecard\Controller\WCPaymentControllerBase */
        $required_mappings = array_merge($required_mappings, $controller->getRequiredMappings($connector_name));
      }
    }
    return $required_mappings;
  }

}
