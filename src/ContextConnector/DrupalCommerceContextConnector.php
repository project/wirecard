<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\DrupalCommerceContextConnector
 */

namespace Drupal\wirecard\ContextConnector;

use Drupal\wirecard\Client\Exception\GeneralException;

/**
 * Context connector if payment module is used stand-a-lone.
 */
class DrupalCommerceContextConnector extends ContextConnectorBase {

  /**
   * {@inheritdoc}
   *
   * Wirecard does not support retrying of payment with the same order number.
   * We need to add two zeros to the order number. We reserve them for payment
   * retries. Only 99 retries are allowed.
   *
   * @throws \Drupal\wirecard\Client\Exception\GeneralException
   */
  public function getOrderNumber() {
    if (empty($this->payment->context_data['order_number'])) {
      // Add two zeros to the order number. We reserve them for retries.
      // Wirecard doesn't support retrying payment with the same order number.
      $this->payment->context_data['order_number'] = $this->payment->context_data['order_id'] * 100;
      entity_save('payment', $this->payment);
    }
    // Allow only 99 retries.
    else if ((int) substr($this->payment->context_data['order_number'], -2) < 99) {
      // Increment order_number to retry payment.
      $this->payment->context_data['order_number']++;
      entity_save('payment', $this->payment);
    }
    // If max number of retries is reached...
    else {
      // set status to "Max retries reached".
      $this->payment->setStatus(new \PaymentStatusItem(WIRECARD_PAYMENT_STATUS_MAX_RETRIES));
      entity_save('payment', $this->payment);
      throw new GeneralException("The maximum number of retries for payment {$this->payment->pid} has been reached");
    }

    $this->payment->context_data['order_number'];
  }

  /**
   * {@inheritdoc}
   */
  protected function get($method) {
    $method_parts = explode('::get', $method);
    $property = lcfirst($method_parts[1]);
    // Statically cache all token replacements.
    if (!isset($this->data)) {
      foreach ($this->getFormHandler()->getSettings() as $key => $setting) {
        $order = commerce_order_load($this->payment->context_data['order_id']);
        $this->data[$key] = token_replace($setting, array('payment' => $this->payment, 'commerce-order' => $order), array('clear' => TRUE));
      }
    }
    return !empty($this->data[$property]) ? $this->data[$property] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketAmount() {
    $order = commerce_order_load($this->payment->context_data['order_id']);
    $total_value = reset($order->commerce_order_total);
    return $total_value[0]['amount'] / 100;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketCurrency() {
    $order = commerce_order_load($this->payment->context_data['order_id']);
    $total_value = reset($order->commerce_order_total);
    return $total_value[0]['currency_code'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItems() {
    $order = commerce_order_load($this->payment->context_data['order_id']);
    return count($order->commerce_line_items);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemArticleNumber($index) {
    $line_item = $this->getLineItem($index);
    return $line_item->line_item_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemDescription($index) {
    $line_item = $this->getLineItem($index);
    return $line_item->line_item_label;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemQuantity($index) {
    $line_item = $this->getLineItem($index);
    return $line_item->quantity;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemTax($index) {
    // @todo Implement Tax integration.
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemUnitPrice($index) {
    $line_item = $this->getLineItem($index);
    $price_value = reset($line_item->commerce_unit_price);
    return $price_value[0]['amount'] / 100;
  }

  /**
   * Gets line item by index from order.
   *
   * @param $index
   *   Line item index.
   *
   * @return \stdClass
   *   Line item object.
   */
  protected function getLineItem($index) {
    $order = commerce_order_load($this->payment->context_data['order_id']);
    $line_items = reset($order->commerce_line_items);
    $line_item = commerce_line_item_load($line_items[$index - 1]['line_item_id']);
    return $line_item;
  }

}
