<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\UbercartContextConnectorForm
 */

namespace Drupal\wirecard\ContextConnector;

/**
 * Configuration form for the ubercart connector.
 */
class UbercartContextConnectorForm extends ContextConnectorFormBase {

  /**
   * @inheritdoc
   */
  public static function getFormFields() {
    return array(
      'customerStatement' => array(
        'title' => t('Customer Statement'),
        'description' => t('Text displayed on invoice of financial institution of your consumer.'),
      ),
      'orderReference' => array(
        'title' => t('Order reference'),
        'description' => t('Unique order reference ID sent from merchant to financial institution.'),
      ),
      'consumerBillingFax' => array(
        'title' => t('Consumer billing fax'),
      ),
      'consumerShippingFax' => array(
        'title' => t('Consumer shipping fax'),
      ),
      'consumerBirthDate' => array(
        'title' => t('Consumer birth date'),
        'description' => t('Birth date of consumer in the format YYYY-MM-DD.'),
      ),
      'companyName' => array(
        'title' => t('Company name'),
      ),
      'companyVatId' => array(
        'title' => t('Company VAT id'),
      ),
      'companyTradeRegistryNumber' => array(
        'title' => t('Company trade registry number'),
        'description' => t('Trade registration number of company.'),
      ),
      'dueDate' => array(
        'title' => t('Due date'),
        'description' => t('Date when payment is debited from consumer´s bank account. The due date is calculated by merchant or, if the field is left empty, Wirecard will automatically calculate the due date. Date as "DD.MM.YYYY".'),
      ),
      'companyRegisterKey' => array(
        'title' => t('Company register key'),
        'type' => 'string',
        'description' => t('Additional or alternative register key of company.'),
      ),
      'creditorId' => array(
        'title' => t('Creditor id'),
        'type' => 'string',
        'description' => t('Unique identifier of creditor (merchant).'),
      ),
      'mandateId' => array(
        'title' => t('Mandate id'),
        'type' => 'string',
        'description' => t('Identifier of displayed mandate.'),
      ),
      'mandateSignatureDate' => array(
        'title' => t('Mandate signature date'),
        'type' => 'string',
        'description' => t('Date when mandate was signed. Date as "DD.MM.YYYY".'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getTokenHelp() {
    return array(
      '#theme' => 'token_tree',
      '#token_types' => array('payment', 'uc_order'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettingDefaults() {
    return array(
      'orderReference' => '[uc_order:order-id]',
    );
  }
}
