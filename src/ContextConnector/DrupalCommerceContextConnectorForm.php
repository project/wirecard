<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\DrupalCommerceContextConnector
 */

namespace Drupal\wirecard\ContextConnector;

/**
 * Configuration form for the drupal commerce connector.
 */
class DrupalCommerceContextConnectorForm extends ContextConnectorFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getTokenHelp() {
    return array(
      '#theme' => 'token_tree',
      '#token_types' => array('payment', 'commerce-order'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettingDefaults() {
    return array(
      'orderReference' => '[commerce-order:order-id]',
      'consumerBillingFirstname' => '[commerce-order:commerce-customer-billing:commerce_customer_address:first-name]',
      'consumerBillingLastname' => '[commerce-order:commerce-customer-billing:commerce_customer_address:last-name]',
      'consumerBillingAddress1' => '[commerce-order:commerce-customer-billing:commerce_customer_address:thoroughfare]',
      'consumerBillingAddress2' => '[commerce-order:commerce-customer-billing:commerce_customer_address:premise] [commerce-order:commerce-customer-billing:commerce_customer_address:sub-premise]',
      'consumerBillingCity' => '[commerce-order:commerce-customer-billing:commerce_customer_address:locality]',
      'consumerBillingState' => '[commerce-order:commerce-customer-billing:commerce_customer_address:administrative-area]',
      'consumerBillingCountry' => '[commerce-order:commerce-customer-billing:commerce_customer_address:country-code]',
      'consumerBillingZipCode' => '[commerce-order:commerce-customer-billing:commerce_customer_address:postal-code]',
      'consumerEmail' => '[commerce-order:owner:mail]',
    );
  }

}
