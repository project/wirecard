<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\UbercartContextConnector
 */

namespace Drupal\wirecard\ContextConnector;

use Drupal\wirecard\Client\WCClientConfig;
use Drupal\wirecard\Client\Exception\GeneralException;

/**
 * Context connector if payment module is used stand-a-lone.
 */
class UbercartContextConnector extends ContextConnectorBase {


  /**
   * Current Ubercart order.
   *
   * @var \stdClass
   */
  protected $order;

  /**
   * @inheritdoc
   */
  public function __construct(\Payment $payment, WCClientConfig $config, $connector_name) {
    parent::__construct($payment, $config, $connector_name);
    payment_ubercart_order_id_load($payment);
    $this->order = uc_order_load($payment->payment_ubercart_uc_order_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderNumber() {
    if (empty($this->payment->context_data['order_number']) && payment_ubercart_order_id_load($this->payment)) {
      // Initialize the order number with the number from ubercart.
      // Add two zeros to the order number. We reserve them for retries.
      // Wirecard doesn't support retrying payment with the same order number.
      $this->payment->context_data['order_number'] = $this->payment->payment_ubercart_uc_order_id * 100;
    }
    // Allow only 99 retries.
    else if ((int) substr($this->payment->context_data['order_number'], -2) < 99) {
      // Increment order_number to retry payment.
      $this->payment->context_data['order_number']++;
      entity_save('payment', $this->payment);
    }
    // If max number of retries is reached...
    else {
      // set status to "Max retries reached".
      $this->payment->setStatus(new \PaymentStatusItem(WIRECARD_PAYMENT_STATUS_MAX_RETRIES));
      entity_save('payment', $this->payment);
      throw new GeneralException("The maximum number of retries for payment {$this->payment->pid} has been reached");
    }

    $this->payment->context_data['order_number'];
  }

  /**
   * {@inheritdoc}
   */
  protected function get($method) {
    $method_parts = explode('::get', $method);
    $property = lcfirst($method_parts[1]);
    // Statically cache all token replacements.
    if (!isset($this->data)) {
      foreach ($this->getFormHandler()->getSettings() as $key => $setting) {
        payment_ubercart_order_id_load($this->payment);
        $order = uc_order_load($this->payment->payment_ubercart_uc_order_id);
        $this->data[$key] = token_replace($setting, array('payment' => $this->payment, 'uc_order' => $order), array('clear' => TRUE));
      }
    }
    return !empty($this->data[$property]) ? $this->data[$property] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerEmail() {
    return $this->order->primary_email;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingFirstname() {
    return $this->order->billing_first_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingLastname() {
    return $this->order->billing_last_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingAddress1() {
    return $this->order->billing_street1;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingAddress2() {
    return $this->order->billing_street2;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingCity() {
    return $this->order->billing_city;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingState() {
    return uc_get_zone_code($this->order->billing_zone);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingCountry() {
    $country_id = $this->order->billing_country;
    $country_data = uc_get_country_data(array('country_id' => $country_id));
    return $country_data[0]['country_iso_code_2'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingZipCode() {
    return $this->order->billing_postal_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingPhone() {
    return $this->order->billing_phone;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingFirstName() {
    return $this->order->delivery_first_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingLastName() {
    return $this->order->delivery_last_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingAddress1() {
    return $this->order->delivery_street1;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingAddress2() {
    return $this->order->delivery_street2;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingCity() {
    return $this->order->delivery_city;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingState() {
    return uc_get_zone_code($this->order->delivery_zone);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingCountry() {
    $country_id = $this->order->delivery_country;
    $country_data = uc_get_country_data(array('country_id' => $country_id));
    return $country_data[0]['country_iso_code_2'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingZipCode() {
    return $this->order->delivery_postal_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingPhone() {
    return $this->order->delivery_phone;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketAmount() {
    return $this->order->order_total;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketCurrency() {
    return $this->order->currency;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItems() {
    return count($this->order->products);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemArticleNumber($index) {
    $line_item = $this->getLineItem($index);
    return $line_item->nid;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemDescription($index) {
    $line_item = $this->getLineItem($index);
    return $line_item->title;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemQuantity($index) {
    $line_item = $this->getLineItem($index);
    return $line_item->qty;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemTax($index) {
    // @todo Implement Tax integration.
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemUnitPrice($index) {
    $line_item = $this->getLineItem($index);
    return number_format($line_item->price, 2);
  }

  /**
   * Gets line item by index from order.
   *
   * @param $index
   *   Line item index.
   *
   * @return \stdClass
   *   Line item object.
   */
  protected function getLineItem($index) {
    $line_items = array_values($this->order->products);
    $line_item = $line_items[$index - 1];
    return $line_item;
  }

}
