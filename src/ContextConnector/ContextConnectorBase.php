<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\ContextConnectorBase
 */

namespace Drupal\wirecard\ContextConnector;

use Drupal\wirecard\Client\WCClientConfig;
use Drupal\wirecard\Client\Exception\GeneralException;

/**
 * Base implementation.
 */
abstract class ContextConnectorBase implements ContextConnectorInterface {

  /**
   * The associated payment.
   *
   * @var \Payment
   */
  protected $payment;

  /**
   * The client config.
   *
   * @var WCClientConfig
   */
  protected $config;

  /**
   * Connector name.
   *
   * @var string
   */
  protected $connectorName;

  /**
   * The context connector form handler.
   *
   * @var ContextConnectorFormBase
   */
  protected $formHandler;

  /**
   * Tokens replacements data.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructs the object.
   *
   * @param \Payment $payment
   *   The associated payment object.
   * @param WCClientConfig $config
   *   Wirecard client configuration.
   * @param string $connector_name
   *   Connector name. Used to prepare variable name.
   */
  public function __construct(\Payment $payment, WCClientConfig $config, $connector_name) {
    $this->payment = $payment;
    $this->config = $config;
    $this->connectorName = $connector_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectorName() {
    return $this->connectorName;
  }

  /**
   * Helper method; gets property value from settings.
   *
   * @param string $method
   *   The called method name.
   *
   * @return string
   *   Property from settings.
   */
  protected function get($method) {
    $method_parts = explode('::get', $method);
    $property = lcfirst($method_parts[1]);
    // Statically cache all token replacements.
    if (!isset($this->data)) {
      foreach ($this->getFormHandler()->getSettings() as $key => $setting) {
        $this->data[$key] = token_replace($setting, array('payment' => $this->payment), array('clear' => TRUE));
      }
    }
    return !empty($this->data[$property]) ? $this->data[$property] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormHandler() {
    if (!isset($this->formHandler)) {
      $connector_info = wirecard_connector_info();
      $handler_class = $connector_info[$this->connectorName]['form_handler'];
      $this->formHandler = new $handler_class($this->connectorName);
    }
    return $this->formHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return $this->payment->totalAmount(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrency() {
    return $this->payment->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderDescription() {
    return $this->payment->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getSuccessUrl() {
    return url('wirecard/success/' . $this->payment->pid . '/' . $this->payment->context_data['wirecard_orderIdent'], array('absolute' => TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return url('wirecard/cancel/' . $this->payment->pid . '/' . $this->payment->context_data['wirecard_orderIdent'], array('absolute' => TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function getFailureUrl() {
    return url('wirecard/failure/' . $this->payment->pid . '/' . $this->payment->context_data['wirecard_orderIdent'], array('absolute' => TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function getPendingUrl() {
    return url('wirecard/pending/' . $this->payment->pid . '/' . $this->payment->context_data['wirecard_orderIdent'], array('absolute' => TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmUrl() {
    return url('wirecard/confirm/' . $this->payment->pid . '/' . $this->payment->context_data['wirecard_orderIdent'], array('absolute' => TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function getReturnUrl() {
    return url('wirecard/return/' . $this->payment->pid . '/' . $this->payment->context_data['wirecard_orderIdent'], array('absolute' => TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceUrl() {
    return url($this->config->serviceUrl, array('absolute' => TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerId() {
    return $this->config->customerId;
  }

  /**
   * {@inheritdoc}
   */
  public function getShopId() {
    return $this->config->shopId;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguage() {
    return isset($this->config->language) ? $this->config->language : $GLOBALS[LANGUAGE_TYPE_INTERFACE]->language;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerIpAddress() {
    return $_SERVER['REMOTE_ADDR'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerUserAgent() {
    return $_SERVER['HTTP_USER_AGENT'];
  }

  /**
   * {@inheritdoc}
   */
  public function getWindowName() {
    return 'wirecard';
  }

  /**
   * Handle getBasketItem...() calls.
   */
  public function __call($method, $args) {
    if (strpos($method, 'getBasketItem') !== FALSE) {
      $index = preg_match('/\d+/', $method, $match);
      $method_suffix = str_replace("getBasketItem$index", '', $method);
      $method_name = "getBasketItem{$method_suffix}";
      if (method_exists($this, $method_name)) {
        return $this->$method_name($index);
      }
    }

    throw new GeneralException('Call to undefined method ' . __CLASS__ . '::' . $method . '()');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\wirecard\Client\Exception\GeneralException
   */
  public function getIframeCssUrl() {
    if (!empty($this->payment->method->controller_data['settings']['pci3_dss_saq_enable'])) {
      if (!empty($this->payment->method->controller_data['settings']['pci3_iframe_css_url'])) {
        $css_url = $this->payment->method->controller_data['settings']['pci3_iframe_css_url'];
        $path = $GLOBALS['base_url'] . '/' . $css_url;
        if (!file_exists($css_url)) {
          throw new GeneralException("File $path does not exist");
        }
        if (pathinfo($path, PATHINFO_EXTENSION) != 'css') {
          throw new GeneralException("File $path should have a '.css' extension");
        }
        return $path;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getcreditcardShowCvcField() {
    if (!empty($this->payment->method->controller_data['settings']['pci3_dss_saq_enable'])) {
      return !empty($this->payment->method->controller_data['settings']['pci3_show_cvc']) ? 'true' : 'false';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreditcardShowCardholderNameField() {
    if (!empty($this->payment->method->controller_data['settings']['pci3_dss_saq_enable'])) {
      return !empty($this->payment->method->controller_data['settings']['pci3_show_card_holder']) ? 'true' : 'false';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreditcardShowIssueDateField() {
    if (!empty($this->payment->method->controller_data['settings']['pci3_dss_saq_enable'])) {
      return !empty($this->payment->method->controller_data['settings']['pci3_show_issue_date']) ? 'true' : 'false';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreditcardShowIssueNumberField() {
    if (!empty($this->payment->method->controller_data['settings']['pci3_dss_saq_enable'])) {
      return !empty($this->payment->method->controller_data['settings']['pci3_show_issue_number']) ? 'true' : 'false';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getJavascriptScriptVersion () {
    if (!empty($this->payment->method->controller_data['settings']['pci3_dss_saq_enable'])) {
      return 'pci3';
    }
  }

  /**
   * {@inheritdoc}
   *
   * Wirecard does not support retrying of payment with the same order number.
   * We need to add two zeros to the order number. We reserve them for payment
   * retries. Only 99 retries are allowed.
   *
   * @throws \Drupal\wirecard\Client\Exception\GeneralException
   */
  public function getOrderNumber() {
    if (empty($this->payment->context_data['order_number'])) {
      // Add two zeros to the order number. We reserve them for retries.
      // Wirecard doesn't support retrying payment with the same order number.
      $this->payment->context_data['order_number'] = $this->payment->pid * 100;
      entity_save('payment', $this->payment);
    }
    // Allow only 99 retries.
    else if ((int) substr($this->payment->context_data['order_number'], -2) < 99) {
      // Increment order_number to retry payment.
      $this->payment->context_data['order_number']++;
      entity_save('payment', $this->payment);
    }
    // If max number of retries is reached...
    else {
      // set status to "Max retries reached".
      $this->payment->setStatus(new \PaymentStatusItem(WIRECARD_PAYMENT_STATUS_MAX_RETRIES));
      entity_save('payment', $this->payment);
      throw new GeneralException("The maximum number of retries for payment {$this->payment->pid} has been reached");
    }

    $this->payment->context_data['order_number'];
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderReference() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerStatement() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getFinancialInstitution() {
    return $this->payment->context_data['financialinstitution'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingFirstname() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingLastname() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingAddress1() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingAddress2() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingCity() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingState() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingCountry() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingZipCode() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerEmail() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingPhone() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBillingFax() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingFirstName() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingLastName() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingAddress1() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingAddress2() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingCity() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingState() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingCountry() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingZipCode() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingPhone() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerShippingFax() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerBirthDate() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getCompanyName() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getCompanyVatId() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getCompanyTradeRegistryNumber() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getRegisterKey() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketAmount() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketCurrency() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItems() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemArticleNumber($index) {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemDescription($index) {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemQuantity($index) {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemTax($index) {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getBasketItemUnitPrice($index) {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreditorId() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getDueDate() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getMandateId() {
    return $this->get(__METHOD__);
  }

  /**
   * {@inheritdoc}
   */
  public function getMandateSignatureDate() {
    return $this->get(__METHOD__);
  }

}
