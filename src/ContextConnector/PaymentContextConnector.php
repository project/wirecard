<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\PaymentContextConnect
 */

namespace Drupal\wirecard\ContextConnector;

/**
 * Context connector if payment module is used stand-a-lone.
 */
class PaymentContextConnector extends ContextConnectorBase {

}
