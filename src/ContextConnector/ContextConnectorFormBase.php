<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\ContextConnectorFormBase
 */

namespace Drupal\wirecard\ContextConnector;

use Drupal\wirecard\Util\DataMapping;

/**
 * Configuration form for the payment connector.
 */
abstract class ContextConnectorFormBase implements ContextConnectorFormInterface {

  /**
   * Connector name. Used to prepare variable name.
   *
   * @var string
   */
  protected $connectorName;

  /**
   * Properties that miss Wirecard connector mapping.
   *
   * @var string[]
   */
  protected $missingMappings;

  /**
   * Constructs the object.
   *
   * @param string $connector_name
   *   Connector name. Used to prepare variable name.
   */
  public function __construct($connector_name) {
    $this->connectorName = $connector_name;
  }

  /**
   * Gets field human readable title.
   *
   * @param $field
   *   Field machine name.
   *
   * @return string|null
   *   Field human readable title. NULL if field title does not exist.
   */
  public static function getFieldLabel($field) {
    $fields = static::getFormFields();
    if (isset($fields[$field]['title'])) {
      return $fields[$field]['title'];
    }
  }

  /**
   * Gets settings for the form elements.
   *
   * @return array
   *   Settings for the form elements. Contains array of elements with:
   *    - title (string): Element title.
   *    - description (string): Element description.
   */
  public static function getFormFields() {
    return array(
      'customerStatement' => array(
        'title' => t('Customer Statement'),
        'description' => t('Text displayed on invoice of financial institution of your consumer.'),
      ),
      'orderReference' => array(
        'title' => t('Order reference'),
        'description' => t('Unique order reference ID sent from merchant to financial institution.'),
      ),
      'consumerBillingFirstname' => array(
        'title' => t('Consumer billing firstname'),
      ),
      'consumerBillingLastname' => array(
        'title' => t('Consumer billing lastname'),
      ),
      'consumerBillingAddress1' => array(
        'title' => t('Consumer billing address1'),
        'description' => t('Billing address line 1 (name of street and house number).'),
      ),
      'consumerBillingAddress2' => array(
        'title' => t('Consumer billing address2'),
        'description' => t('Billing address line 2 (optionally the house number if not already set within consumerBillingAddress1).'),
      ),
      'consumerBillingCity' => array(
        'title' => t('Consumer billing city'),
      ),
      'consumerBillingState' => array(
        'title' => t('Consumer billing State'),
      ),
      'consumerBillingCountry' => array(
        'title' => t('Consumer billing country'),
        'description' => t('An ISO 3166-1 country code.'),
      ),
      'consumerBillingZipCode' => array(
        'title' => t('Consumer billing zip code'),
      ),
      'consumerEmail' => array(
        'title' => t('Consumer E-mail'),
      ),
      'consumerBillingPhone' => array(
        'title' => t('Consumer billing phone'),
      ),
      'consumerBillingFax' => array(
        'title' => t('Consumer billing fax'),
      ),
      'consumerShippingFirstName' => array(
        'title' => t('Consumer shipping FirstName'),
      ),
      'consumerShippingLastName' => array(
        'title' => t('Consumer shipping LastName'),
      ),
      'consumerShippingAddress1' => array(
        'title' => t('Consumer shipping address1'),
        'description' => t('Shipping address line 1.'),
      ),
      'consumerShippingAddress2' => array(
        'title' => t('Consumer shipping address2'),
        'description' => t('Shipping address line 2.'),
      ),
      'consumerShippingCity' => array(
        'title' => t('Consumer shipping city'),
      ),
      'consumerShippingState' => array(
        'title' => t('Consumer shipping State'),
      ),
      'consumerShippingCountry' => array(
        'title' => t('Consumer shipping country'),
        'description' => t('An ISO 3166-1 country code.'),
      ),
      'consumerShippingZipCode' => array(
        'title' => t('Consumer shipping zip code'),
      ),
      'consumerShippingPhone' => array(
        'title' => t('Consumer shipping phone'),
      ),
      'consumerShippingFax' => array(
        'title' => t('Consumer shipping fax'),
      ),
      'consumerBirthDate' => array(
        'title' => t('Consumer birth date'),
        'description' => t('Birth date of consumer in the format YYYY-MM-DD.'),
      ),
      'companyName' => array(
        'title' => t('Company name'),
      ),
      'companyVatId' => array(
        'title' => t('Company VAT id'),
      ),
      'companyTradeRegistryNumber' => array(
        'title' => t('Company trade registry number'),
        'description' => t('Trade registration number of company.'),
      ),
      'dueDate' => array(
        'title' => t('Due date'),
        'description' => t('Date when payment is debited from consumer´s bank account. The due date is calculated by merchant or, if the field is left empty, Wirecard will automatically calculate the due date. Date as "DD.MM.YYYY".'),
      ),
      'companyRegisterKey' => array(
        'title' => t('Company register key'),
        'type' => 'string',
        'description' => t('Additional or alternative register key of company.'),
      ),
      'creditorId' => array(
        'title' => t('Creditor id'),
        'type' => 'string',
        'description' => t('Unique identifier of creditor (merchant).'),
      ),
      'mandateId' => array(
        'title' => t('Mandate id'),
        'type' => 'string',
        'description' => t('Identifier of displayed mandate.'),
      ),
      'mandateSignatureDate' => array(
        'title' => t('Mandate signature date'),
        'type' => 'string',
        'description' => t('Date when mandate was signed. Date as "DD.MM.YYYY".'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form_state) {
    $form = array();
    foreach (static::getFormFields() as $key => $setting) {
      $form[$key] = array(
        '#type' => 'textfield',
        '#title' => $setting['title'],
        '#description' => isset($setting['description']) ? $setting['description'] : '',
        '#default_value' => $this->getSetting($key),
        '#maxlength' => '255',
      );
    }

    $this->checkRequiredMappings($form);
    $form['tokens'] = $this->getTokenHelp();

    return $form;
  }

  /**
   * Gets the token help.
   *
   * @return array
   *   A renderable array.
   */
  protected function getTokenHelp() {
    return array(
      '#theme' => 'token_tree',
      '#token_types' => array('payment'),
    );
  }

  /**
   * Checks missing, required mappings. Marks required properties as mandatory.
   *
   * Notifies user about missing mappings.
   *
   * @param $form
   */
  protected function checkRequiredMappings(&$form) {
    // Mark each missing property as required.
    if ($mappings = DataMapping::getRequiredMappings($this->connectorName)) {
      foreach ($mappings as $field) {
        $form[$field]['#required'] = TRUE;
      }
    }
    // Notify user about missing mappings.
    if (DataMapping::getMissingMappings()) {
      drupal_set_message(t('Some required data mappings are missing - please provide all mappings marked as required.'), 'warning', FALSE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, array &$form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array $form, array &$form_state) {
    $values = $form_state['values'][$this->connectorName]['form'];
    unset($values['submit']);
    $this->setSettings($values);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    return variable_get("wirecard_connector_{$this->connectorName}_settings", array()) + $this->getSettingDefaults();
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($name) {
    $settings = $this->getSettings();
    if (isset($settings[$name])) {
      return $settings[$name];
    }
  }

  /**
   * Returns default values for settings.
   */
  protected function getSettingDefaults() {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $values) {
    variable_set("wirecard_connector_{$this->connectorName}_settings", $values);
  }
}
