<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\ContextConnectorInterface
 */

namespace Drupal\wirecard\ContextConnector;

/**
 * Interface for context connectors.
 *
 * Context connectors are responsible for extracting the data needed by
 * Wirecard from the context of a specific payment.
 */
interface ContextConnectorInterface {

  /**
   * Gets the connector name, which is the identifier of the class used.
   *
   * @return string
   *   The name.
   */
  public function getConnectorName();

  /**
   * Returns the payment amount.
   *
   * @return float
   */
  public function getAmount();

  /**
   * Returns the payment currency.
   *
   * @return string
   */
  public function getCurrency();

  /**
   * Returns the order description.
   *
   * @return string
   */
  public function getOrderDescription();

  /**
   * Returns the success url.
   *
   * @return string
   */
  public function getSuccessUrl();

  /**
   * Returns the cancel url.
   *
   * @return string
   */
  public function getCancelUrl();

  /**
   * Returns the failure url.
   *
   * @return string
   */
  public function getFailureUrl();

  /**
   * Returns the pending url.
   *
   * @return string
   */
  public function getPendingUrl();

  /**
   * Returns the confirm url.
   *
   * @return string
   */
  public function getConfirmUrl();

  /**
   * Returns the return url.
   *
   * @return string
   */
  public function getReturnUrl();

  /**
   * Returns the service url.
   *
   * @return string
   */
  public function getServiceUrl();

  /**
   * Returns the Unique ID of merchant.
   *
   * @return string
   */
  public function getCustomerId();

  /**
   * Returns the language for displayed texts on payment page.
   *
   * @return string
   */
  public function getLanguage();

  /**
   * Returns IP address of consumer.
   *
   * @return string
   */
  public function getConsumerIpAddress();

  /**
   * Returns user-agent of browser of consumer.
   *
   * @return string
   */
  public function getConsumerUserAgent();

  /**
   * Returns the window name of browser window where payment page is opened.
   *
   * @return string
   */
  public function getWindowName();

  /**
   * Returns the shop id.
   *
   * @return string
   */
  public function getShopId();

  /**
   * Returns the order number of the payment.
   *
   * @return int
   */
  public function getOrderNumber();

  /**
   * Returns text displayed on invoice of financial institution.
   *
   * @return string
   */
  public function getCustomerStatement();

  /**
   * Returns order reference ID sent from merchant to financial institution.
   *
   * @return mixed|null
   */
  public function getOrderReference();

  /**
   * Returns financial institution name regarding to selected payment method.
   *
   * @return string|null
   */
  public function getFinancialInstitution();

  /**
   * Returns first name of consumer.
   *
   * @return string|null
   */
  public function getConsumerBillingFirstname();

  /**
   * Returns last name of consumer.
   *
   * @return string|null
   */
  public function getConsumerBillingLastname();

  /**
   * Returns billing address line 1 (name of street and house number).
   *
   * @return string|null
   */
  public function getConsumerBillingAddress1();

  /**
   * Returns billing address line 2.
   *
   * @return string|null
   */
  public function getConsumerBillingAddress2();

  /**
   * Returns billing city.
   *
   * @return string|null
   */
  public function getConsumerBillingCity();

  /**
   * Returns billing country code.
   *
   * @return string|null
   */
  public function getConsumerBillingCountry();

  /**
   * Returns billing zip code.
   *
   * @return string|null
   */
  public function getConsumerBillingZipCode();

  /**
   * Returns E-mail address of consumer.
   *
   * @return string|null
   */
  public function getConsumerEmail();

  /**
   * Returns phone number of consumer.
   *
   * @return string|null
   */
  public function getConsumerBillingPhone();

  /**
   * Returns fax number of consumer.
   *
   * @return string|null
   */
  public function getConsumerBillingFax();

  /**
   * Returns first name of consumer.
   *
   * @return string|null
   */
  public function getConsumerShippingFirstName();

  /**
   * Returns last name of consumer.
   *
   * @return string|null
   */
  public function getConsumerShippingLastName();

  /**
   * Returns shipping address line 1.
   *
   * @return string|null
   */
  public function getConsumerShippingAddress1();

  /**
   * Returns shipping address line 2.
   *
   * @return string|null
   */
  public function getConsumerShippingAddress2();

  /**
   * Returns shipping city.
   *
   * @return string|null
   */
  public function getConsumerShippingCity();

  /**
   * Returns shipping state.
   *
   * @return string|null
   */
  public function getConsumerShippingState();

  /**
   * Returns shipping country code (ISO 3166-1).
   *
   * @return string|null
   */
  public function getConsumerShippingCountry();

  /**
   * Returns shipping ZIP code.
   *
   * @return string|null
   */
  public function getConsumerShippingZipCode();

  /**
   * Returns shipping phone number.
   *
   * @return string|null
   */
  public function getConsumerShippingPhone();

  /**
   * Returns shipping fax number.
   *
   * @return string|null
   */
  public function getConsumerShippingFax();

  /**
   * Returns birth date of consumer in the format YYYY-MM-DD.
   *
   * @return string|null
   */
  public function getConsumerBirthDate();

  /**
   * Returns name of company.
   *
   * @return string|null
   */
  public function getCompanyName();

  /**
   * Returns VAT ID of company.
   *
   * @return string|null
   */
  public function getCompanyVatId();

  /**
   * Returns trade registration number of company.
   *
   * @return string|null
   */
  public function getCompanyTradeRegistryNumber();

  /**
   * Returns additional or alternative register key of company.
   *
   * @return string|null
   */
  public function getRegisterKey();

  /**
   * Returns overall amount of shopping cart.
   *
   * @return float|null
   */
  public function getBasketAmount();

  /**
   * Returns code of currency based on ISO 4217.
   *
   * @return string|null
   */
  public function getBasketCurrency();

  /**
   * Returns number of items in shopping cart.
   *
   * @return int|null
   */
  public function getBasketItems();

  /**
   * Returns unique ID of article n in shopping cart.
   *
   * @param int $index
   *   The index of the basket item to use. Index numbers are incremental and
   *   start with 1.
   *
   * @return string|null
   */
  public function getBasketItemArticleNumber($index);

  /**
   * Returns product description of article n in shopping cart.
   *
   * @param int $index
   *   The index of the basket item to use. Index numbers are incremental and
   *   start with 1.
   *
   * @return string|null
   */
  public function getBasketItemDescription($index);

  /**
   * Returns items count of article n in shopping cart.
   *
   * @param int $index
   *   The index of the basket item to use. Index numbers are incremental and
   *   start with 1.
   *
   * @return int|null
   */
  public function getBasketItemQuantity($index);

  /**
   * Returns tax amount of article n in shopping cart.
   *
   * @param int $index
   *   The index of the basket item to use. Index numbers are incremental and
   *   start with 1.
   *
   * @return float|null
   */
  public function getBasketItemTax($index);

  /**
   * Returns price per unit of article n in shopping cart without taxes.
   *
   * @param int $index
   *   The index of the basket item to use. Index numbers are incremental and
   *   start with 1.
   *
   * @return float|null
   */
  public function getBasketItemUnitPrice($index);

  /**
   * Returns unique identifier of creditor (merchant).
   *
   * @return string|null
   */
  public function getCreditorId();

  /**
   * Returns date when payment is debited from consumer's bank account.
   *
   * @return string|null
   */
  public function getDueDate();

  /**
   * Returns identifier of displayed mandate.
   *
   * @return string|null
   */
  public function getMandateId();

  /**
   * Returns date when mandate was signed.
   *
   * @return string|null
   */
  public function getMandateSignatureDate();

  /**
   * Gets URL for style sheet to format the credit card input fields.
   *
   * Please be aware that @import, url() are ignored for security reasons.
   *
   * @return string|null
   */
  public function getIframeCssUrl();

  /**
   * Returns display of CVC field.
   *
   * @return string
   *   'true' if enabled. 'false' if disabled.
   */
  public function getcreditcardShowCvcField();

  /**
   * Returns display of card holder name field.
   *
   * @return string
   *   'true' if enabled. 'false' if disabled.
   */
  public function getCreditcardShowCardholderNameField();

  /**
   * Returns display of card issue date field.
   *
   * @return string
   *   'true' if enabled. 'false' if disabled.
   */
  public function getCreditcardShowIssueDateField();

  /**
   * Returns display of card issue number field.
   *
   * @return string
   *   'true' if enabled. 'false' if disabled.
   */
  public function getCreditcardShowIssueNumberField();

  /**
   * Returns javascript version. Alphanumeric with a fixed length of 4.
   *
   * @return string|null
   *   The value pci3 must be set for this parameter to allow the input of
   *   credit card related data in the displayed iframe.
   */
  public function getJavascriptScriptVersion();

  /**
   * Gets the connector form handler object.
   *
   * Returns the form handler associated with the context connector.
   *
   * @return \Drupal\wirecard\ContextConnector\ContextConnectorFormInterface
   *   The wirecard context connector to be used for the payment.
   */
  public function getFormHandler();

}