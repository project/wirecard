<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\ContextConnectorFormInterface
 */

namespace Drupal\wirecard\ContextConnector;

/**
 * Drupal configuration forms for context connectors.
 */
interface ContextConnectorFormInterface {

  /**
   * Builds the configuration form.
   *
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   The form with added configuration form elements.
   */
  public function buildForm(array &$form_state);

  /**
   * Validates the form.
   *
   * @param array $form
   *   The part of the form, where the elements have been added.
   * @param array $form_state
   *   Form state array.
   */
  public function validateForm(array $form, array &$form_state);

  /**
   * Submits the form.
   *
   * @param array $form
   *   The part of the form, where the elements have been added.
   * @param array $form_state
   *   Form state array.
   */
  public function submitForm(array $form, array &$form_state);

  /**
   * Gets connector settings.
   *
   * @return array
   *   Connector settings array. Each key is string with setting name. Each
   *   value is string with setting value.
   */
  public function getSettings();

  /**
   * Gets connector settings.
   *
   * @param string $name
   *   Setting name.
   *
   * @return string
   *   Connector setting value string.
   */
  public function getSetting($name);

  /**
   * Sets connector settings.
   *
   * @param $values array
   *   Connector settings array. Each key is string with setting name. Each
   *   value is string with setting value.
   */
  public function setSettings(array $values);

}