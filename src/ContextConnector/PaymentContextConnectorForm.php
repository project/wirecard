<?php

/**
 * @file
 * Contains \Drupal\wirecard\ContextConnector\PaymentContextConnectorForm
 */

namespace Drupal\wirecard\ContextConnector;

/**
 * Configuration form for the payment connector.
 */
class PaymentContextConnectorForm extends ContextConnectorFormBase {

}
