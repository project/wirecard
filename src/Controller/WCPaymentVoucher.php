<?php

/**
 * @file
 * Wirecard Voucher payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard Voucher payment method controller.
 */
class WCPaymentVoucher extends WCPaymentControllerBase {

  /**
   * The wirecard payment method type.
   *
   * @var string
   */
  protected $paymentType = 'VOUCHER';

  /**
   * {@inheritdoc}
   */
  public $requiresSensitiveData = TRUE;

  /**
   * Defaults for test mode.
   *
   * @var array
   */
  protected $testDefaults = array(
    'voucher_id' => '0123456789',
  );

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Voucher');
    $this->description = t('Wirecard Voucher payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Voucher');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $form = parent::getPaymentForm($form, $form_state);
    $values = $this->getFormStateValues($form_state);

    $form['voucher_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Voucher'),
      '#default_value' => !empty($values['voucher_id']) ? $values['voucher_id'] : '',
      '#required' => TRUE,
      '#attributes' => array('id' => 'wirecard-payment-voucher-id'),
    );

    // Javascript settings for element ids.
    $form['#attached']['js'][] = array(
      'data' => array(
        'wirecardMethodSelectors' => array(
          'voucherId' => 'wirecard-payment-voucher-id',
        )
      ),
      'type' => 'setting',
    );

    // Pre-populate fields with test data if test mode is activated.
    if ($form_state['payment']->method->controller_data['test_mode'] != self::PRODUCTION_MODE) {
      foreach ($this->testDefaults as $key => $value) {
        $form[$key]['#description'] = t('Testing mode is enabled. You can use "!value" for testing.', array('!value' => $value));
        if (empty($form[$key]['#default_value'])) {
          $form[$key]['#default_value'] = $value;
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentForm(array $form, array &$form_state) {
    parent::validatePaymentForm($form, $form_state);
    if (!form_get_errors()) {
      $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
      $form_state['payment']->context_data['voucher_id'] = $values['voucher_id'];
    }
  }
}
