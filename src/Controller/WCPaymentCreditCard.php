<?php

/**
 * @file
 * Wirecard Credit Card payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard Credit Card payment method controller.
 */
class WCPaymentCreditCard extends WCPaymentControllerBase {

  /**
   * The wirecard payment method type.
   *
   * @var string
   */
  protected $paymentType = 'CCARD';

  /**
   * {@inheritdoc}
   */
  public $requiresSensitiveData = TRUE;

  /**
   * Defaults for test mode.
   *
   * @var array
   */
  protected $testDefaults = array(
    'card_holder' => 'Jane Doe',
    'card_number' => '9500000000000001',
    'card_code' => 123,
    'card_expiration_month' => 12,
    'card_expiration_year' => 2031,
    'card_issue_month' => 1,
    'card_issue_year' => 2013,
    'card_issue_number' => 1234,
  );

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Credit Card');
    $this->description = t('Wirecard Credit Card payment integration.');
    $this->testDefaults['card_expiration_year'] = date('Y') + 20;
    $this->testDefaults['card_issue_year'] = date('Y') - 2;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Credit Card');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationForm(array $form, array &$form_state) {
    $form = parent::getConfigurationForm($form, $form_state);
    // Get default settings.
    $payment_method = $form_state['payment_method'];
    $controller = $payment_method->controller;
    $controller_data = $payment_method->controller_data + $controller->controller_data_defaults;

    $form['credit_card'] = array(
      '#type' => 'fieldset',
      '#title' => t('Credit Card settings'),
      '#description' => t('Configuration for PCI3 DSS SAQ-A compliant behaviour for entering credit card data'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#weight' => 10,
    );
    $form['credit_card']['card_issue_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable card issue information'),
      '#description' => t('Adds card issue fields in payment form.'),
      '#default_value' => isset($controller_data['settings']['card_issue_enabled']) ? $controller_data['settings']['card_issue_enabled'] : FALSE,
      '#states' => array(
        'invisible' => array(
          ':input[name="controller_form[credit_card][pci3_dss_saq_enable]"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['credit_card']['pci3_dss_saq_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable PCI3 DSS SAQ-A'),
      '#description' => t('Check to enable compliance to PCI3 DSS SAQ-A where the web page containing the input fields for credit card data is delivered by Wirecard and not via the web server of the merchant.'),
      '#default_value' => isset($controller_data['settings']['pci3_dss_saq_enable']) ? $controller_data['settings']['pci3_dss_saq_enable'] : TRUE,
    );
    $states = array(
      '#states' => array(
        'invisible' => array(
          ':input[name="controller_form[credit_card][pci3_dss_saq_enable]"]' => array('checked' => FALSE),
        ),
      ),
    );
    $form['credit_card']['pci3_show_cvc'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show CVC field'),
      '#description' => t('Check to show CVC field on the payment form.'),
      '#default_value' => isset($controller_data['settings']['pci3_show_cvc']) ? $controller_data['settings']['pci3_show_cvc'] : TRUE,
    ) + $states;
    $form['credit_card']['pci3_show_card_holder'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show card holder field'),
      '#description' => t('Check to show card holder field on the payment form.'),
      '#default_value' => isset($controller_data['settings']['pci3_show_card_holder']) ? $controller_data['settings']['pci3_show_card_holder'] : TRUE,
    ) + $states;
    $form['credit_card']['pci3_show_issue_date'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show issue date field'),
      '#description' => t('Check to show issue date field on the payment form.'),
      '#default_value' => isset($controller_data['settings']['pci3_show_issue_date']) ? $controller_data['settings']['pci3_show_issue_date'] : FALSE,
    ) + $states;
    $form['credit_card']['pci3_show_issue_number'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show issue number field'),
      '#description' => t('Check to show issue number field on the payment form.'),
      '#default_value' => isset($controller_data['settings']['pci3_show_issue_number']) ? $controller_data['settings']['pci3_show_issue_number'] : FALSE,
    ) + $states;
    $form['credit_card']['pci3_iframe_css_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Iframe CSS URL'),
      '#description' => t('URL for style sheet to format the credit card input fields as delivered by Wirecard. Please be aware that @import, url() are ignored for security reasons.'),
      '#default_value' => isset($controller_data['settings']['pci3_iframe_css_url']) ? $controller_data['settings']['pci3_iframe_css_url'] : '',
      '#field_prefix' => check_url($GLOBALS['base_url']) . '/',
    ) + $states;

    // Move advanced settings after credit card settings.
    $form['advanced']['#weight'] = 11;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array $form, array &$form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    if ($values['credit_card']['pci3_dss_saq_enable']) {
      // Validate attached CSS file.
      if ($values['credit_card']['pci3_iframe_css_url']) {
        $path = $GLOBALS['base_url'] . '/' . $values['credit_card']['pci3_iframe_css_url'];
        if (!file_exists($values['credit_card']['pci3_iframe_css_url'])) {
          form_set_error('pci3_iframe_css_url', t('File !path does not exist', array('!path' => $path)));
          return FALSE;
        }
        if (pathinfo($path, PATHINFO_EXTENSION) != 'css') {
          form_set_error('pci3_iframe_css_url', t('File $path should have a ".css" extension', array('!path' => $path)));
          return FALSE;
        }
      }
    }

    $controller_data = &$form_state['payment_method']->controller_data;
    $controller_data['settings'] = array(
      'card_issue_enabled' => $values['credit_card']['card_issue_enabled'],
      'pci3_dss_saq_enable' => $values['credit_card']['pci3_dss_saq_enable'],
      'pci3_show_cvc' => $values['credit_card']['pci3_show_cvc'],
      'pci3_show_card_holder' => $values['credit_card']['pci3_show_card_holder'],
      'pci3_show_issue_date' => $values['credit_card']['pci3_show_issue_date'],
      'pci3_show_issue_number' => $values['credit_card']['pci3_show_issue_number'],
      'pci3_iframe_css_url' => $values['credit_card']['pci3_iframe_css_url'],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $form = parent::getPaymentForm($form, $form_state);

    // Check if PCI3 DSS SAQ-A mode is enabled.
    if ($form_state['payment']->method->controller_data['settings']['pci3_dss_saq_enable']) {
      $form['card_form'] = array(
        '#type' => 'container',
        '#attributes' => array('id' => 'wirecard-credit-card-data-iframe'),
      );

      // Javascript settings PCI3 DSS SAQ-A mode.
      $form['#attached']['js'][] = array(
        'data' => array(
          'wirecardMethodData' => array(
            $this->paymentType => array(
              'pci3DssSaqEnable' => TRUE,
              'containerId' => 'wirecard-credit-card-data-iframe',
            ),
          ),
        ),
        'type' => 'setting',
      );
      // The HTML containing the input fields for sensitive credit card data is
      // hosted at Wirecard. Based on Javascript the consumer input is
      // immediately send to Wirecard, validated and stored in the data storage,
      // therefore no submit for storing this credit card data is required.
      return $form;
    }

    $values = $this->getFormStateValues($form_state);

    $form['card_holder'] = array(
      '#type' => 'textfield',
      '#title' => t('Card holder'),
      '#default_value' => !empty($values['card_holder']) ? $values['card_holder'] : '',
      '#required' => TRUE,
      '#attributes' => array('id' => 'wirecard-payment-card-holder'),
    );
    $form['card_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Credit card number'),
      '#default_value' => !empty($values['card_number']) ? $values['card_number'] : '',
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
      '#attributes' => array('id' => 'wirecard-payment-card-number'),
    );
    $form['card_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Card security code'),
      '#default_value' => !empty($values['card_code']) ? $values['card_code'] : '',
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
      '#attributes' => array('id' => 'wirecard-payment-card-code'),
    );
    $form['card_expiration_month'] = array(
      '#type' => 'select',
      '#title' => t('Expiration month'),
      '#options' => array(
        1 => t('Jan'),
        2 => t('Feb'),
        3 => t('Mar'),
        4 => t('Apr'),
        5 => t('May'),
        6 => t('Jun'),
        7 => t('Jul'),
        8 => t('Aug'),
        9 => t('Sep'),
        10 => t('Oct'),
        11 => t('Nov'),
        12 => t('Dec'),
      ),
      '#default_value' => !empty($values['card_expiration_month']) ? $values['card_expiration_month'] : '',
      '#required' => TRUE,
      '#attributes' => array('id' => 'wirecard-payment-card-expiration-month'),
    );
    $year = date('Y');
    $years = array($year => $year);
    for ($i = 1; $i <= 20; $i++) {
      $years[$year + $i] = $year + $i;
    }
    $form['card_expiration_year'] = array(
      '#type' => 'select',
      '#title' => t('Expiration year'),
      '#default_value' => !empty($values['card_expiration_year']) ? $values['card_expiration_year'] : '',
      '#element_validate' => array('element_validate_integer_positive'),
      '#options' => $years,
      '#required' => TRUE,
      '#attributes' => array('id' => 'wirecard-payment-card-expiration-year'),
    );

    // Prepare javascript settings for element ids on payment form.
    $js_settings = array(
      'wirecardMethodSelectors' => array(
        'cardholdername' => 'wirecard-payment-card-holder',
        'pan' => 'wirecard-payment-card-number',
        'cardverifycode' => 'wirecard-payment-card-code',
        'expirationMonth' => 'wirecard-payment-card-expiration-month',
        'expirationYear' => 'wirecard-payment-card-expiration-year',
      ),
    );

    // Card issue data is optional.
    if ($form_state['payment']->method->controller_data['settings']['card_issue_enabled']) {
      $form['card_issue_month'] = array(
        '#type' => 'select',
        '#title' => t('Issue month'),
        '#options' => array(
          1 => t('Jan'),
          2 => t('Feb'),
          3 => t('Mar'),
          4 => t('Apr'),
          5 => t('May'),
          6 => t('Jun'),
          7 => t('Jul'),
          8 => t('Aug'),
          9 => t('Sep'),
          10 => t('Oct'),
          11 => t('Nov'),
          12 => t('Dec'),
        ),
        '#default_value' => !empty($values['card_issue_month']) ? $values['card_issue_month'] : '',
        '#required' => TRUE,
        '#attributes' => array('id' => 'wirecard-payment-card-issue-month'),
      );
      $year = date('Y') - 20;
      $years = array($year => $year);
      for ($i = 1; $i <= 40; $i++) {
        $years[$year + $i] = $year + $i;
      }
      $form['card_issue_year'] = array(
        '#type' => 'select',
        '#title' => t('Issue year'),
        '#required' => TRUE,
        '#default_value' => !empty($values['card_issue_year']) ? $values['card_issue_year'] : '',
        '#element_validate' => array('element_validate_integer_positive'),
        '#options' => $years,
        '#attributes' => array('id' => 'wirecard-payment-card-issue-year'),
      );
      $form['card_issue_number'] = array(
        '#type' => 'textfield',
        '#title' => t('Issue number'),
        '#default_value' => !empty($values['card_issue_number']) ? $values['card_issue_number'] : '',
        '#element_validate' => array('element_validate_integer_positive'),
        '#required' => TRUE,
        '#attributes' => array('id' => 'wirecard-payment-card-issue-number'),
      );

      // Extend javascript settings for element ids with card issue data.
      $js_settings['wirecardMethodSelectors'] += array(
        'issueMonth' => 'wirecard-payment-card-issue-month',
        'issueYear' => 'wirecard-payment-card-issue-year',
        'issueNumber' => 'wirecard-payment-card-issue-number',
      );
    }

    // Javascript settings for element ids.
    $form['#attached']['js'][] = array(
      'data' => $js_settings,
      'type' => 'setting',
    );

    // Pre-populate fields with test data if test mode is activated.
    if ($form_state['payment']->method->controller_data['test_mode'] != self::PRODUCTION_MODE) {
      foreach ($this->testDefaults as $key => $value) {
        $form[$key]['#description'] = t('Testing mode is enabled. You can use "!value" for testing.', array('!value' => $value));
        if (empty($form[$key]['#default_value'])) {
          $form[$key]['#default_value'] = $value;
        }
      }
    }

    return $form;
  }

}
