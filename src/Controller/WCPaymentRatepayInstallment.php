<?php

/**
 * @file
 * Wirecard Ratepay Installment payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentRatepayInstallment payment method controller.
 */
class WCPaymentRatepayInstallment extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'INSTALLMENT';

  /**
   * {@inheritdoc}
   */
  protected $requestName = 'Ratepay';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Ratepay Installment');
    $this->description = t('Wirecard installment integration by Ratepay.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Ratepay Installment');
  }

}
