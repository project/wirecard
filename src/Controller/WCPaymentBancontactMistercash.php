<?php

/**
 * @file
 * Wirecard BancontactMistercash payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard BancontactMistercash payment method controller.
 */
class WCPaymentBancontactMistercash extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'BANCONTACT_MISTERCASH';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Bancontact/Mister Cash');
    $this->description = t('Wirecard Bancontact/Mister Cash payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Bancontact/Mister Cash');
  }

}
