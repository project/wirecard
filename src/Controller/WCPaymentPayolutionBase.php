<?php

/**
 * @file
 * Wirecard Payolution Invoice payment method base class.
 */

namespace Drupal\wirecard\Controller;

use Drupal\wirecard\Client\Request\CheckoutInitRequest;
use Drupal\wirecard\ContextConnector\ContextConnectorFormBase;

/**
 * Base class for Wirecard WCPaymentInvoice payment method controller.
 */
abstract class WCPaymentPayolutionBase extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $requestName = 'Payolution';

  /**
   * {@inheritdoc}
   *
   * Required parameters validation.
   */
  public function validate(\Payment $payment, \PaymentMethod $payment_method, $strict) {
    parent::validate($payment, $payment_method, $strict);
    try {
      $connector = $this->getConnector($payment, $payment_method);

      $birth_date = $connector->getConsumerBirthDate();
      // Birth date of consumer should be in the format YYYY-MM-DD.
      if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $birth_date)) {
        throw new \Exception(t('!field is invalid', array(
          '!field' => ContextConnectorFormBase::getFieldLabel('consumerBirthDate'),
        )));
      }
      // Customer age should be greater than 18.
      $age = floor((time() - strtotime($birth_date)) / 31556926);
      if ($age < 18) {
        throw new \Exception(t('Consumer must be older than 18 years of age.'));
      }

      // Billing address and shipping address must be the same.
      $class = CheckoutInitRequest::getClassName($this->getRequestName());
      $addresses = array(
        'Shipping' => '',
        'Billing' => '',
      );
      foreach ($addresses as $address_type => &$address_value) {
        foreach (get_class_vars($class) as $name => $default_value) {
          if (strpos($name, "consumer{$address_type}") !== FALSE) {
            $method = 'get' . ucfirst($name);
            $address_value .= $connector->$method();
          }
        }
      }
      if ($addresses['Shipping'] && $addresses['Shipping'] != $addresses['Billing']) {
        throw new \Exception(t('Billing address and shipping address must be the same.'));
      }
    }
    catch (\Exception $e) {
      $this->getLogger()->error($e->getMessage(), array(), NULL, FALSE);
      throw new \PaymentValidationException($e->getMessage());
    }
  }

}
