<?php

/**
 * @file
 * Wirecard Skrill Direct payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentSkrillDirect payment method controller.
 */
class WCPaymentSkrillDirect extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'SKRILLDIRECT';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Skrill Direct');
    $this->description = t('Wirecard Skrill Direct payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Skrill Direct');
  }

}
