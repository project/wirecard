<?php

/**
 * @file
 * Wirecard mpass payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentMpass payment method controller.
 */
class WCPaymentMpass extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'MPASS';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard mpass');
    $this->description = t('Wirecard mpass payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('mpass');
  }

}
