<?php

/**
 * @file
 * Wirecard moneta.ru payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentMoneta payment method controller.
 */
class WCPaymentMoneta extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'MONETA';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard moneta.ru');
    $this->description = t('Wirecard moneta.ru payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('moneta.ru');
  }

}
