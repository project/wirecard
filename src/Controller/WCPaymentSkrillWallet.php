<?php

/**
 * @file
 * Wirecard Skrill Digital Wallet payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentSkrillWallet payment method controller.
 */
class WCPaymentSkrillWallet extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'SKRILLWALLET';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Skrill Digital Wallet');
    $this->description = t('Wirecard Skrill Digital Wallet payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Skrill Digital Wallet');
  }

}
