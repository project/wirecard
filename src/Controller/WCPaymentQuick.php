<?php

/**
 * @file
 * Wirecard @Quick payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentQuick payment method controller.
 */
class WCPaymentQuick extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'QUICK';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard @Quick');
    $this->description = t('Wirecard @Quick payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('@Quick');
  }

}
