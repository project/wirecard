<?php

/**
 * @file
 * Wirecard Trustly payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentTrustly payment method controller.
 */
class WCPaymentTrustly extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'TRUSTLY';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Trustly');
    $this->description = t('Wirecard Trustly payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Trustly');
  }

}
