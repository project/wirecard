<?php

/**
 * @file
 * Wirecard Przelewy24 payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentPrzelewy24 payment method controller.
 */
class WCPaymentPrzelewy24 extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'PRZELEWY24';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Przelewy24');
    $this->description = t('Wirecard Przelewy24 payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Przelewy24');
  }

}
