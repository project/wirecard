<?php

/**
 * @file
 * Wirecard eps Online-Überweisung payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentEps payment method controller.
 */
class WCPaymentEps extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'EPS';

  /**
   * List of eps financial institutions.
   *
   * @var string[]
   */
  protected $financialInstitutions = array(
    'BA-CA' => 'Bank Austria Creditanstalt',
    'BB-Racon' => 'Bank Burgenland',
    'ARZ|BAF' => 'Ärztebank',
    'ARZ|BCS' => 'Bankhaus Carl Spängler & Co. AG',
    'Bawag|B' => 'BAWAG',
    'ARZ|VB' => 'Die Östereischischen Volksbanken',
    'Bawag|E' => 'easybank',
    'Spardat|EBS' => 'Erste Bank und Sparkassen',
    'ARZ|GB' => 'Gärtnerbank',
    'ARZ|HAA' => 'Hypo Alpe-Adria-Bank AG, HYPO Alpe-Adria-Bank International AG',
    'ARZ|HI' => 'Hypo Investmentbank AG',
    'Hypo-Racon|O' => 'Hypo Oberösterreich',
    'Hypo-Racon|S' => 'Hypo Salzburg',
    'Hypo-Racon|St' => 'Hypo Steiermark',
    'ARZ|HTB' => 'Hypo Tirol Bank AG',
    'ARZ|IB' => 'Immo-Bank',
    'ARZ|IKB' => 'Investkredit Bank AG',
    'ARZ|NLH' => 'Niederösterreichische Landes-Hypothekenbank AG',
    'ARZ|OB' => 'Oberbank AG',
    'ARZ|AB' => 'Österreichische Apothekerbank',
    'ARZ|BSS' => 'Bankhaus Schelhammer & Schattera AG',
    'Bawag|P' => 'PSK Bank',
    'Racon' => 'Raiffeisen Bank',
    'Bawag|S' => 'Sparda Bank',
    'ARZ|VLH' => 'Vorarlberger Landes- und Hypothekerbank AG',
    'ARZ|SB' => 'Schoellerbank AG',
    'ARZ|SBL' => 'Sparda-Bank Linz',
    'ARZ|SBVI' => 'Sparda-Bank Villach/Innsbruck',
    'ARZ|VRB' => 'VR-Bank Braunau',
    'ARZ|BTV' => 'BTV VIER LÄNDER BANK',
    'ARZ|BKS' => 'BKS Bank AG',
    'ARZ|AAB' => 'Austrian Anadi Bank AG',
    'ARZ|BD' => 'bankdirekt.at AG',
    'ARZ|PB' => 'PRIVAT BANK AG',
  );

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard eps Online-Überweisung');
    $this->description = t('Wirecard eps Online-Überweisung payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('eps Online-Überweisung');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $form = parent::getPaymentForm($form, $form_state);

    $values = $this->getFormStateValues($form_state);

    $form['financialinstitution'] = array(
      '#type' => 'select',
      '#title' => t('Financial institution'),
      '#options' => $this->financialInstitutions,
      '#default_value' => !empty($values['financialinstitution']) ? $values['financialinstitution'] : '',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentForm(array $form, array &$form_state) {
    parent::validatePaymentForm($form, $form_state);
    if (!form_get_errors()) {
      $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
      $form_state['payment']->context_data['financialinstitution'] = $values['financialinstitution'];
    }
  }

}
