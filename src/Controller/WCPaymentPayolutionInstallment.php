<?php

/**
 * @file
 * Wirecard Payolution Installment payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentPayolutionInstallment payment method controller.
 */
class WCPaymentPayolutionInstallment extends WCPaymentPayolutionBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'INSTALLMENT';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Payolution Installment');
    $this->description = t('Wirecard installment integration by Payolution.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Payolution Installment');
  }

}
