<?php

/**
 * @file
 * Wirecard payment method base class.
 */

namespace Drupal\wirecard\Controller;

use Drupal\wirecard\Client\Exception\WCErrorException;
use Drupal\wirecard\Client\Exception\WCExceptionBase;
use Drupal\wirecard\Client\Request\CheckoutInitRequest;
use Drupal\wirecard\Client\Request\DataStorageInitRequest;
use Drupal\wirecard\Client\Request\RequestBase;
use Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultResponse;
use Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultCancelResponse;
use Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultFailureResponse;
use Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultPendingResponse;
use Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessResponse;
use Drupal\wirecard\Client\Response\ResponseError;
use Drupal\wirecard\Client\WCSeamlessAPIClient;
use Drupal\wirecard\Client\WCClientConfig;
use Drupal\wirecard\ContextConnector\ContextConnectorInterface;
use Drupal\wirecard\ContextConnector\ContextConnectorFormInterface;
use Drupal\wirecard\Util\WirecardMetadata;
use Drupal\wirecard\Util\WirecardLogger;

/**
 * Wirecard payment method controller base class.
 */
abstract class WCPaymentControllerBase extends \PaymentMethodController implements WCPaymentControllerInterface {

  /**
   * Production mode option key.
   *
   * @var int
   */
  const PRODUCTION_MODE = 0;

  /**
   * Demo mode option key.
   *
   * @var int
   */
  const DEMO_MODE = 1;

  /**
   * Payment method table name. Used for configuration storage.
   *
   * @var string
   */
  public $schema = 'wirecard_payment_method';

  /**
   * The logger to use.
   *
   * @var WirecardLogger|null
   */
  protected $logger;

  /**
   * The API client.
   *
   * @var WCSeamlessAPIClient|null
   */
  protected $client;

  /**
   * The context connector.
   *
   * @var ContextConnectorInterface
   */
  protected $connector;

  /**
   * Properties that miss Wirecard connector mapping.
   *
   * @var string[]
   */
  protected $missingMappings;

  /**
   * {@inheritdoc}
   */
  public $controller_data_defaults = array(
    'test_mode' => self::DEMO_MODE,
    'customer_id' => '',
    'secret' => '',
    'status' => PAYMENT_STATUS_SUCCESS,
    'password' => '',
    'shop_id' => '',
    'language' => '',
  );

  /**
   * {@inheritdoc}
   */
  public $payment_method_configuration_form_elements_callback = 'wirecard_payment_method_configuration_form_elements';

  /**
   * {@inheritdoc}
   */
  public $payment_configuration_form_elements_callback = 'wirecard_payment_configuration_form_elements';

  /**
   * The wirecard payment type / method name.
   *
   * @var string
   */
  protected $paymentType;

  /**
   * (optional) The wirecard payment request name.
   *
   * By default $requestName is equal to $paymentType. $requestName should be
   * set only if the payment type is the same for few payment methods.
   *
   * Example: Both Payolution and Ratepay have the same payment type INVOICE but
   * request parameters are different for each payment method. Requests are
   * handled in separate classes with name corresponding to request name:
   * CheckoutInitPayolutionRequest, CheckoutInitRatepayRequest.
   *
   * @var string
   * @see CheckoutInitRequest::create()
   */
  protected $requestName;

  /**
   * Defines whether it is required to store sensitive data.
   *
   * Sensitive data will be stored in data storage for current payment method.
   *
   * @var boolean
   */
  public $requiresSensitiveData = FALSE;

  /**
   * Generates link to the payment view.
   *
   * @param \Payment $payment
   *   Payment entity.
   *
   * @return string|null
   *   HTML of link to the payment view. NULL if payment id is not set.
   */
  public function getPaymentLink(\Payment $payment) {
    if ($payment->pid) {
      $uri = payment_uri($payment);
      return l(payment_title($payment), $uri['path']);
    }
  }

  /**
   * Resolves Wirecard payment request name.
   *
   * By default $requestName is equal to $paymentType. $requestName should be
   * set only if the payment type is the same for few payment methods.
   *
   * Example: Both Payolution and Ratepay have the same payment type INVOICE but
   * request parameters are different for each payment method. Requests are
   * handled in separate classes with name corresponding to request name:
   * CheckoutInitPayolutionRequest, CheckoutInitRatepayRequest.
   */
  protected function getRequestName() {
    return $this->requestName ? $this->requestName : $this->paymentType;
  }

  /**
   * Returns a pre-configured logger.
   *
   * @return WirecardLogger
   */
  public function getLogger() {
    if (!isset($this->logger)) {
      $this->logger = WirecardLogger::create();
    }
    return $this->logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getClient(\PaymentMethod $payment_method) {
    if (!$this->client) {
      $config['paymentType'] = $this->paymentType;
      // Demo mode configuration.
      if (variable_get('wirecard_force_demo_mode') || $payment_method->controller_data['test_mode'] == self::DEMO_MODE) {
        if (!empty($this->requestName) && $this->requestName == 'Payolution' && $this->paymentType == 'INVOICE') {
          // Payolution invoice payment method has a specific test data.
          $config['customerId'] = 'D200906';
          $config['secret'] = 'PXEEY98CG8S2NFXY58YUTBQYS6W2UJ5BERJM8KX35HSNCJ6XUHDERZQ6SX3P';
          $config['shopId'] = 'qmorekar';
        }
        else {
          $config['customerId'] = 'D200001';
          $config['secret'] = 'B8AKTPWBRMNBV455FG6M2DANE99WU2';
          $config['shopId'] = 'qmore';
          $config['password'] = 'jcv45z';
        }
      }
      // Production mode configuration.
      else if ($payment_method->controller_data['test_mode'] == self::PRODUCTION_MODE) {
        $config['customerId'] = $payment_method->controller_data['customer_id'];
        $config['secret'] = $payment_method->controller_data['secret'];
        $config['shopId'] = $payment_method->controller_data['shop_id'];
        $config['password'] = $payment_method->controller_data['password'];
      }

      // Handle Wirecard checkout URL configuration.
      if (!empty($payment_method->controller_data['settings']['service_url'])) {
        $config['serviceUrl'] = $payment_method->controller_data['settings']['service_url'];
      }
      else {
        $config['serviceUrl'] = '<front>';
      }

      // Handle language configuration.
      if (!empty($payment_method->controller_data['language'])) {
        $config['language'] = $payment_method->controller_data['language'];
      }
      // Get global language on fallback.
      else {
        global $language;
        $config['language'] = $language->language;
      }
      $this->client = new WCSeamlessAPIClient(WCClientConfig::create($config), $this->getLogger());
    }

    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnector(\Payment $payment, \PaymentMethod $payment_method = NULL) {
    if (!$payment_method) {
      $payment_method = $payment->method;
    }
    $hash = spl_object_hash($payment) . ':' . spl_object_hash($payment_method);

    if (!isset($this->connector[$hash])) {
      $connector_info = wirecard_connector_info();
      $connector_name = 'payment';
      // $payment->context can be empty during creation. Fall back to the
      // 'payment' connector as a default connector.
      if (!empty($payment->context)) {
        if (!empty($connector_info[$payment->context]['connector'])) {
          $connector_name = $payment->context;
        }
      }
      $connector_class = $connector_info[$connector_name]['connector'];
      $this->connector[$hash] = new $connector_class($payment, $this->getClient($payment_method)->getConfig(), $connector_name);
    }
    return $this->connector[$hash];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationForm(array $form, array &$form_state) {
    // Get default settings.
    $payment_method = $form_state['payment_method'];
    $controller = $payment_method->controller;
    $controller_data = $payment_method->controller_data + $controller->controller_data_defaults;

    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('General settings'),
      '#collapsible' => TRUE,
      '#group' => 'vertical_tabs',
    );
    $form['general']['test_mode'] = array(
      '#type' => 'radios',
      '#title' => t('Testing'),
      '#options' => array(
        self::DEMO_MODE => t('Demo mode'),
        self::PRODUCTION_MODE => t('Production mode'),
      ),
      '#description' => t('In "Demo mode" there is no communication with your financial service provider but only between your online shop and Wirecard Checkout Seamless. In "Production mode" real money will be transferred.'),
      '#default_value' => $controller_data['test_mode'],
    );
    $form['general']['customer_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Customer id'),
      '#description' => t('Your personal customer id obtained by Wirecard. E.g. D200001'),
      '#default_value' => $controller_data['customer_id'],
      '#required' => TRUE,
    );
    $form['general']['secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Secret'),
      '#description' => t('Your personal secret obtained by Wirecard. Pre-shared key, used to sign the transmitted data. E.g. B8AKTPWBRMNBV455FG6M2DANE99WU2'),
      '#default_value' => $controller_data['secret'],
      '#required' => TRUE,
    );
    $form['general']['password'] = array(
      // @todo: Could use 'password' type.
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#description' => t('Make sure to enter a password, if the Wirecard backend operations should be used via API or the Wirecard Rules module.'),
      '#default_value' => $controller_data['password'],
    );
    $form['general']['status'] = array(
      '#type' => 'select',
      '#title' => t('Final payment status'),
      '#description' => t('The status to give a payment after being processed by this payment method.'),
      '#default_value' => isset($controller_data['status']) ? $controller_data['status'] : PAYMENT_STATUS_SUCCESS,
      '#options' => payment_status_options(),
    );
    $form['general']['message'] = array(
      '#type' => 'text_format',
      '#title' => t('Payment form message'),
      '#description' => t('Message shown to the user during payment.'),
      '#default_value' => isset($controller_data['message']) ? $controller_data['message'] : '',
      '#format' => isset($controller_data['text_format']) ? $controller_data['text_format'] : filter_default_format(),
    );
    $form['restrictions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Restrictions'),
      '#description' => t('If one of this restrictions are not met for a payment, this payment method is not available to customers.'),
      '#prefix' => '<div id="form-restriction-wrapper">',
      '#suffix' => '</div>',
    );
    $form['restrictions']['restriction_currency'] = array(
      '#type' => 'textfield',
      '#title' => t('Amount restriction currency'),
      '#description' => t('The ISO 4217 currency code of the currency used for amount restrictions below. Amount restrictions only apply to the currency selected.'),
      '#default_value' => isset($controller_data['settings']['restriction_currency']) ? $controller_data['settings']['restriction_currency'] : 'EUR',
      '#ajax' => array(
        'callback' => 'wirecard_payment_configuration_form_restriction_amounts',
        'wrapper' => 'form-restriction-wrapper',
        'method' => 'replace',
      ),
      '#submit' => array('wirecard_payment_form_ajax_rebuild_submit'),
      '#executes_submit_callback' => TRUE,
    );
    $stored_currency = (isset($controller_data['settings']['restriction_currency']) ? $controller_data['settings']['restriction_currency'] : 'EUR');
    $currency = isset($form_state['values']['restrictions']['restriction_currency']) ? $form_state['values']['restrictions']['restriction_currency'] : $stored_currency;
    $form['restrictions']['min_amount'] = array(
      '#type' => 'payment_amount',
      '#title' => t('Minimum amount'),
      '#currency_code' => $currency,
      '#default_value' => isset($controller_data['settings']['min_amount']) ? $controller_data['settings']['min_amount'] : NULL,
    );
    $form['restrictions']['max_amount'] = array(
      '#type' => 'payment_amount',
      '#title' => t('Maximum amount'),
      '#currency_code' => $currency,
      '#default_value' => isset($controller_data['settings']['max_amount']) ? $controller_data['settings']['max_amount'] : NULL,
    );
    $form['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['advanced']['shop_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Shop id'),
      '#description' => t('Please use this parameter only if it is enabled by Wirecard. E.g. qmore'),
      '#default_value' => $controller_data['shop_id'],
    );
    $form['advanced']['language'] = array(
      '#type' => 'textfield',
      '#title' => t('Language code'),
      '#description' => t('Language used by Wirecard during the checkout process. By default Drupal\'s current interface language is used; e.g. "en".'),
      '#default_value' => $controller_data['language'],
    );
    $form['advanced']['service_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Service URL'),
      '#description' => t('URL of your service page containing contact information. Defaults to @url - the front page.', array('@url' => '<front>')),
      '#default_value' => isset($controller_data['settings']['service_url']) ? $controller_data['settings']['service_url'] : '<front>',
    );

    // Checks missing mappings.
    $this->checkRequiredMappings($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array $form, array &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $controller_data = &$form_state['payment_method']->controller_data;
    $controller_data['test_mode'] = $values['general']['test_mode'];
    $controller_data['customer_id'] = $values['general']['customer_id'];
    $controller_data['secret'] = $values['general']['secret'];
    $controller_data['password'] = $values['general']['password'];
    $controller_data['status'] = $values['general']['status'];
    $controller_data['message'] = $values['general']['message']['value'];
    $controller_data['text_format'] = $values['general']['message']['format'];
    $controller_data['language'] = $values['advanced']['language'];
    $controller_data['shop_id'] = $values['advanced']['shop_id'];
    $controller_data['settings']['service_url'] = $values['advanced']['service_url'];
    $controller_data['settings']['restriction_currency'] = $values['restrictions']['restriction_currency'];
    $controller_data['settings']['min_amount'] = $values['restrictions']['min_amount'];
    $controller_data['settings']['max_amount'] = $values['restrictions']['max_amount'];
    // Set default generic title.
    // @see payment_form_payment_method_submit()
    if (empty($form_state['values']['title_generic'])) {
      $form_state['values']['title_generic'] = $this->getTitleGeneric();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $payment = $form_state['payment'];
    if (!empty($payment->method->controller_data['message'])) {
      $form['message'] = array(
        '#type' => 'markup',
        '#markup' => check_markup($payment->method->controller_data['message'], $payment->method->controller_data['text_format']),
      );
    }
    try {
      // Add form wrapper. Required to handle submit in javascript.
      $form['#prefix'] = '<div id="wirecard-payment-form">';
      $form['#suffix'] = '</div>';

      $payment = $form_state['payment'];
      $client = $this->getClient($payment->method);

      // Create and initialize Order ident if not done yet.
      $order_ident = $this->getCurrentOrderIdent($payment);
      // Store it in the session for later use.
      $this->storeCurrentOrderIdent($order_ident);

      // Only initialize the data storage once per payment type, so that the
      // same data storage is re-used when the form is reloaded multiple times;
      // e.g., via ajax.
      if (empty($form_state['storage']['wirecard'][$this->paymentType])) {
        // Clear storage once payment type is changed.
        $form_state['storage']['wirecard'] = array();
        $form_state['storage']['wirecard'][$this->paymentType]['data_storage_response'] = $client->initializeDataStorage($this->createDataStorageInitRequest($payment));
        $response = $form_state['storage']['wirecard'][$this->paymentType]['data_storage_response'];
        // Keep the storage id for payment execution later.
        $this->storeCurrentStorageId($response->storageId);
      }

      // Check we need to store sensitive payment data to the data storage.
      if ($this->requiresSensitiveData) {
        $response = $form_state['storage']['wirecard'][$this->paymentType]['data_storage_response'];
        // @todo Should there be some exception for non-js?
        // Add javascript provided by Wirecard. We need to add a payment-type
        // token to omit js caching.
        $js_url = $response->javascriptUrl . '?payment-type=' . $this->paymentType;
        // Save payment method to the javascript storage.
        $form['#attached']['js'][] = array(
          'data' => array(
            'wirecardMethodData' => array(
              'methodName' => $this->paymentType,
            ),
            'wirecardScriptSrc' => $js_url,
          ),
          'type' => 'setting',
        );
        // Add custom javascript that handles Wirecard storage,
        $form['#attached']['js'][] = drupal_get_path('module', 'wirecard') . '/js/wirecard.js';
      }
      else {
        // Remove javascript settings for payment methods which do not require
        // data storage.
        $form['#attached']['js'][] = array(
          'data' => array(
            'wirecardMethodData' => array(),
          ),
          'type' => 'setting',
        );
      }
    }
    catch (WCErrorException $e) {
      drupal_set_message(t($e->getLogMessage(), $e->getLogContext()), 'error');
      $this->getLogger()->error($e->getLogMessage(), $e->getLogContext());
      // @todo storage id might be outdated. We need to reload the form or
      //   reinitialize storage earlier instead showing an error here.
      form_set_error('', $e->getLogMessage());
    }
    catch (WCExceptionBase $e) {
      $this->getLogger()->error($e->getLogMessage(), $e->getLogContext());
      form_set_error('', $e->getLogMessage());
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * Required parameters validation.
   */
  public function validate(\Payment $payment, \PaymentMethod $payment_method, $strict) {
    parent::validate($payment, $payment_method, $strict);
    // Check amount restrictions.
    $controller_data = $payment_method->controller_data;
    if (isset($controller_data['settings']['restriction_currency']) && $controller_data['settings']['restriction_currency'] == $payment->currency_code) {
      // Check minimum and maximum amount.
      if (isset($controller_data['settings']['min_amount']) && $controller_data['settings']['min_amount'] > $payment->totalAmount(TRUE)) {
        throw new \PaymentValidationAmountBelowMinimumException(t('The amount should be higher than !minimum.', array(
          '!minimum' => payment_amount_human_readable($controller_data['settings']['min_amount'], $payment->currency_code),
        )));
      }
      if (!empty($controller_data['settings']['max_amount']) && $controller_data['settings']['max_amount'] < $payment->totalAmount(TRUE)) {
        throw new \PaymentValidationAmountExceedsMaximumException(t('The amount should be lower than !maximum.', array(
          '!maximum' => payment_amount_human_readable($controller_data['settings']['max_amount'], $payment->currency_code),
        )));
      }
    }
    // Check for missing mappings in the used connector.
    $connector = $this->getConnector($payment, $payment_method);
    $mapping = $connector->getFormHandler()->getSettings();
    $name = $connector->getConnectorName();
    $missing_mappings = array();
    foreach ($this->getRequiredMappings($connector->getConnectorName()) as $key) {
      if (empty($mapping[$key])) {
        $missing_mappings[$name][$key] = $key;
      }
    }
    if ($missing_mappings) {
      // Log error on missing mappings.
      $this->getLogger()->error('There are some missing mappings for the following payment method: <a href="!method_url">!method</a>. Please configure the missing mappings at the <a href="!config_url">Wirecard data mapping page</a>.', array(
        '!method_url' => "/admin/config/services/payment/method/$payment_method->pmid/edit",
        '!method' => $payment_method->title_specific,
        '!config_url' => '/admin/config/services/payment/wirecard'
      ), NULL, FALSE);
      throw new \PaymentValidationException("Missing mappings for the payment method $payment_method->title_specific");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredMappings($connector_name) {
    /* @var $class CheckoutInitRequest */
    $class = CheckoutInitRequest::getClassName($this->getRequestName());
    $required_properties = $class::getRequiredProperties();
    $required_properties = $required_properties ? $required_properties : array();
    $connectors = wirecard_connector_info();
    $connector_form_handler = $connectors[$connector_name]['form_handler'];
    $mapping_properties = array_keys($connector_form_handler::getFormFields());
    $mapping_properties = $mapping_properties ? $mapping_properties : array();
    $required_mappings = array_intersect($required_properties, $mapping_properties);
    return $required_mappings;
  }

  /**
   * {@inheritdoc}
   */
  public function getMissingMappings() {
    if (!$this->missingMappings) {
      $missing_mappings = array();
      foreach (wirecard_connector_info(TRUE) as $connector_name => $connector_info) {
        if ($required_mappings = $this->getRequiredMappings($connector_name)) {
          /* @var $handler ContextConnectorFormInterface */
          $handler = new $connector_info['form_handler']($connector_name);
          $mapping = $handler->getSettings();
          foreach ($required_mappings as $property) {
            // Check if mapping is missing.
            if (empty($mapping[$property])) {
              $missing_mappings[$connector_name][$property] = $property;
            }
          }
        }
      }

      $this->missingMappings = $missing_mappings;
    }
    return $this->missingMappings;
  }

  /**
   * Checks required and missing mappings.
   *
   * Notifies user about missing mappings.
   *
   * @param $form
   */
  protected function checkRequiredMappings(&$form, &$form_state) {
    if ($form_state['storage']['wirecard_missing_mappings'] = $this->getMissingMappings()) {
      // Check if we are at method add form.
      if (empty($form_state['payment_method']->pmid)) {
        $form_state['storage']['is_new'] = TRUE;
      }
      // We are at edit form of existing method.
      else {
        // Notifies user about missing mappings.
        drupal_set_message(t('There are some missing data mappings for current payment method. Please configure the missing mappings at the <a href="!config_url">Wirecard data mapping page</a>.', array(
          '!config_url' => '/admin/config/services/payment/wirecard'
        )), 'warning');
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentForm(array $form, array &$form_state) {

  }

  /**
   * Sets request parameters with information returned from the connector.
   *
   * @param RequestBase $request
   *   The request to prepare.
   * @param ContextConnectorInterface $connector
   *   The connector to use.
   *
   * @return array
   *   Parameters array for payment execution. Depends on payment method.
   */
  protected function setRequestParameterFromConnector(RequestBase $request, ContextConnectorInterface $connector) {
    foreach (get_class_vars(get_class($request)) as $name => $default_value) {
      $method_name = 'get' . ucfirst($name);
      // All the dynamic getBasketItem{n}...() methods are handled in __call().
      if (method_exists($connector, $method_name) || strpos($method_name, 'getBasketItem') !== FALSE) {
        $request->{$name} = $connector->$method_name();
      }
    }
  }

  /**
   * Creates a checkout init request object.
   *
   * @param \Payment $payment
   *   The payment to use.
   *
   * @return CheckoutInitRequest
   *   The created request.
   */
  protected function createCheckoutInitRequest(\Payment $payment) {
    $request = CheckoutInitRequest::create($this->paymentType, array(), $this->getRequestName());
    $request->orderIdent = $this->getCurrentOrderIdent($payment);
    $request->storageId = $this->getCurrentStorageId();
    $request->pluginVersion = WirecardMetadata::generatePluginVersion($payment);
    $this->setRequestParameterFromConnector($request, $this->getConnector($payment));
    return $request;
  }

  /**
   * Creates a data storage init request object.
   *
   * @param \Payment $payment
   *   The payment to use.
   *
   * @return DataStorageInitRequest
   *   The created request.
   */
  protected function createDataStorageInitRequest(\Payment $payment) {
    $request = DataStorageInitRequest::create($this->paymentType);
    $request->orderIdent = $this->getCurrentOrderIdent($payment);
    $this->setRequestParameterFromConnector($request, $this->getConnector($payment));
    return $request;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(\Payment $payment) {
    $client = $this->getClient($payment->method);

    try {
      $request = $this->createCheckoutInitRequest($payment);
      $response = $client->initializeCheckout($request);

      entity_save('payment', $payment);
      // Redirect to 3rd party payment page.
      drupal_goto($response->redirectUrl);
    }
    catch (WCErrorException $e) {
      drupal_set_message(t($e->getLogMessage(), $e->getLogContext()), 'error');
      $this->getLogger()->error($e->getLogMessage(), $e->getLogContext(), $this->getPaymentLink($payment), FALSE);
      throw new \PaymentValidationException($e->getLogMessage());
    }
    catch (WCExceptionBase $e) {
      $this->getLogger()->error($e->getLogMessage(), $e->getLogContext(), $this->getPaymentLink($payment), FALSE);
      throw new \PaymentValidationException($e->getLogMessage());
    }
  }

  /**
   * Page callback for the confirm page as posted to Wirecard.
   *
   * @param \Payment $payment
   *   The payment entity.
   */
  public function confirmPage(\Payment $payment) {
    try {
      $this->getLogger()
        ->debug('Confirmation request received for payment %pid.', array('%pid' => $payment->pid), $this->getPaymentLink($payment));
      $response = $this->getClient($payment->method)->handleConfirmation();

      if ($response instanceof CheckoutResultSuccessResponse) {
        $response->verifyFingerPrint($this->getClient($payment->method)->getConfig());

        $this->getLogger()
          ->info('Payment %pid was successful.', array('%pid' => $payment->pid), $this->getPaymentLink($payment));

        // Set success status.
        $success_status = $payment->method->controller_data['status'];
        $payment->setStatus(new \PaymentStatusItem($success_status));
      }
      elseif ($response instanceof CheckoutResultPendingResponse) {
        $response->verifyFingerPrint($this->getClient($payment->method)->getConfig());

        $this->getLogger()
          ->info('Payment %pid is pending.', array('%pid' => $payment->pid), $this->getPaymentLink($payment));

        $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_PENDING));
      }
      elseif ($response instanceof CheckoutResultCancelResponse) {
        $this->getLogger()
          ->info('Payment %pid has been canceled.', array('%pid' => $payment->pid), $this->getPaymentLink($payment));
        $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_CANCELLED));
      }
      else {
        if ($response instanceof CheckoutResultFailureResponse) {
          $payment->context_data['errors'] = $response->errors;
        }
        $this->getLogger()
          ->info('Payment %pid has failed.', array('%pid' => $payment->pid), $this->getPaymentLink($payment));
        $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_FAILED));
      }

      $this->handleResponseData($payment, $response);
      entity_save('payment', $payment);
    }
    catch (WCExceptionBase $e) {
      $this->getLogger()->error($e->getLogMessage(), $e->getLogContext(), $this->getPaymentLink($payment));
      drupal_set_message("Unknown error. Please contact site administrator.", 'error');
    }
    catch (\Exception $e) {
      $this->getLogger()->error($e->getMessage(), array(), $this->getPaymentLink($payment));
    }
  }

  /**
   * Handles the response data, saves results to payment entity.
   *
   * @param \Payment $payment
   *   The payment entity.
   * @param CheckoutResultResponse $response
   *   Response object.
   */
  protected function handleResponseData(\Payment $payment, CheckoutResultResponse $response) {
    if (!empty($response->gatewayReferenceNumber)) {
      $payment->context_data['gateway_reference_number'] = $response->gatewayReferenceNumber;
    }
    if (!empty($response->orderNumber)) {
      $payment->context_data['wirecard_order_number'] = $response->orderNumber;
    }
  }

  /**
   * Page callback for the finish page visited by the consumer.
   *
   * @param \Payment $payment
   *   The payment entity.
   */
  public function finishPage(\Payment $payment) {
    if (payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_FAILED) && !empty($payment->context_data['errors'])) {
      foreach ($payment->context_data['errors'] as $error) {
        if ($error instanceof ResponseError) {
          drupal_set_message($error->consumerMessage, 'error');
        }
      }
    }
    $this->cleanSesssion();
    $payment->finish();
  }

  /**
   * Stores the given order identity.
   *
   * @param int $order_ident
   *   The value to store.
   */
  public function storeCurrentOrderIdent($order_ident) {
    $_SESSION['wirecard_order_id'] = $order_ident;
  }

  /**
   * Gets the stored order identity.
   *
   * If no order identity has been created yet, a unique id is created.
   *
   * @param $payment
   *   (optional) The payment for which to get an order ident. If given, the
   *   order ident of the payment is returned instead of generating a new one.
   *   If the payment has no order ident, a new one is generated anyway.
   *
   * @return string|null
   *   The order identity.
   */
  public function getCurrentOrderIdent($payment = NULL) {
    if (isset($_SESSION['wirecard_order_id'])) {
      if ($payment) {
        // Make sure the payment refers to the same orer ID.
        $payment->context_data['wirecard_orderIdent'] = $_SESSION['wirecard_order_id'];
      }
      return $_SESSION['wirecard_order_id'];
    }
    return $payment ? $this->generateOrderIdent($payment) : NULL;
  }

  /**
   * Generates an order identity and sets it on the payment.
   *
   * If the payment already has an order identiy associated, it is re-used.
   *
   * @param \Payment $payment
   *   The payment for which to get an order ident. If given, the
   *   order ident of the payment is returned instead of generating a new one.
   *   If the payment has no order ident, a new one is generated anyway.
   *
   * @return string
   *   The order identity.
   */
  public function generateOrderIdent(\Payment $payment) {
    if (!isset($payment->context_data['wirecard_orderIdent'])) {
      $payment->context_data['wirecard_orderIdent'] = mt_rand(100000000, 999999999);
    }
    return $payment->context_data['wirecard_orderIdent'];
  }

  /**
   * Stores the given storage id.
   *
   * @param $storage_id
   *   The value to store.
   */
  public function storeCurrentStorageId($storage_id) {
    $_SESSION['wirecard_storage_id'] = $storage_id;
  }

  /**
   * Gets the given storage id.
   *
   * @return string
   */
  public function getCurrentStorageId() {
    return $_SESSION['wirecard_storage_id'];
  }

  /**
   * Cleans all payment method specifc session items.
   */
  public function cleanSesssion() {
    unset($_SESSION['wirecard_storage_id']);
    unset($_SESSION['wirecard_order_id']);
  }

  /**
   * Gets form state values.
   *
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form state values.
   */
  protected function getFormStateValues(array $form_state) {
    $values = array();
    // Get form state values. They could be nested because current form is
    // embedded.
    if (!empty($form_state['values'])) {
      $parents = !empty($form['#parents']) ? $form['#parents'] : array();
      $values = drupal_array_get_nested_value($form_state['values'], $parents);
    }
    return $values;
  }

}
