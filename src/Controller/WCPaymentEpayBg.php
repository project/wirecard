<?php

/**
 * @file
 * Wirecard ePay.bg payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentEpayBg payment method controller.
 */
class WCPaymentEpayBg extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'EPAY_BG';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard ePay.bg');
    $this->description = t('Wirecard ePay.bg payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('ePay.bg');
  }

}
