<?php

/**
 * @file
 * Wirecard Paypal payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard Paypal payment method controller.
 */
class WCPaymentPaypal extends WCPaymentControllerBase {

  /**
   * The wirecard payment method type.
   *
   * @var string
   */
  protected $paymentType = 'PAYPAL';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Paypal');
    $this->description = t('Wirecard Paypal payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Paypal');
  }

}
