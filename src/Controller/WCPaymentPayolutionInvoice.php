<?php

/**
 * @file
 * Wirecard Payolution Invoice payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentInvoice payment method controller.
 */
class WCPaymentPayolutionInvoice extends WCPaymentPayolutionBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'INVOICE';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Payolution Invoice');
    $this->description = t('Wirecard invoice integration by Payolution.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Payolution Invoice');
  }

}
