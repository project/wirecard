<?php

/**
 * @file
 * Wirecard iDEAL payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentIdl payment method controller.
 */
class WCPaymentIdl extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'IDL';

  /**
   * List of iDEAL financial institutions.
   *
   * @var string[]
   */
  protected $financialInstitutions = array(
    'ABNAMROBANK' => 'ABN AMRO Bank',
    'ASNBANK' => 'ASN Bank',
    'INGBANK' => 'ING',
    'KNAB' => 'knab',
    'RABOBANK' => 'Rabobank',
    'SNSBANK' => 'SNS Bank',
    'REGIOBANK' => 'RegioBank',
    'TRIODOSBANK' => 'Triodos Bank',
    'VANLANSCHOT' => 'Van Lanschot Bankiers'
  );

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard iDEAL');
    $this->description = t('Wirecard iDEAL payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('iDEAL');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $form = parent::getPaymentForm($form, $form_state);

    $values = $this->getFormStateValues($form_state);

    $form['financialinstitution'] = array(
      '#type' => 'select',
      '#title' => t('Financial institution'),
      '#options' => $this->financialInstitutions,
      '#default_value' => !empty($values['financialinstitution']) ? $values['financialinstitution'] : '',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentForm(array $form, array &$form_state) {
    parent::validatePaymentForm($form, $form_state);
    if (!form_get_errors()) {
      $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
      $form_state['payment']->context_data['financialinstitution'] = $values['financialinstitution'];
    }
  }

}
