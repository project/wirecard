<?php

/**
 * @file
 * Wirecard SEPA payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard SEPA payment method controller.
 */
class WCPaymentSepa extends WCPaymentControllerBase {

  /**
   * The wirecard payment method type.
   *
   * @var string
   */
  protected $paymentType = 'SEPA-DD';

  /**
   * {@inheritdoc}
   */
  public $requiresSensitiveData = TRUE;

  /**
   * Defaults for test mode.
   *
   * @var string[]
   */
  protected $testDefaults = array(
    'account_owner' => 'John Doe',
    'iban' => 'AT863207300000301101',
    'bic' => 'RLNWATWWBRL',
  );

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard SEPA direct debit');
    $this->description = t('Wirecard SEPA direct debit payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('SEPA direct debit');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $form = parent::getPaymentForm($form, $form_state);

    $values = $this->getFormStateValues($form_state);

    $form['account_owner'] = array(
      '#type' => 'textfield',
      '#title' => t('Account owner'),
      '#default_value' => !empty($values['account_owner']) ? $values['account_owner'] : '',
      '#required' => TRUE,
      '#attributes' => array(
        'id' => 'wirecard-payment-account-owner',
        'autocomplete' => 'off',
      ),
    );
    $form['iban'] = array(
      '#type' => 'textfield',
      '#title' => t('IBAN'),
      '#default_value' => !empty($values['iban']) ? $values['iban'] : '',
      '#required' => TRUE,
      '#attributes' => array(
        'id' => 'wirecard-payment-iban',
        'autocomplete' => 'off',
      ),
    );
    $form['bic'] = array(
      '#type' => 'textfield',
      '#title' => t('BIC'),
      '#default_value' => !empty($values['bic']) ? $values['bic'] : '',
      '#required' => TRUE,
      '#attributes' => array(
        'id' => 'wirecard-payment-bic',
        'autocomplete' => 'off',
      ),
    );

    // Javascript settings for element ids.
    $form['#attached']['js'][] = array(
      'data' => array(
        'wirecardMethodSelectors' => array(
          'accountOwner' => 'wirecard-payment-account-owner',
          'bankAccountIban' => 'wirecard-payment-iban',
          'bankBic' => 'wirecard-payment-bic',
        )
      ),
      'type' => 'setting',
    );

    // Pre-populate fields with test data if test mode is activated.
    if ($form_state['payment']->method->controller_data['test_mode'] != self::PRODUCTION_MODE) {
      foreach ($this->testDefaults as $key => $value) {
        $form[$key]['#description'] = t('Testing mode is enabled. You can use "!value" for testing.', array('!value' => $value));
        if (empty($form[$key]['#default_value'])) {
          $form[$key]['#default_value'] = $value;
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentForm(array $form, array &$form_state) {
    parent::validatePaymentForm($form, $form_state);

    if (!form_get_errors()) {
      $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
      $form_state['payment']->context_data['account_owner'] = $values['account_owner'];
      $form_state['payment']->context_data['iban'] = $values['iban'];
      $form_state['payment']->context_data['bic'] = $values['bic'];
    }
  }

}
