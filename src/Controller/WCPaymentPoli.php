<?php

/**
 * @file
 * Wirecard POLi payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentPoli payment method controller.
 */
class WCPaymentPoli extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'POLI';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard POLi');
    $this->description = t('Wirecard POLi payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('POLi');
  }

}
