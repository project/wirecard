<?php

/**
 * @file
 * Wirecard Ratepay Invoice payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentInvoice payment method controller.
 */
class WCPaymentRatepayInvoice extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'INVOICE';

  /**
   * {@inheritdoc}
   */
  protected $requestName = 'Ratepay';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Ratepay Invoice');
    $this->description = t('Wirecard invoice integration by Ratepay.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Ratepay Invoice');
  }

}
