<?php

/**
 * @file
 * Wirecard Credit Card - MOTO payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard Credit Card - MOTO payment method controller.
 */
class WCPaymentCreditCardMoto extends WCPaymentCreditCard {

  /**
   * The wirecard payment method type.
   *
   * @var string
   */
  protected $paymentType = 'CCARD-MOTO';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Credit Card - MOTO');
    $this->description = t('Wirecard Credit Card - Mail Order and Telephone Order payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Credit Card - MOTO');
  }

}
