<?php

/**
 * @file
 * Wirecard TatraPay payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentTatraPay payment method controller.
 */
class WCPaymentTatraPay extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'TATRAPAY';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard TatraPay');
    $this->description = t('Wirecard TatraPay payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('TatraPay');
  }

}
