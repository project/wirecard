<?php

/**
 * @file
 * Wirecard Paybox payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard Paybox payment method controller.
 */
class WCPaymentPaybox extends WCPaymentControllerBase {

  /**
   * The wirecard payment method type.
   *
   * @var string
   */
  protected $paymentType = 'PBX';

  /**
   * {@inheritdoc}
   */
  public $requiresSensitiveData = TRUE;

  /**
   * Defaults for test mode.
   *
   * @var array
   */
  protected $testDefaults = array(
    'paybox_number' => '0123456789',
  );

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Paybox');
    $this->description = t('Wirecard Paybox payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Paybox');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $form = parent::getPaymentForm($form, $form_state);
    $values = $this->getFormStateValues($form_state);

    $form['paybox_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Mobile phone number'),
      '#default_value' => !empty($values['paybox_number']) ? $values['paybox_number'] : '',
      '#required' => TRUE,
      '#attributes' => array(
        'id' => 'wirecard-payment-paybox-number',
        'autocomplete' => 'off',
      ),
    );

    // Javascript settings for element ids.
    $form['#attached']['js'][] = array(
      'data' => array(
        'wirecardMethodSelectors' => array(
          'payerPayboxNumber' => 'wirecard-payment-paybox-number',
        )
      ),
      'type' => 'setting',
    );

    // Pre-populate fields with test data if test mode is activated.
    if ($form_state['payment']->method->controller_data['test_mode'] != self::PRODUCTION_MODE) {
      foreach ($this->testDefaults as $key => $value) {
        $form[$key]['#description'] = t('Testing mode is enabled. You can use "!value" for testing.', array('!value' => $value));
        if (empty($form[$key]['#default_value'])) {
          $form[$key]['#default_value'] = $value;
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentForm(array $form, array &$form_state) {
    parent::validatePaymentForm($form, $form_state);
    if (!form_get_errors()) {
      $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
      $form_state['payment']->context_data['paybox_number'] = $values['paybox_number'];
    }
  }
}
