<?php

/**
 * @file
 * Wirecard Giropay payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard Giropay payment method controller.
 */
class WCPaymentGiropay extends WCPaymentControllerBase {

  /**
   * The wirecard payment method type.
   *
   * @var string
   */
  protected $paymentType = 'GIROPAY';

  /**
   * {@inheritdoc}
   */
  public $requiresSensitiveData = TRUE;

  /**
   * Defaults for test mode.
   *
   * @var array
   */
  protected $testDefaults = array(
    'account_owner' => 'Jane Doe',
    'bank_number' => '99000001',
    'bank_account' => '1234567890',
  );

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Giropay');
    $this->description = t('Wirecard Giropay payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Giropay');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentForm(array $form, array &$form_state) {
    $form = parent::getPaymentForm($form, $form_state);
    $values = $this->getFormStateValues($form_state);

    $form['account_owner'] = array(
      '#type' => 'textfield',
      '#title' => t('Account owner'),
      '#default_value' => !empty($values['account_owner']) ? $values['account_owner'] : '',
      '#required' => TRUE,
      '#attributes' => array(
        'id' => 'wirecard-payment-account-owner',
        'autocomplete' => 'off',
      ),
    );
    $form['bank_account'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank account'),
      '#default_value' => !empty($values['bank_account']) ? $values['bank_account'] : '',
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
      '#attributes' => array(
        'id' => 'wirecard-payment-bank-account',
        'autocomplete' => 'off',
      ),
    );
    $form['bank_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank number'),
      '#default_value' => !empty($values['bank_number']) ? $values['bank_number'] : '',
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
      '#attributes' => array(
        'id' => 'wirecard-payment-bank-number',
        'autocomplete' => 'off',
      ),
    );

    // Javascript settings for element ids.
    $form['#attached']['js'][] = array(
      'data' => array(
        'wirecardMethodSelectors' => array(
          'accountOwner' => 'wirecard-payment-account-owner',
          'bankAccount' => 'wirecard-payment-bank-account',
          'bankNumber' => 'wirecard-payment-bank-number',
        )
      ),
      'type' => 'setting',
    );

    // Pre-populate fields with test data if test mode is activated.
    if ($form_state['payment']->method->controller_data['test_mode'] != self::PRODUCTION_MODE) {
      foreach ($this->testDefaults as $key => $value) {
        $form[$key]['#description'] = t('Testing mode is enabled. You can use "!value" for testing.', array('!value' => $value));
        if (empty($form[$key]['#default_value'])) {
          $form[$key]['#default_value'] = $value;
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentForm(array $form, array &$form_state) {
    parent::validatePaymentForm($form, $form_state);
    if (!form_get_errors()) {
      $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
      $form_state['payment']->context_data['account_owner'] = $values['account_owner'];
      $form_state['payment']->context_data['bank_account'] = $values['bank_account'];
      $form_state['payment']->context_data['bank_number'] = $values['bank_number'];
    }
  }
}
