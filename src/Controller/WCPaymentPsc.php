<?php

/**
 * @file
 * Wirecard Paysafecard payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentPsc payment method controller.
 */
class WCPaymentPsc extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'PSC';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard Paysafecard');
    $this->description = t('Wirecard Paysafecard payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('Paysafecard');
  }

}
