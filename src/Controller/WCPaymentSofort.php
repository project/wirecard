<?php

/**
 * @file
 * Wirecard SOFORT Banking payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentSofort payment method controller.
 */
class WCPaymentSofort extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'SOFORTUEBERWEISUNG';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard SOFORT Banking');
    $this->description = t('Wirecard SOFORT Banking payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('SOFORT Banking');
  }

}
