<?php

/**
 * @file
 * Wirecard eKonto payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard WCPaymentEkonto payment method controller.
 */
class WCPaymentEkonto extends WCPaymentControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $paymentType = 'EKONTO';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Wirecard eKonto');
    $this->description = t('Wirecard eKonto payment integration.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleGeneric() {
    return t('eKonto');
  }

}
