<?php
/**
 * @file
 * Wirecard payment method.
 */

namespace Drupal\wirecard\Controller;

/**
 * Wirecard payment method controller.
 */
interface WCPaymentControllerInterface {

  /**
   * Returns payment method human readable name.
   *
   * @return string
   *   Payment method human readable name.
   */
  public function getTitleGeneric();

  /**
   * Provides configuration form for payment method.
   *
   * @param array $form
   *   Form elements.
   * @param array $form_state
   *   Form state.
   *
   * @return array
   *   Configuration form elements.
   */
  public function getConfigurationForm(array $form, array &$form_state);

  /**
   * Validates configuration form for payment method.
   *
   * @param array $form
   *   Form elements.
   * @param array $form_state
   *   Form state.
   */
  public function validateConfigurationForm(array $form, array &$form_state);

  /**
   * Provides payment form for payment method.
   *
   * @param array $form
   *   Form elements.
   * @param array $form_state
   *   Form state.
   *
   * @return array
   *   Payment form elements.
   */
  public function getPaymentForm(array $form, array &$form_state);

  /**
   * Validates payment form for payment method.
   *
   * @param array $form
   *   Form elements.
   * @param array $form_state
   *   Form state.
   */
  public function validatePaymentForm(array $form, array &$form_state);

  /**
   * Gets Wirecard payment API client.
   *
   * @param \PaymentMethod $payment_method
   *   Payment method entity.
   *
   * @return \Drupal\wirecard\Client\WCSeamlessAPIClient
   *   Wirecard payment API client object.
   */
  public function getClient(\PaymentMethod $payment_method);

  /**
   * Gets the connector object for a payment.
   *
   * Returns the right connector depending on the payment context.
   *
   * @param \Payment $payment
   *   Payment entity.
   * @param \PaymentMethod $payment_method
   *   (optional) Payment method entity. If not set - payment method from
   *   $payment entity is used.
   *
   * @return \Drupal\wirecard\ContextConnector\ContextConnectorInterface
   *   The wirecard context connector to be used for the payment.
   */
  public function getConnector(\Payment $payment, \PaymentMethod $payment_method = NULL);

  /**
   * Get all required parameters that exist in mapping.
   *
   * @param string $connector_name
   *   Connector name.
   *
   * @return string[]
   *   Array of required mappings.
   */
  public function getRequiredMappings($connector_name);

  /**
   * Gets missing mappings from Wirecard connector for the current controller.
   *
   * Checks all the required parameters for the current Wirecard  payment
   * controller against Wirecard properties mapping.
   *
   * @return array[]
   *   Array of missing mappings:
   *   - Key (string): connector name.
   *   - Value (array): strings array with properties names that miss mapping.
   */
  public function getMissingMappings();

}
