<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessRatepayResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Base class for response of checkout process by Wirecard for Ratepay method.
 *
 * @see CheckoutInitRatepayRequest
 */
class CheckoutResultSuccessRatepayResponse extends CheckoutResultSuccessResponse {

  /**
   * Reference number of invoice or installment.
   *
   * @var string
   */
  public $gatewayReferenceNumber;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'gatewayReferenceNumber',
    ));
  }
}
