<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessVoucherResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Response of checkout process by Wirecard for Voucher method.
 *
 * @see CheckoutInitRequest
 */
class CheckoutResultSuccessVoucherResponse extends CheckoutResultSuccessResponse {

  /**
   * Unique voucher identification code available on consumer´s voucher.
   *
   * @var string
   */
  public $voucherId;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'voucherId',
    ));
  }
}
