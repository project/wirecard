<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultCancelResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Cancelled checkout result response.
 */
class CheckoutResultCancelResponse extends CheckoutResultResponse {

}
