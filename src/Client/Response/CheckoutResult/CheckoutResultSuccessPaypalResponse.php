<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessPaypalResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Response of checkout process by Wirecard for PayPal method.
 *
 * @see CheckoutInitRequest
 */
class CheckoutResultSuccessPaypalResponse extends CheckoutResultSuccessResponse {

  /**
   * Transaction ID of authorization.
   *
   * @var string
   */
  public $gatewayReferenceNumber;

  /**
   * Billing agreement ID returned by PayPal.
   *
   * @var string
   */
  public $paypalBillingAgreementID;

  /**
   *  City of shipping address.
   *
   * @var string
   */
  public $paypalPlayerAddressCity;

  /**
   * Country of shipping address.
   *
   * @var string
   */
  public $paypalPayerAddressCountry;

  /**
   * Country code (ISO 3166-1) of shipping address.
   *
   * @var string
   */
  public $paypalPayerAddressCountryCode;

  /**
   *  Name of shipping address.
   *
   * @var string
   */
  public $paypalPayerAddressName;

  /**
   * State of shipping address.
   *
   * @var string
   */
  public $paypalPayerAddressState;

  /**
   * Street line 1 of shipping address.
   *
   * @var string
   */
  public $paypalPayerAddressStreet1;

  /**
   * Street line 2 of shipping address.
   *
   * @var string
   */
  public $paypalPayerAddressStreet2;

  /**
   * ZIP code of shipping address.
   *
   * @var string
   */
  public $paypalPayerAddressZIP;

  /**
   * E-mail address of consumer as returned by PayPal.
   *
   * @var string
   */
  public $paypalPayerEmail;

  /**
   * First name of consumer as returned by PayPal.
   *
   * @var string
   */
  public $paypalPayerFirstName;

  /**
   * ID of consumer as returned by PayPal.
   *
   * @var string
   */
  public $paypalPayerID;

  /**
   * Last name of consumer as returned by PayPal
   *
   * @var string
   */
  public $paypalPayerLastName;


  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'gatewayReferenceNumber',
      'paypalBillingAgreementID',
      'paypalPlayerAddressCity',
      'paypalPayerAddressCountry',
      'paypalPayerAddressName',
      'paypalPayerAddressState',
      'paypalPayerAddressStreet1',
      'paypalPayerAddressStreet2',
      'paypalPayerAddressZIP',
      'paypalPayerEmail',
      'paypalPayerFirstName',
      'paypalPayerID',
      'paypalPayerLastName',
    ));
  }
}
