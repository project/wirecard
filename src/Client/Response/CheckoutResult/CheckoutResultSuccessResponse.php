<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

use Drupal\wirecard\Client\Response\FingerPrintedResponseInterface;
use Drupal\wirecard\Client\WCClientConfig;
use Drupal\wirecard\Util\FingerPrint;

/**
 * Successful checkout result response.
 */
class CheckoutResultSuccessResponse extends CheckoutResultResponse implements FingerPrintedResponseInterface {

  /**
   * Financial institution.
   *
   * Based on pre-selected payment method a sub-selection of financial
   * institutions regarding to pre-selected payment method.
   *
   * @var string
   */
  public $financialInstitution;

  /**
   * Language used for displayed texts on payment page.
   *
   * @var string
   */
  public $language;

  /**
   * Unique number identifying the payment.
   *
   * @var string
   */
  public $orderNumber;

  /**
   * Selected payment method of your consumer.
   *
   * @var string
   */
  public $paymentType;

  /**
   * Returned fingerprint.
   *
   * Returned fingerprint of the parameter values as given in the
   * requestFingerprintOrder.
   *
   * @var string
   */
  public $responseFingerprint;

  /**
   * Ordered list of parameters used for calculating the fingerprint.
   *
   * @var string
   */
  public $responseFingerprintOrder;

  /**
   * Amount of payment.
   *
   * @var string
   */
  public $amount;

  /**
   * Currency code of amount.
   *
   * @var string
   */
  public $currency;

  /**
   * Contract number of the processor or acquirer.
   *
   * @var string
   */
  public $gatewayContractNumber;

  /**
   * Reference number of the processor or acquirer.
   *
   * @var string
   */
  public $gatewayReferenceNumber;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'financialInstitution',
      'language',
      'orderNumber',
      'paymentType',
      'responseFingerprint',
      'responseFingerprintOrder',
      // Gateway properties are documented to be there, but apparently not
      // always there.
    ));
  }


  /**
   * {@inheritdoc}
   */
  public function verifyFingerPrint(WCClientConfig $config) {
    FingerPrint::verifyFingerPrint($this, $config);
  }

}
