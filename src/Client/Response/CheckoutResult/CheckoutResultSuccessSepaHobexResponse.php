<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessSepaHobexResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Response of checkout process by Wirecard for Payolution method.
 *
 * @see CheckoutInitSepaHobexRequest
 */
class CheckoutResultSuccessSepaHobexResponse extends CheckoutResultSuccessResponse {

  /**
   * Unique identification of creditor (always Hobex).
   *
   * @var string
   */
  public $creditorId;

  /**
   * Date when payment is debited from consumer's account(calculated by Hobex).
   *
   * @var string
   */
  public $dueDate;

  /**
   * Identifier of the displayed mandate.
   *
   * If request parameter mandateId is not set we will generate the mandate ID.
   *
   * @var string
   */
  public $mandateId;

  /**
   * Date when the mandate was signed by the consumer.
   *
   * If not given by the merchant we use the current date as signature date.
   *
   * @var string
   */
  public $mandateSignatureDate;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'creditorId',
      'dueDate',
      'mandateId',
      'mandateSignatureDate',
    ));
  }
}
