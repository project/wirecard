<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultFailureResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Failing checkout result response.
 */
class CheckoutResultFailureResponse extends CheckoutResultResponse {

  /**
   * The errors of the response.
   *
   * @var \Drupal\wirecard\Client\Response\ResponseError[]
   */
  public $errors = array();
}
