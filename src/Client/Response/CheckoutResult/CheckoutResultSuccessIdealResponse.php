<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessIdealResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Response of checkout process by Wirecard for Ideal method.
 *
 * @see CheckoutInitRequest
 */
class CheckoutResultSuccessIdealResponse extends CheckoutResultSuccessResponse {

  /**
   * IBAN of account.
   *
   * @var string
   */
  public $idealConsumerIBAN;

  /**
   * BIC of bank.
   *
   * @var string
   */
  public $idealConsumerBIC;

  /**
   * Name of account owner as returned by iDEAL issuer.
   *
   * @var string
   */
  public $idealConsumerName;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'idealConsumerIBAN',
      'idealConsumerBIC',
      'idealConsumerName',
    ));
  }
}
