<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessSofortResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Response of checkout process by Wirecard for Sofort method.
 *
 * @see CheckoutInitRequest
 */
class CheckoutResultSuccessSofortResponse extends CheckoutResultSuccessResponse {

  /**
   * Security criterion returned by sofortüberweisung.
   *
   * ”1” means the criteria were met and ”0” means criteria were not met, in the
   * later case you should wait until the amount has been received by you before
   * sending goods or grant access to digital media.
   *
   * @var int
   */
  public $securityCriteria;

  /**
   * Account number of sender as returned by sofortüberweisung.
   *
   * @var int
   */
  public $senderAccountNumber;

  /**
   * Owner of account as returned by sofortüberweisung.
   *
   * @var string
   */
  public $senderAccountOwner;

  /**
   * Name of bank as returned by sofortüberweisung.
   *
   * @var string
   */
  public $senderBankName;

  /**
   * Number of bank as returned by sofortüberweisung.
   *
   * @var int
   */
  public $senderBankNumber;

  /**
   * BIC of sender as returned by sofortüberweisung.
   *
   * @var string
   */
  public $senderBIC;

  /**
   * Country code of sender as returned by sofortüberweisung.
   *
   * @var string
   */
  public $senderCountry;

  /**
   * IBAN of sender as returned by sofortüberweisung.
   *
   * @var string
   */
  public $senderIBAN;


  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'securityCriteria',
      'senderAccountNumber',
      'senderAccountOwner',
      'senderBankName',
      'senderBankNumber',
      'senderBIC',
      'senderCountry',
      'senderIBAN',
    ));
  }
}
