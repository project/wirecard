<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessPayolutionResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Base class for response of checkout process for Payolution method.
 *
 * @see CheckoutInitPayolutionRequest
 */
class CheckoutResultSuccessPayolutionResponse extends CheckoutResultSuccessResponse {

  /**
   * Reference number of invoice or installment.
   *
   * @var string
   */
  public $gatewayReferenceNumber;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'gatewayReferenceNumber',
    ));
  }
}
