<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultSuccessCcardResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Response of checkout process by Wirecard for Credit Card method.
 *
 * @see CheckoutInitRequest
 */
class CheckoutResultSuccessCcardResponse extends CheckoutResultSuccessResponse {

  /**
   * Last four digits of the credit card number.
   *
   * @var int
   */
  public $anonymousPan;

  /**
   * Shows if the card holder has been successfully authenticated.
   *
   * Returns ”YES” if the card holder has been successfully authenticated,
   * otherwise ”NO”.
   *
   * @var string
   */
  public $authenticated;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'anonymousPan',
      'authenticated',
    ));
  }
}
