<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Response\CheckoutResultSuccessSepaWirecardBankResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

/**
 * Response of checkout process by Wirecard for SEPA and Wirecard Bank method.
 *
 * @see CheckoutResultSuccessSepaWirecardBankResponse
 */
class CheckoutResultSuccessSepaWirecardBankResponse extends CheckoutResultSuccessResponse {

  /**
   * Unique identifier of creditor (merchant).
   *
   * @var string
   */
  public $creditorId;

  /**
   * Date when payment is debited from consumer´s bank account.
   *
   * @var string
   */
  public $dueDate;

  /**
   * Identifier of displayed mandate.
   *
   * If request parameter mandateId is not set we will generate the mandate ID.
   *
   * @var string
   */
  public $mandateId;

  /**
   * Date when the mandate was signed by the consumer.
   *
   * If not given by the merchant we use the current date as signature date.
   *
   * @var string
   */
  public $mandateSignatureDate;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'creditorId',
      'dueDate',
      'mandateId',
      'mandateSignatureDate',
    ));
  }
}
