<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultResponse
 */

namespace Drupal\wirecard\Client\Response\CheckoutResult;

use Drupal\wirecard\Client\Response\ResponseBase;
use Drupal\wirecard\Client\Exception\RequestException;

/**
 * Response of the checkout process posted to use by Wirecard.
 */
abstract class CheckoutResultResponse extends ResponseBase {

  /**
   * Result of checkout process: “SUCCESS”, “CANCEL”, “FAILURE” or “PENDING”.
   *
   * @var string
   */
  public $paymentState;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array('paymentState');
  }

  /**
   * {@inheritdoc}
   */
  public static function parse(array $response) {
    if (!isset($response['paymentState'])) {
      throw new RequestException("Malformed request.");
    }
    if ($response['paymentState'] == 'FAILURE') {
      $response['errors'] = static::parseErrors($response);
    }
    $class = str_replace('ResultResponse', 'Result' . static::toCamelCase($response['paymentState']) . 'Response', __CLASS__);
    return new $class($response);
  }

}
