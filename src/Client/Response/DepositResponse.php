<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\ResponseBackEndOperationsBase
 */

namespace Drupal\wirecard\Client\Response;

/**
 * Represents the HTTP response of the DataStorageInit request.
 *
 * @see DataStorageInitRequest
 */
class DepositResponse extends ResponseBase {

  /**
   * Payment number.
   *
   * Returned if a new payment object has been created due to a partial deposit.
   *
   * @var int
   */
  public $paymentNumber;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'paymentNumber',
    ));
  }
}
