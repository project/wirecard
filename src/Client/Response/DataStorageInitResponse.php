<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\DataStorageInitResponse
 */

namespace Drupal\wirecard\Client\Response;

/**
 * Represents the HTTP response of the DataStorageInit request.
 *
 * @see DataStorageInitRequest
 */
class DataStorageInitResponse extends ResponseBase {

  /**
   * URL to a JavaScript.
   *
   * URL to a JavaScript resource which have to be included for using the
   * storage operations of the data storage.
   *
   * @var string
   */
  public $javascriptUrl;

  /**
   * Unique reference of the data storage for a consumer.
   *
   * @var string
   */
  public $storageId;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'javascriptUrl',
      'storageId',
    ));
  }
}
