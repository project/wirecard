<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\CheckoutInitResponse
 */

namespace Drupal\wirecard\Client\Response;

/**
 * Represents the HTTP response of the DataStorageInit request.
 *
 * @see CheckoutInitRequest
 */
class CheckoutInitResponse extends ResponseBase {

  /**
   * URL to redirect your consumer.
   *
   * @var string
   */
  public $redirectUrl;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'redirectUrl',
    ));
  }
}
