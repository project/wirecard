<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\ResponseBase
 */

namespace Drupal\wirecard\Client\Response;

use Drupal\wirecard\Client\Exception\ResponseException;
use Drupal\wirecard\Client\Exception\WCErrorException;
use Drupal\wirecard\Util\ModelBase;

/**
 * Base class for all WCClient responses.
 */
abstract class ResponseBase extends ModelBase {

  /**
   * Constructs the object.
   *
   * @param array $data
   *   The response data represented as array.
   *
   * @throws ResponseException
   */
  public function __construct(array $data) {
    // Check required properties are there and set values.
    foreach ($this->getRequiredProperties() as $key) {
      if (!isset($data[$key])) {
        $key = check_plain($key);
        throw ResponseException::create("Required response property $key missing for response class " . get_called_class(), $data);
      }
      else {
        $this->{$key} = $data[$key];
        unset($data[$key]);
      }
    }
    foreach ($data as $key => $value) {
      // Ignore bogus entries with empty keys - unfortunately they exist.
      if ($key) {
        $this->{$key} = $data[$key];
      }
    }
  }

  /**
   * Defines the properties that are required for a response.
   *
   * @return string[]
   *   An array of required property names.
   */
  protected function getRequiredProperties() {
    return array();
  }

  /**
   * Parses response data.
   *
   * @param array $response
   *   The response data.
   *
   * @return static
   *   The response.
   *
   * @throws WCErrorException
   *   Thrown if errors occurred.
   */
  public static function parse(array $response) {
    if (isset($response['errors'])) {
      throw new WCErrorException(static::parseErrors($response));
    }
    else {
      return new static($response);
    }
  }

  /**
   * Parses errors.
   *
   * This method assumes the response contains errors.
   *
   * @param array $response
   *   The response.
   *
   * @return ResponseError[]
   */
  public static function parseErrors(array $response) {
    $errors = array();
    for ($i = 1; $i <= $response['errors']; $i++) {
      $errors[$i] = new ResponseError($response["error.$i.errorCode"], $response["error.$i.message"], $response["error.$i.consumerMessage"]);
    }
    return $errors;
  }

}
