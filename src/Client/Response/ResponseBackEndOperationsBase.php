<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\ResponseBackEndOperationsBase
 */

namespace Drupal\wirecard\Client\Response;

use Drupal\wirecard\Client\Exception\ResponseException;
use Drupal\wirecard\Client\Exception\WCBackendOperationException;

/**
 * Base class for all Wirecard back-end operations responses.
 */
abstract class ResponseBackEndOperationsBase extends ResponseBase {

  /**
   * Number representing the status of the operation.
   *
   * Value of parameter status:
   *  - 0: Operation successfully done.
   *  - 1: Error during request parameter verification.
   *  - 2: Execution of operation denied.
   *  - 3: Error within external financial institution.
   *  - 4: Internal error.
   *
   * @var int
   */
  public $status;

  /**
   * Parses response data.
   *
   * @param array $response
   *   The response data.
   *
   * @return static
   *   The response.
   *
   * @throws WCBackendOperationException
   *   Thrown if errors occurred.
   * @throws ResponseException
   *   Thrown if errors occurred.
   * @throws WCBackendOperationException
   *   Thrown if errors occurred.
   */
  public static function parse(array $response) {
    parent::parse($response);
    if (!isset($response['status'])) {
      throw ResponseException::create("Missing status on back-end operation response.", $response);
    }
    if ($response['status'] != 0) {
      throw WCBackendOperationException::create($response['status'], $response);
    }
    else {
      return new static($response);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getRequiredProperties() {
    return array_merge(parent::getRequiredProperties(), array(
      'status',
    ));
  }
}
