<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\ApproveReversalResponse
 */

namespace Drupal\wirecard\Client\Response;

/**
 * Represents the HTTP response of the DataStorageInit request.
 *
 * @see DataStorageInitRequest
 */
class ApproveReversalResponse extends ResponseBackEndOperationsBase {

}
