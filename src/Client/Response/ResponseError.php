<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\ResponseError.
 */

namespace Drupal\wirecard\Client\Response;

/**
 * Models wirecard response errors.
 */
class ResponseError {

  /**
   * Numeric error code which you should log for later use.
   *
   * @var string
   */
  public $errorCode;

  /**
   * Error message in English.
   *
   * @var string
   */
  public $message;

  /**
   * Error message in localized language for your consumer.
   *
   * @var string
   */
  public $consumerMessage;

  /**
   * Constructs the object.
   */
  public function __construct($code, $message, $consumer_message) {
    $this->errorCode = $code;
    $this->message = $message;
    $this->consumerMessage = $consumer_message;
  }

}
