<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Response\FingerPrintedResponseInterface
 */

namespace Drupal\wirecard\Client\Response;

use Drupal\wirecard\Client\Exception\ResponseException;
use Drupal\wirecard\Client\WCClientConfig;

/**
 * Interface for response having finger prints.
 *
 * This interface requires that there are the
 *  - responseFingerprintOrder
 *  - responseFingerprint
 * properties on the object.
 */
interface FingerPrintedResponseInterface {

  /**
   * Verifies the fingerprint and throws an exception if it's invalid.
   *
   * @param WCClientConfig $config
   *   The wirecard config.
   *
   * @throws ResponseException
   */
  public function verifyFingerPrint(WCClientConfig $config);

}
