<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\WCClientConfig.
 */

namespace Drupal\wirecard\Client;

/**
 * Represents the Wirceard Client configuration.
 */
class WCClientConfig {

  /**
   * The wirecard payment method / type name.
   *
   * @var string
   */
  public $paymentType;

  /**
   * Personal customer id obtained by Wirecard. E.g. D200001.
   *
   * @var string
   */
  public $customerId;

  /**
   * Personal secret obtained by Wirecard.
   *
   * Pre-shared key, used to sign the transmitted data.
   * E.g. B8AKTPWBRMNBV455FG6M2DANE99WU2.
   *
   * @var string
   */
  public $secret;

  /**
   * Shop id. Use only if it is enabled by Wirecard. E.g. qmore.
   *
   * @var NULL|string
   */
  public $shopId;

  /**
   * Language code for Wirecard checkout. E.g. en.
   *
   * @var string
   */
  public $language;

  /**
   * (optional) Wirecard password for back-end operations. E.g. jcv45z
   *
   * Make sure to enter a password, if the Wirecard backend operations should be
   * used via API or the Wirecard Rules module.
   *
   * @var null|string
   */
  public $password;

  /**
   * URL of your service page containing contact information.
   *
   * @var string
   */
  public $serviceUrl;

  /**
   * Creates configuration by applying defaults from the current user.
   *
   * @param array $values
   *   (optional) Initialize from an array of values.
   *
   * @return static
   */
  public static function create(array $values = array()) {
    return new static($values);
  }

  /**
   * Constructs the object.
   *
   * @param array $values
   *   (optional) Initialize from an array of values.
   */
  public function __construct(array $values = array()) {
    // Set properties from config.
    foreach ($values as $key => $value) {
      if (property_exists(__CLASS__, $key)) {
        $this->{$key} = $value;
      }
      else {
        $key = check_plain($key);
        throw new \InvalidArgumentException("Unkown key $key in client configuration.");
      }
    }
  }

  /**
   * Returns an array representation of the config.
   *
   * @return array
   */
  public function toArray() {
    return get_object_vars($this);
  }

}
