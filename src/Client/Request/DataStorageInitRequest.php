<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\DataStorageInitRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to the initialize the data storage.
 *
 * @see DataStorageInitResponse
 */
class DataStorageInitRequest extends RequestBase {

  /**
   * Unique ID of merchant.
   *
   * @var string
   */
  public $customerId;

  /**
   * (optional) Unique ID of your online shop.
   *
   * @var string
   */
  public $shopId;

  /**
   * Unique reference to the order of your consumer.
   *
   * @var int
   */
  public $orderIdent;

  /**
   * Return URL for outdated browsers.
   *
   * @var string
   */
  public $returnUrl;

  /**
   * Language code for returned texts and error messages.
   *
   * @var string
   */
  public $language;

  /**
   * Computed fingerprint of the parameter values and the secret.
   *
   * @var string
   */
  public $requestFingerprint;

  /**
   * (optional) Version number of JavaScript.
   *
   * @var string
   */
  public $javascriptScriptVersion;

  /**
   * {@inheritdoc}
   */
  public function getFingerPrintPropertyNames() {
    return array(
      'customerId',
      'shopId',
      'orderIdent',
      'returnUrl',
      'language',
      'javascriptScriptVersion',
      'secret'
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array(
      'customerId',
      'orderIdent',
      'returnUrl',
      'language',
      'requestFingerprint',
    );
  }

  /**
   * Creates request instance according to payment type.
   *
   * @param string $paymentType
   *   Payment type. E.g. Sepa, Ccard etc.
   * @param array $values
   *   (optional) Array with required request parameters
   *
   * @return static DataStorageInitRequest
   *   Request instance.
   *
   * @see initializeDataStorage()
   */
  public static function create($paymentType, $values = array()) {
    $class = __NAMESPACE__ . '\DataStorageInit' . static::toCamelCase($paymentType) . 'Request';
    if (!class_exists($class)) {
      $class = __NAMESPACE__ . '\DataStorageInitRequest';
    }
    return new $class($values);
  }

}
