<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\ApproveReversalRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to cancel an approval.
 *
 * @see ApproveReversalResponse
 */
class ApproveReversalRequest extends RequestBase {

  /**
   * Unique ID of merchant.
   *
   * @var string
   */
  public $customerId;

  /**
   * (optional) Unique ID of your online shop.
   *
   * @var string
   */
  public $shopId = NULL;

  /**
   * Wirecard password for back-end operations.
   *
   * @var string
   */
  public $password;

  /**
   * Language code for returned texts and error messages.
   *
   * @var string
   */
  public $language;

  /**
   * Computed fingerprint of the parameter values and the secret.
   *
   * @var string
   */
  public $requestFingerprint;

  /**
   * Unique reference to the order of your consumer.
   *
   * @var int
   */
  public $orderNumber;

  /**
   * {@inheritdoc}
   */
  public function getFingerPrintPropertyNames() {
    return array(
      'customerId',
      'shopId',
      'password',
      'secret',
      'language',
      'orderNumber',
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array(
      'customerId',
      'shopId',
      'password',
      'language',
      'requestFingerprint',
      'orderNumber',
    );
  }

}
