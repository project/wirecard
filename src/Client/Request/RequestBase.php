<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\RequestBase
 */

namespace Drupal\wirecard\Client\Request;

use Drupal\wirecard\Util\ModelBase;

/**
 * Base class for all WCClient requests.
 */
abstract class RequestBase extends ModelBase {

  /**
   * Returns the properties required by request.
   *
   * @return string[]
   *   An array of required property names.
   * @return RequestBase
   *   (optional) Request object.
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array();
  }

  /**
   * Returns the properties to be fingerprinted in the right order.
   *
   * @return string[]
   *   An array of properties to fingerprint, in the right order.
   */
  public function getFingerPrintPropertyNames() {
    // Get request property values except requestFingerprint.
    // In some requests elements order is very important. Make sure all the
    // properties of request class are declared in the right fingerprint order.
    $properties = $this->toArray();
    unset($properties['requestFingerprint']);
    return array_merge(array_keys($properties), array('secret'));
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    // Get request property values.
    $properties = get_object_vars($this);
    foreach ($properties as $key => $value) {
      // Empty non-required properties are skipped.
      if (!in_array($key, static::getRequiredProperties($this)) && $value === NULL) {
        unset($properties[$key]);
      }
    }
    return $properties;
  }
}
