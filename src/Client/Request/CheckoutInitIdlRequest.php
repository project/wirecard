<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitIdlRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize data storage for "iDEAL" method.
 *
 * @see CheckoutResultSuccessPayolutionResponse
 */
class CheckoutInitIdlRequest extends CheckoutInitRequest {

  /**
   * Financial institutions regarding to pre-selected payment method.
   *
   * Based on pre-selected payment method a sub-selection of financial
   * institutions regarding to pre-selected payment method.
   *
   * @var string
   */
  public $financialInstitution;

}
