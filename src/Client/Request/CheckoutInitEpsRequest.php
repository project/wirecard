<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitEpsRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize data storage for "eps" method.
 *
 * @see CheckoutResultSuccessPayolutionResponse
 */
class CheckoutInitEpsRequest extends CheckoutInitRequest {

  /**
   * Financial institutions regarding to pre-selected payment method.
   *
   * Based on pre-selected payment method a sub-selection of financial
   * institutions regarding to pre-selected payment method.
   *
   * @var string
   */
  public $financialInstitution;

}
