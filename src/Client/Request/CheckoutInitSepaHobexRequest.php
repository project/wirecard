<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Request\CheckoutInitSepaHobexRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize checkout for SEPA and Hobex method.
 *
 * @see CheckoutResultSuccessSepaHobexResponse
 */
class CheckoutInitSepaHobexRequest extends CheckoutInitRequest {

  /**
   * Identifier of displayed mandate.
   *
   * @var string
   */
  public $mandateId;

  /**
   * Date when mandate was signed.
   *
   * @var string
   */
  public $mandateSignatureDate;

}
