<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\DataStorageInitRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to the initialize the data storage.
 *
 * @see DataStorageInitResponse
 */
class DataStorageInitCcardRequest extends DataStorageInitRequest {

  /**
   * (optional) URL for style sheet to format the credit card input fields.
   *
   * Please be aware that @import, url() are ignored for security reasons.
   *
   * @var NULL|string
   */
  public $iframeCssUrl;

  /**
   * (optional) Enables/disables display of CVC field.
   *
   * @var bool
   */
  public $creditcardShowCvcField;

  /**
   * (optional) Enables/disables display of card holder name field.
   *
   * @var bool
   */
  public $creditcardShowCardholderNameField;

  /**
   * (optional) Enables/disables display of card issue date field.
   *
   * @var bool
   */
  public $creditcardShowIssueDateField;

  /**
   * (optional) Enables/disables display of card issue number field.
   *
   * @var bool
   */
  public $creditcardShowIssueNumberField;

  /**
   * (optional) Javascript version.
   *
   * The value pci3 must be set for this parameter to allow the input of credit
   * card related data in the displayed iframe. Alphanumeric with a fixed length
   * of 4.
   *
   * @var string
   */
  public $javascriptScriptVersion;

}
