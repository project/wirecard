<?php
/**
 * @file
 * Contains Drupal\wirecard\Client\Request\CheckoutInitSepaWirecardBankRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize checkout for SEPA and Wirecard Bank.
 *
 * @see CheckoutResultSuccessPayolutionResponse
 */
class CheckoutResultSuccessSepaWirecardBankResponse extends CheckoutInitRequest {

  /**
   * (optional) Unique identifier of creditor (merchant).
   *
   * @var string
   */
  public $creditorId;

  /**
   * (optional) Date when payment is debited from consumer's bank account.
   *
   * The due date is calculated by merchant or, if the field is left empty,
   * Wirecard will automatically calculate the due date.
   *
   * @var string
   */
  public $dueDate;

  /**
   * (optional) Identifier of displayed mandate.
   *
   * @var string
   */
  public $mandateId;

  /**
   * (optional) Date when mandate was signed.
   *
   * @var string
   */
  public $mandateSignatureDate;

}
