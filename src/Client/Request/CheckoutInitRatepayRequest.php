<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitRatepayRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Base class for request to initialize checkout for Ratepay method.
 *
 * @see CheckoutResultSuccessRatepayResponse
 */
class CheckoutInitRatepayRequest extends CheckoutInitRequest {

  /**
   * First name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingFirstname;

  /**
   * Last name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingLastname;

  /**
   * Billing address line 1 (name of street and house number).
   *
   * Alphanumeric with a variable length of up to 100 bytes.
   *
   * @var string
   */
  public $consumerBillingAddress1;

  /**
   * Billing address line 2
   *
   * Alphanumeric with a variable length of up to 100 bytes. Optionally the
   * house number if not already set within consumerBillingAddress1.
   *
   * @var string
   */
  public $consumerBillingAddress2;

  /**
   * Billing city. Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingCity;

  /**
   * Billing country code (ISO 3166-1). Alphabetic with a fixed length of 2.
   *
   * @var string
   */
  public $consumerBillingCountry;

  /**
   * Billing zip code. Alphanumeric with a variable length of up to 12.
   *
   * @var string
   */
  public $consumerBillingZipCode;

  /**
   * Phone number of consumer.
   *
   * Numeric with special characters and a fixed length of 10.
   *
   * @var string
   */
  public $consumerBillingPhone;

  /**
   * E-mail address of consumer.
   *
   * Alphanumeric with special characters and a variable length of up to 256.
   *
   * @var string
   */
  public $consumerEmail;

  /**
   * Birth date of consumer in the format YYYY-MM-DD.
   *
   * Numeric with special characters and a fixed length of 10.
   *
   * @var string
   */
  public $consumerBirthDate;

  /**
   * Overall amount of shopping cart.
   *
   * @var float
   */
  public $basketAmount;

  /**
   * Code of currency based on ISO 4217.
   *
   * Alphabetic with a fixed length of 3 or numeric with a fixed length of 3.
   *
   * @var string
   */
  public $basketCurrency;

  /**
   * Number of items in shopping cart.
   *
   * @var int
   */
  public $basketItems;

  /**
   * Unique ID of article n in shopping cart.
   *
   * Alphanumeric with special characters.
   * 1 - running number starting with 1 and up to the number of basket items.
   *
   * @var string
   */
  public $basketItem1ArticleNumber;

  /**
   * Product description of article n in shopping cart.
   *
   * Alphanumeric with special characters.
   * 1 - running number starting with 1 and up to the number of basket items.
   *
   * @var string
   */
  public $basketItem1Description;

  /**
   * Items count of article n in shopping cart.
   *
   * 1 - running number starting with 1 and up to the number of basket items.
   *
   * @var int
   */
  public $basketItem1Quantity;

  /**
   * Tax amount of article n in shopping cart.
   *
   * 1 - running number starting with 1 and up to the number of basket items.
   *
   * @var float
   */
  public $basketItem1Tax;

  /**
   * Price per unit of article n in shopping cart without taxes.
   *
   * 1 - running number starting with 1 and up to the number of basket items.
   *
   * @var float
   */
  public $basketItem1UnitPrice;

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    $parameters = array_merge(parent::getRequiredProperties(), array(
      'consumerBillingFirstname',
      'consumerBillingLastname',
      'consumerBillingAddress1',
      'consumerBillingAddress2',
      'consumerBillingCity',
      'consumerBillingCountry',
      'consumerBillingZipCode',
      'consumerEmail',
      'consumerBirthDate',
      'basketAmount',
      'basketCurrency',
      'basketItems',
    ));

    // There should be at least one item in the cart for Ratepay method.
    $basket_items_count = 1;
    if ($request && !empty($request->basketItems)) {
      $basket_items_count = $request->basketItems;
    }
    // Add basket parameters in form of "basketItem(n)ArticleNumber".
    $basket_item_params = array(
      'ArticleNumber',
      'Description',
      'Quantity',
      'Tax',
      'UnitPrice',
    );
    for ($i = 1; $i <= $basket_items_count; $i++) {
      foreach ($basket_item_params as $basket_item_param) {
        $parameters[] = "basketItem{$i}{$basket_item_param}";
      }
    }

    return $parameters;
  }

}
