<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\SeamlessCheckoutInitRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize checkout.
 *
 * @see CheckoutInitResponse
 */
class CheckoutInitRequest extends RequestBase {

  /**
   * Computed string of the used modules.
   *
   * @var string
   */
  public $pluginVersion;

  /**
   * Unique ID of merchant.
   *
   * @var string
   */
  public $customerId;

  /**
   * Language for displayed texts on payment page.
   *
   * @var string
   */
  public $language;

  /**
   * Selected payment method of your consumer.
   *
   * @var string
   */
  public $paymentType;

  /**
   * Amount of payment.
   *
   * @var float
   */
  public $amount;

  /**
   * Unique description of the consumer's order in a human readable form.
   *
   * @var string
   */
  public $orderDescription;

  /**
   * Currency code of amount.
   *
   * @var string
   */
  public $currency;

  /**
   * URL of your online shop when checkout process was successful.
   *
   * @var string
   */
  public $successUrl;

  /**
   * URL of your online shop when checkout process has been cancelled.
   *
   * @var string
   */
  public $cancelUrl;

  /**
   * URL of your online shop when an error occurred within checkout process.
   *
   * @var string
   */
  public $failureUrl;

  /**
   * URL of your service page containing contact information.
   *
   * @var string
   */
  public $serviceUrl;

  /**
   * Return URL for outdated browsers.
   *
   * @var string
   */
  public $returnUrl;

  /**
   * URL of your online shop where Wirecard sends a server-to-server
   * confirmation.
   *
   * @var string
   */
  public $confirmUrl;

  /**
   * Ordered list of parameters used for calculating the fingerprint.
   *
   * @var string
   */
  public $requestFingerprintOrder;

  /**
   * Computed fingerprint of the parameter values as given in the
   * requestFingerprintOrder.
   *
   * @var string
   */
  public $requestFingerprint;

  /**
   * IP address of consumer.
   *
   * @var string
   */
  public $consumerIpAddress;

  /**
   * User-agent of browser of consumer.
   *
   * @var string
   */
  public $consumerUserAgent;

  /**
   * (optional) Window name of browser window where payment page is opened.
   *
   * @var string
   */
  public $windowName;

  /**
   * (optional) Unique reference of the data storage for a consumer.
   *
   * @var string
   */
  public $storageId;

  /**
   * (optional) Unique reference to the order of your consumer.
   *
   * @var int
   */
  public $orderIdent;

  /**
   * (optional) Unique ID of your online shop.
   *
   * @var string
   */
  public $shopId;

  /**
   * (optional) URL when result of checkout could not be determined yet.
   *
   * Alphanumeric with special characters.
   *
   * @var string
   */
  public $pendingUrl;

  /**
   * (optional) URL for information page regarding de-activated JavaScript.
   *
   * @var string
   */
  public $noScriptInfoUrl;

  /**
   * (optional) Order number of payment.
   *
   * @var int
   */
  public $orderNumber;

  /**
   * (optional) Check for duplicate requests done by your consumer.
   *
   * @var bool
   */
  public $duplicateRequestCheck;

  /**
   * (optional) Text displayed on invoice of financial institution.
   *
   * @var string
   */
  public $customerStatement;

  /**
   * (optional) Unique order reference ID sent from merchant to financial
   * institution.
   *
   * @var string
   */
  public $orderReference;

  /**
   * (optional) Transaction identifier.
   *
   * Possible values are SINGLE (for one-off transactions) or INITIAL (for the
   * first transaction of a series of recurring transactions).
   *
   * @var string
   */
  public $transactionIdentifier;

  /**
   * (optional) First name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingFirstname;

  /**
   * (optional) Last name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingLastname;

  /**
   * (optional) Billing address line 1 (name of street and house number).
   *
   * Alphanumeric with a variable length of up to 100 bytes.
   *
   * @var string
   */
  public $consumerBillingAddress1;

  /**
   * (optional) Billing address line 2
   *
   * Alphanumeric with a variable length of up to 100 bytes. Optionally the
   * house number if not already set within consumerBillingAddress1.
   *
   * @var string
   */
  public $consumerBillingAddress2;

  /**
   * (optional) Billing city.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingCity;

  /**
   * (optional) Billing state.
   *
   * @var string
   */
  public $consumerBillingState;

  /**
   * (optional) Billing country code (ISO 3166-1).
   *
   * Alphabetic with a fixed length of 2.
   *
   * @var string
   */
  public $consumerBillingCountry;

  /**
   * (optional) Billing zip code.
   *
   * Alphanumeric with a variable length of up to 12.
   *
   * @var string
   */
  public $consumerBillingZipCode;

  /**
   * (optional) E-mail address of consumer.
   *
   * Alphanumeric with special characters and a variable length of up to 256.
   *
   * @var string
   */
  public $consumerEmail;

  /**
   * (optional) Birth date of consumer in the format YYYY-MM-DD.
   *
   * @var string
   */
  public $consumerBirthDate;

  /**
   * (optional) Phone number of consumer.
   *
   * Numeric with special characters and a fixed length of 10.
   *
   * @var string
   */
  public $consumerBillingPhone;

  /**
   * (optional) Fax number of consumer.
   *
   * @var string
   */
  public $consumerBillingFax;

  /**
   * (optional) First name of consumer.
   *
   * @var string
   */
  public $consumerShippingFirstName;

  /**
   * (optional) Last name of consumer.
   *
   * @var string
   */
  public $consumerShippingLastName;

  /**
   * (optional) Shipping address line 1.
   *
   * @var string
   */
  public $consumerShippingAddress1;

  /**
   * (optional) Shipping address line 2.
   *
   * @var string
   */
  public $consumerShippingAddress2;

  /**
   * (optional) Shipping city.
   *
   * @var string
   */
  public $consumerShippingCity;

  /**
   * (optional) Shipping country code (ISO 3166-1).
   *
   * @var string
   */
  public $consumerShippingCountry;

  /**
   * (optional) Shipping state.
   *
   * @var string
   */
  public $consumerShippingState;

  /**
   * (optional) Shipping ZIP code.
   *
   * @var string
   */
  public $consumerShippingZipCode;

  /**
   * (optional) Shipping phone number.
   *
   * @var string
   */
  public $consumerShippingPhone;

  /**
   * (optional) Shipping fax number.
   *
   * @var string
   */
  public $consumerShippingFax;

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array(
      'customerId',
      'language',
      'paymentType',
      'amount',
      'orderDescription',
      'currency',
      'successUrl',
      'cancelUrl',
      'failureUrl',
      'serviceUrl',
      'returnUrl',
      'confirmUrl',
      'requestFingerprintOrder',
      'requestFingerprint',
      'consumerIpAddress',
      'consumerUserAgent',
    );
  }

  /**
   * Gets class name of checkout init request handler according to payment type.
   *
   * @param string $payment_type
   *   Payment type. E.g. Sepa, Ccard etc.
   *
   * @return string
   *   Class name.
   */
  public static function getClassName($payment_type) {
    $class = __NAMESPACE__ . '\CheckoutInit' . static::toCamelCase($payment_type) . 'Request';
    if (!class_exists($class)) {
      $class = __NAMESPACE__ . '\CheckoutInitRequest';
    }
    return $class;
  }

  /**
   * Creates request instance according to payment type.
   *
   * @param string $payment_type
   *   Payment type. E.g. Sepa, Ccard etc.
   * @param array $values
   *   (optional) Array with initial values to set for request parameters.
   * @param array $requestName
   *   (optional) The wirecard payment request name. By default payment type is
   *    used. Should be set only if the payment type is the same for few payment
   *    methods.
   *    @see WCPaymentControllerBase::getRequestName()
   *
   * @return static CheckoutInitRequest
   *   Request instance.
   *
   * @see initializeCheckout()
   */
  public static function create($payment_type, array $values = array(), $requestName = NULL) {
    if (!$requestName) {
      $requestName = $payment_type;
    }
    $values += array('paymentType' => $payment_type);
    $class = static::getClassName($requestName);
    return new $class($values);
  }

}
