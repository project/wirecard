<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitTrustlyRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize checkout for Trustly method.
 *
 * @see CheckoutResultResponse
 */
class CheckoutInitTrustlyRequest extends CheckoutInitRequest {

  /**
   * Last name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingLastname;

  /**
   * Billing country code (ISO 3166-1). Alphabetic with a fixed length of 2.
   *
   * @var string
   */
  public $consumerBillingCountry;

  /**
   * URL when result of checkout could not be determined yet.
   *
   * Alphanumeric with special characters.
   *
   * @var string
   */
  public $pendingUrl;

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array_merge(parent::getRequiredProperties(), array(
      'consumerBillingLastname',
      'consumerBillingCountry',
      'pendingUrl',
    ));
  }

}
