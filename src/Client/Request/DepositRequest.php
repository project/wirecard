<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\DepositRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to convert an approval into a deposit.
 *
 * This operation causes an approval (authorization) to be converted into a
 * deposit (captured payment).
 *
 * @see ApproveReversalResponse
 */
class DepositRequest extends RequestBase {

  /**
   * Unique ID of merchant.
   *
   * @var string
   */
  public $customerId;

  /**
   * (optional) Unique ID of your online shop.
   *
   * @var string
   */
  public $shopId = NULL;

  /**
   * Wirecard password for back-end operations.
   *
   * @var string
   */
  public $password;

  /**
   * Language code for returned texts and error messages.
   *
   * @var string
   */
  public $language;

  /**
   * Computed fingerprint of the parameter values and the secret.
   *
   * @var string
   */
  public $requestFingerprint;

  /**
   * Unique reference to the order of your consumer.
   *
   * @var int
   */
  public $orderNumber;

  /**
   * Amount of payment.
   *
   * @var float
   */
  public $amount;

  /**
   * Currency code of amount.
   *
   * @var string
   */
  public $currency;

  /**
   * {@inheritdoc}
   */
  public function getFingerPrintPropertyNames() {
    return array(
      'customerId',
      'shopId',
      'password',
      'secret',
      'language',
      'orderNumber',
      'amount',
      'currency',
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array(
      'customerId',
      'shopId',
      'password',
      'language',
      'requestFingerprint',
      'orderNumber',
      'amount',
      'currency',
    );
  }

}
