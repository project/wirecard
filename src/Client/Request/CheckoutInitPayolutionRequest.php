<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitPayolutionRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Base class for request to initialize checkout for Payolution method.
 *
 * @see CheckoutResultSuccessPayolutionResponse
 */
class CheckoutInitPayolutionRequest extends CheckoutInitRequest {

  /**
   * First name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingFirstname;

  /**
   * Last name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingLastname;

  /**
   * Billing address line 1 (name of street and house number).
   *
   * Alphanumeric with a variable length of up to 100 bytes.
   *
   * @var string
   */
  public $consumerBillingAddress1;

  /**
   * Billing address line 2
   *
   * Alphanumeric with a variable length of up to 100 bytes. Optionally the
   * house number if not already set within consumerBillingAddress1.
   *
   * @var string
   */
  public $consumerBillingAddress2;

  /**
   * Billing city. Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingCity;

  /**
   * Billing country code (ISO 3166-1). Alphabetic with a fixed length of 2.
   *
   * @var string
   */
  public $consumerBillingCountry;

  /**
   * Billing state.
   *
   * @var string
   */
  public $consumerBillingState;

  /**
   * Billing zip code. Alphanumeric with a variable length of up to 12.
   *
   * @var string
   */
  public $consumerBillingZipCode;

  /**
   * E-mail address of consumer.
   *
   * Alphanumeric with special characters and a variable length of up to 256.
   *
   * @var string
   */
  public $consumerEmail;

  /**
   * Birth date of consumer in the format YYYY-MM-DD.
   *
   * Numeric with special characters and a fixed length of 10.
   *
   * @var string
   */
  public $consumerBirthDate;

  /**
   * Name of company.
   *
   * (optional) This request parameter is required if you wish to handle an invoice for B2B
   * consumers.
   *
   * @var string
   */
  public $companyName;

  /**
   * (optional) VAT ID of company.
   *
   * @var string
   */
  public $companyVatId;

  /**
   * (optional) Trade registration number of company.
   */
  public $companyTradeRegistryNumber;

  /**
   * (optional) Additional or alternative register key of company.
   *
   * @var string
   */
  public $companyRegisterKey;

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array_merge(parent::getRequiredProperties(), array(
      'consumerBillingFirstname',
      'consumerBillingLastname',
      'consumerBillingAddress1',
      'consumerBillingAddress2',
      'consumerBillingCity',
      'consumerBillingCountry',
      'consumerBillingZipCode',
      'consumerEmail',
      'consumerBirthDate',
    ));
  }

}
