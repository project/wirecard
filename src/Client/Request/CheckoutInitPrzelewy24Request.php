<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitPrzelewy24Request
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize checkout for Przelewy24 method.
 *
 * @see CheckoutResultResponse
 */
class CheckoutInitPrzelewy24Request extends CheckoutInitRequest {

  /**
   * E-mail address of consumer.
   *
   * Alphanumeric with special characters and a variable length of up to 256.
   *
   * @var string
   */
  public $consumerEmail;

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array_merge(parent::getRequiredProperties(), array('consumerEmail'));
  }

}
