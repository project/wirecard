<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitSkrillDigitalWalletRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize checkout for Skrill Digital Wallet.
 *
 * @see CheckoutResultResponse
 */
class CheckoutInitSkrillDigitalWalletRequest extends CheckoutInitRequest {

  /**
   * First name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingFirstname;

  /**
   * Last name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingLastname;

  /**
   * E-mail address of consumer.
   *
   * Alphanumeric with special characters and a variable length of up to 256.
   *
   * @var string
   */
  public $consumerEmail;

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array_merge(parent::getRequiredProperties(), array(
      'consumerBillingFirstname',
      'consumerBillingLastname',
      'consumerEmail',
    ));
  }

}
