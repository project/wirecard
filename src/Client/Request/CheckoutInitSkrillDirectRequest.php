<?php
/**
 * @file
 * Contains \Drupal\wirecard\Client\Request\CheckoutInitSkrillDirectRequest
 */

namespace Drupal\wirecard\Client\Request;

/**
 * Represents the request to initialize checkout for Skrill Direct method.
 *
 * @see CheckoutResultResponse
 */
class CheckoutInitSkrillDirectRequest extends CheckoutInitRequest {

  /**
   * First name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingFirstname;

  /**
   * Last name of consumer.
   *
   * Alphanumeric with a variable length of up to 32 bytes.
   *
   * @var string
   */
  public $consumerBillingLastname;

  /**
   * {@inheritdoc}
   */
  public static function getRequiredProperties(RequestBase $request = NULL) {
    return array_merge(parent::getRequiredProperties(), array(
      'consumerBillingFirstname',
      'consumerBillingLastname',
    ));
  }

}
