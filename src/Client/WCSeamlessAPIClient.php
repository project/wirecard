<?php
/**
 * @file
 * Wirecard payment seamless api client class.
 */

namespace Drupal\wirecard\Client;

use Drupal\wirecard\Client\Exception\RequestException;
use Drupal\wirecard\Client\Request\ApproveReversalRequest;
use Drupal\wirecard\Client\Request\DepositRequest;
use Drupal\wirecard\Client\Request\CheckoutInitRequest;
use Drupal\wirecard\Client\Request\DataStorageInitRequest;
use Drupal\wirecard\Client\Request\RequestBase;
use Drupal\wirecard\Client\Response\ApproveReversalResponse;
use Drupal\wirecard\Client\Response\DepositResponse;
use Drupal\wirecard\Client\Response\CheckoutInitResponse;
use Drupal\wirecard\Client\Response\CheckoutResult\CheckoutResultResponse;
use Drupal\wirecard\Client\Response\DataStorageInitResponse;
use Drupal\wirecard\Util\FingerPrint;
use Drupal\wirecard\Util\WirecardLogger;

/**
 * Wirecard payment API client.
 */
class WCSeamlessAPIClient {

  /**
   * Session variable name for storing the id of the Wirecard data storage.
   */
  const WIRECARD_STORAGE_ID = 'WirecardStorageId';

  /**
   * Session variable name for storing the unique identification for order.
   */
  const WIRECARD_ORDER_INDENT = 'WirecardOrderIndent';

  /**
   * The client config.
   *
   * @var WCClientConfig
   */
  protected $config;

  /**
   * The Wirecard logger.
   *
   * @var WirecardLogger
   */
  protected $logger;

  protected $urlWirecardCheckout = "https://checkout.wirecard.com";

  /**
   * Initializes client object.
   *
   * @param WCClientConfig $config
   *   Wirecard client configuration.
   * @param WirecardLogger $logger
   *   The logger to use.
   */
  public function __construct(WCClientConfig $config, WirecardLogger $logger) {
    $this->config = $config;
    $this->logger = $logger;
    // URLs for accessing the Wirecard Checkout Platform.
    $this->urlDatastorageRead = $this->urlWirecardCheckout . "/seamless/dataStorage/read";
  }

  /**
   * Initializes the data storage.
   *
   * @param DataStorageInitRequest $request
   *   Request to init data storage.
   *
   * @return DataStorageInitResponse
   *   Response from data storage init request.
   *
   * @throws \Drupal\wirecard\Client\Exception\RequestException
   * @throws \Drupal\wirecard\Client\Exception\WCErrorException
   */
  public function initializeDataStorage(DataStorageInitRequest $request) {
    $request->requestFingerprint = $this->calculateFingerPrint($request);

    $response = $this->request(
      $this->urlWirecardCheckout . "/seamless/dataStorage/init",
      'POST',
      $request->toArray()
    );

    return DataStorageInitResponse::parse($response);
  }

  /**
   * Initializes the checkout process.
   *
   * @param CheckoutInitRequest $request
   *   Request to initialize Wirecard checkout.
   *
   * @return CheckoutInitResponse
   *   Response from the Wirecard checkout.
   *
   * @throws \Drupal\wirecard\Client\Exception\RequestException
   * @throws \Drupal\wirecard\Client\Exception\WCErrorException
   */
  public function initializeCheckout(CheckoutInitRequest $request) {
    $request->requestFingerprintOrder = implode(',', $request->getFingerPrintPropertyNames());
    $request->requestFingerprint = $this->calculateFingerPrint($request);

    $response = $this->request(
      $this->urlWirecardCheckout . "/seamless/frontend/init",
      'POST',
      $request->toArray()
    );

    return CheckoutInitResponse::parse($response);
  }

  /**
   * Handles the POST request and parses the passed data.
   *
   * @return CheckoutResultResponse
   *
   * @throws RequestException
   */
  public function handleConfirmation() {
    // We use raw POST because PHP does not handle dots (.) in arrays.
    $raw_post = stream_get_contents(fopen('php://input', 'r'));
    $data = $this->unserialize($raw_post);
    $this->logger->debug('Confirmation request content: %content', array('%content' => var_export($data, TRUE)));
    return CheckoutResultResponse::parse($data);
  }

  /**
   * Causes an approval to be converted into a deposit.
   *
   * This operation causes an approval (authorization) to be converted into a
   * deposit (captured payment). This operation is executed by you if an order
   * of a consumer has been accepted.
   *
   * @param int $order_number
   *   The unique order number, as sent to Wirecard before.
   * @param int $amount
   *   Deposited amount.
   * @param string $currency
   *   Currency code of amount.
   *
   * @return DepositResponse
   *   URL to the Wirecard checkout.
   *
   * @throws \Drupal\wirecard\Client\Exception\WCBackendOperationException
   * @throws \Drupal\wirecard\Client\Exception\RequestException
   * @throws \Drupal\wirecard\Client\Exception\WCErrorException
   */
  public function deposit($order_number, $amount, $currency) {
    $request = new DepositRequest(
      array(
        'customerId' => $this->config->customerId,
        'shopId' => $this->config->shopId,
        'password' => $this->config->password,
        'language' => $this->config->language,
        'orderNumber' => $order_number,
        'amount' => $amount,
        'currency' => $currency,
      )
    );

    $request->requestFingerprint = $this->calculateFingerPrint($request);

    $response = $this->request(
      $this->urlWirecardCheckout . "/seamless/backend/deposit",
      'POST',
      $request->toArray()
    );

    return DepositResponse::parse($response);
  }

  /**
   * Cancels payment approval.
   *
   * This can only be done when the approval has not yet been debited and is
   * supported by the financial service provider.
   *
   * @param int $order_number
   *   The unique order number, as sent to Wirecard before.
   *
   * @return ApproveReversalResponse
   *   URL to the Wirecard checkout.
   *
   * @throws \Drupal\wirecard\Client\Exception\WCBackendOperationException
   * @throws \Drupal\wirecard\Client\Exception\RequestException
   * @throws \Drupal\wirecard\Client\Exception\WCErrorException
   */
  public function approveReversal($order_number) {
    $request = new ApproveReversalRequest(
      array(
        'customerId' => $this->config->customerId,
        'shopId' => $this->config->shopId,
        'password' => $this->config->password,
        'language' => $this->config->language,
        'orderNumber' => $order_number,
      )
    );
    $request->requestFingerprint = $this->calculateFingerPrint($request);

    $response = $this->request(
      $this->urlWirecardCheckout . "/seamless/backend/approveReversal",
      'POST',
      $request->toArray()
    );

    return ApproveReversalResponse::parse($response);
  }

  /**
   * Calculates the fingerprint.
   *
   * @param RequestBase $request
   *   The request object for which to calculate the fingerprint.
   *
   * @return string
   *   The calculated fingerprint.
   */
  protected function calculateFingerPrint(RequestBase $request) {
    return FingerPrint::calculateFingerPrint($request->getFingerprintPropertyNames(), $request, $this->getConfig());
  }

  /**
   * Sends a request to the Wirecard Checkout Platform.
   *
   * A thin wrapper around an arbitrary HTTP client.
   *
   * @throws RequestException
   *
   * @return array
   *   The unserialiazed/decoded response data.
   *
   * @see drupal_http_request()
   */
  protected function request($url, $method = 'GET', $data = NULL, $options = array()) {
    $options += array(
      'method' => $method,
      'data' => !empty($data) ? drupal_http_build_query($data) : '',
      'headers' => array(),
    );
    if ($method == 'POST') {
      $options['headers'] += array(
        'Content-Type' => 'application/x-www-form-urlencoded',
      );
    }

    $response = drupal_http_request($url, $options);
    if ($this->hasError($response)) {
      throw RequestException::create($response->code, $response->error, $response);
    }
    return $this->unserialize($response->data);
  }

  /**
   * Helper to determine whether the drupal_http_request() had errors.
   *
   * @param stdclass $request
   *   The request object as returned by drupal_http_request().
   *
   * @return bool
   */
  protected function hasError($request) {
    // Status codes 1XX, 2XX, 3XX are ok, highers not.
    // Unfortunately, drupal_http_request() misbehaves with 201 and does not
    // set $request->error as documented.
    $code = floor($request->code / 100) * 100;
    return $code >= 400;
  }

  /**
   * Unserializes the given request data.
   *
   * @param string $data
   *   The data to decode.
   *
   * @return array
   *   The decoded data.
   */
  protected function unserialize($data) {
    // Like drupal_get_query_array() but with real urldecode().
    $result = array();
    foreach (explode('&', $data) as $param) {
      $param = explode('=', $param, 2);
      $result[$param[0]] = isset($param[1]) ? decode_entities(urldecode($param[1])) : '';
    }
    return $result;
  }

  /**
   * Returns the configuration used.
   *
   * @return \Drupal\wirecard\Client\WCClientConfig
   *   The configuration object used.
   */
  public function getConfig() {
    return $this->config;
  }

}