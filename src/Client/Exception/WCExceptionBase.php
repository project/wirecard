<?php
/**
 * @file
 * Contains WCExceptionBase.
 */

namespace Drupal\wirecard\Client\Exception;

/**
 * A common exception base class.
 */
abstract class WCExceptionBase extends \Exception {

  /**
   * Returns the log message.
   *
   * @return string
   */
  public function getLogMessage() {
    return $this->getMessage();
  }

  /**
   * Returns the log context.
   *
   * @return array
   */
  public function getLogContext() {
    return array();
  }

}
