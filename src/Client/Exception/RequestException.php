<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Exception\RequestException
 */

namespace Drupal\wirecard\Client\Exception;

/**
 * A custom exception class for handling HTTP request exceptions.
 */
class RequestException extends WCExceptionBase {

  /**
   * The status code returned.
   *
   * @var int
   */
  public $code;

  /**
   * The request response.
   *
   * @var mixed
   */
  public $response;

  /**
   * Creates a new exception object.
   *
   * @param int $code
   *   The status code.
   * @param string $message
   *   The response message.
   * @param stdclass $response
   *   The drupal_http_request result object.
   *
   * @return static
   */
  public static function create($code, $message, $response) {
    $exception = new static($message);
    $exception->response = $response;
    $exception->code = $code;
    return $exception;
  }

  /**
   * Returns the log message.
   *
   * @return string
   */
  public function getLogMessage() {
    return 'HTTP error %code: @message';
  }

  /**
   * Returns the log context.
   *
   * @return array
   */
  public function getLogContext() {
    return array(
      '@message' => $this->getMessage(),
      '%code' => $this->code
    );
  }

}
