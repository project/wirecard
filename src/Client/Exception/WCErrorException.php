<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Exception\WCErrorException
 */

namespace Drupal\wirecard\Client\Exception;

/**
 * Handles Wirecard API errors.
 */
class WCErrorException extends WCExceptionBase {

  /**
   * The errors of the repsonse.
   *
   * @var \Drupal\wirecard\Client\Response\ResponseError[]
   */
  public $errors = array();

  /**
   * Constructs the object.
   *
   * @param \Drupal\wirecard\Client\Response\ResponseError[] $errors
   *   The errors.
   */
  public function __construct(array $errors) {
    $this->errors = $errors;
    parent::__construct($this->__toString());
  }

  /**
   * Generates the exception message.
   *
   * @return string
   */
  public function __toString() {
    $msg = array();
    foreach ($this->errors as $error) {
      $msg[] = $error->consumerMessage;
    }
    return implode("\n", $msg);
  }

  /**
   * Returns the log message.
   *
   * @return string
   */
  public function getLogMessage() {
    return $this->__toString();
  }

}
