<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Exception\ResponseException
 */

namespace Drupal\wirecard\Client\Exception;

/**
 * A custom exception class for handling HTTP responses.
 *
 * That class handles responses that are not triggered by a request (i.e.
 * remotely triggered).
 */
class ResponseException extends WCExceptionBase {

  /**
   * The request response.
   *
   * @var mixed
   */
  public $response;

  /**
   * Creates a new exception object.
   *
   * @param string $message
   *   The response message.
   * @param stdClass $response
   *   The drupal_http_request result object.
   *
   * @return static
   */
  public static function create($message, $response) {
    $exception = new static($message);
    $exception->response = $response;
    return $exception;
  }

  /**
   * Returns the log message.
   *
   * @return string
   */
  public function getLogMessage() {
    return 'HTTP repsonse error: @message. Response: %response';
  }

  /**
   * Returns the log context.
   *
   * @return array
   */
  public function getLogContext() {
    return array(
      '@message' => $this->getMessage(),
      '%response' => var_export($this->response, TRUE),
    );
  }

}
