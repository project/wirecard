<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Exception\GeneralException
 */

namespace Drupal\wirecard\Client\Exception;

/**
 * Handles Wirecard API errors.
 */
class GeneralException extends WCExceptionBase {}
