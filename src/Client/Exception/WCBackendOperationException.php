<?php

/**
 * @file
 * Contains \Drupal\wirecard\Client\Exception\RequestBackendOperationException
 */

namespace Drupal\wirecard\Client\Exception;

/**
 * Custom exception class for Wirecard back-end operations response exceptions.
 */
class WCBackendOperationException extends WCExceptionBase {

  /**
   * Request parameter verification error code.
   *
   * @var string
   */
  const REQUEST_PARAMETER_VERIFICATION_ERROR = '1';

  /**
   * Execution of operation denied error code.
   *
   * @var string
   */
  const EXECUTION_OD_OPERATION_DENIED_ERROR = '2';

  /**
   * External financial institution error code.
   *
   * @var string
   */
  const EXTERNAL_FINANCIAL_INSTITUTION_ERROR = '3';

  /**
   * Internal error error code.
   *
   * @var string
   */
  const INTERNAL_ERROR = '4';

  /**
   * Status is undefined error code.
   *
   * @var string
   */
  const STATUS_UNKNOWN_ERROR = '5';

  /**
   * The status code returned.
   *
   * @var int
   */
  public $code;

  /**
   * The request response.
   *
   * @var mixed
   */
  public $response;

  /**
   * Creates a new exception object.
   *
   * @param int $code
   *   The status code.
   * @param array $response
   *   The wirecard back-end operation result array.
   *
   * @return static
   */
  public static function create($code, $response) {
    switch ($code) {
      case self::REQUEST_PARAMETER_VERIFICATION_ERROR:
        $message = 'Error during request parameter verification.';
        break;
      case self::EXECUTION_OD_OPERATION_DENIED_ERROR:
        $message = 'Execution of operation denied.';
        break;
      case self::EXTERNAL_FINANCIAL_INSTITUTION_ERROR:
        $message = 'Error within external financial institution.';
        break;
      case self::INTERNAL_ERROR:
        $message = 'Internal error.';
        break;
      case self::STATUS_UNKNOWN_ERROR:
        $message = 'Response status is undefined.';
        break;
      default:
        $message = 'Unknown error.';
        break;
    }
    $exception = new static($message);
    $exception->result = $response;
    $exception->code = $code;
    return $exception;
  }

  /**
   * Returns the log message.
   *
   * @return string
   */
  public function getLogMessage() {
    return 'Wirecard back-end operations error %code: @message';
  }

  /**
   * Returns the log context.
   *
   * @return array
   */
  public function getLogContext() {
    return array(
      '@message' => $this->getMessage(),
      '%code' => $this->code
    );
  }

}
