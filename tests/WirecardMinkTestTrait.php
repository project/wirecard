<?php

/**
 * @file
 * Contains \WirecardMinkTestTrait.
 */

/**
 * Trait for using mink with the DrupalWebTestCase.
 */
trait WirecardMinkTestTrait {

  /**
   * Default configuration to use for mink.
   *
   * Override by setting the variable before initialize mink.
   *
   * @var array
   */
  protected $minkConfiguration = array(
    'browser' => 'phantomJS',
    'drupalBaseUrl' => 'http://localhost',
    'webdriver' => 'http://localhost:4444/wd/hub',
  );

  /**
   * Sets up mink with phantomjs.
   */
  protected function initMink() {
    $driver = new \Behat\Mink\Driver\Selenium2Driver($this->minkConfiguration['browser'], NULL, $this->minkConfiguration['webdriver']);
    $session = new \Behat\Mink\Session($driver);
    $this->mink = new \Behat\Mink\Mink();
    $this->mink->registerSession('phantomjs', $session);
    $this->mink->setDefaultSessionName('phantomjs');

    // Make sure it works and access a first page. That also makes sure we
    // can set a cookie later on.
    $this->mink->getSession()->visit($this->minkConfiguration['drupalBaseUrl']);
    $html = $this->mink->getSession()
      ->getPage()
      ->getHtml();

    if (strpos($html, 'Drupal') === FALSE) {
      throw new \Behat\Mink\Exception\ResponseTextException("Cannot access Drupal - have you adjusted the mink configuration?");
    }
  }

  /**
   * Retrieves a Drupal path or an absolute path.
   *
   * @param string $path
   *   Drupal path or URL to load into Mink controlled browser.
   * @param array $options
   *   (optional) Options to be forwarded to the url generator.
   * @param array $headers
   *   (optional) @todo: Implement.
   *
   * @return string
   *   The retrieved HTML string.
   */
  protected function drupalGet($path, array $options = array(), array $headers = array()) {

    $url = $this->minkConfiguration['drupalBaseUrl'] . "/" . url($path, $options);
    $session = $this->getSession();
    $this->prepareRequest($session);

    $session->visit($url);
    $out = $session->getPage()->getContent();

    $this->verbose('GET request to: ' . $path .
      '<hr />Ending URL: ' . $out .
      '<hr />' . $out);

    return $out;
  }

  /**
   * Adds debug output of the current page.
   */
  protected function debugPage() {
    $out = $this->getSession()->getPage()->getContent();
    $this->verbose('Debug request at: ' . $this->getSession()->getCurrentUrl() .
      '<hr />' . $out);
  }

  /**
   * Prepare for a request to testing site.
   *
   * The testing site is protected via a SIMPLETEST_USER_AGENT cookie that is
   * checked by drupal_valid_test_ua().
   *
   * @see drupal_valid_test_ua()
   */
  protected function prepareRequest(\Behat\Mink\Session $session) {
    // As selenium does not support custom user agents, set a custom cookie
    // instead.
    if (preg_match('/simpletest\d+/', $this->databasePrefix, $matches)) {
      $session->setCookie('SIMPLETEST_USER_AGENT', str_replace(';', '--', drupal_generate_test_ua($matches[0])));
    }
  }

  /**
   * Returns Mink session.
   *
   * @param string $name
   *   (optional) Name of the session. Defaults to the active session.
   *
   * @return \Behat\Mink\Session
   *   The active Mink session object.
   */
  public function getSession($name = NULL) {
    return $this->mink->getSession($name);
  }

  /**
   * Returns WebAssert object.
   *
   * @param string $name
   *   (optional) Name of the session. Defaults to the active session.
   *
   * @return \Behat\Mink\WebAssert
   *   A new web-assert option for asserting the presence of elements with.
   */
  public function assertSession($name = NULL) {
    return new \Behat\Mink\WebAssert($this->getSession($name));
  }

  /**
   * Asserts the given text is on the current page.
   */
  protected function assertText($text, $message = '', $group = 'Other') {
    $this->assertSession()->responseContains($text);
    $this->pass($message ? $message : format_string('Text %text found.', array('%text' => $text)));
  }

  /**
   * Checks that specific button exists on the current page.
   *
   * @param string $button
   *   One of id|name|label|value for the button.
   * @param \Behat\Mink\Element\TraversableElement $container
   *   (optional) The document to check against. Defaults to the current page.
   *
   * @return \Behat\Mink\Element\NodeElement
   *   The matching element.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   *   When the element doesn't exist.
   */
  public function buttonExists($button, TraversableElement $container = NULL) {
    $container = $container ?: $this->getSession()->getPage();
    $node = $container->findButton($button);

    if ($node === NULL) {
      throw new ElementNotFoundException($this->getSession(), 'button', 'id|name|label|value', $button);
    }

    return $node;
  }

  /**
   * Checks that specific select field exists on the current page.
   *
   * @param string $select
   *   One of id|name|label|value for the select field.
   * @param \Behat\Mink\Element\TraversableElement $container
   *   (optional) The document to check against. Defaults to the current page.
   *
   * @return \Behat\Mink\Element\NodeElement
   *   The matching element
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   *   When the element doesn't exist.
   */
  public function selectExists($select, TraversableElement $container = NULL) {
    $container = $container ?: $this->getSession()->getPage();
    $node = $container->find('named', array(
      'select',
      $this->getSession()->getSelectorsHandler()->xpathLiteral($select),
    ));

    if ($node === NULL) {
      throw new ElementNotFoundException($this->getSession(), 'select', 'id|name|label|value', $select);
    }

    return $node;
  }

}