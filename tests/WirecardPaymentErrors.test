<?php

/**
 * @file
 * Contains class WirecardPaymentErrors.
 */

/**
 * Tests payment execution.
 */
class WirecardPaymentErrors extends WirecardPaymentTestBase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'description' => '',
      'name' => 'Wirecard Payment Errors',
      'group' => 'Wirecard',
    );
  }
  /**
   * Tests error handling of Wirecard integration using SEPA.
   */
  public function testErrorHandling() {
    $this->createSepaPaymentMethod();
    $this->drupalGet('node/' . $this->node->nid);
    $this->getSession()->wait(3000, "jQuery('#wirecard-payment-bic').length > 0");

    // Trigger an error caught by JS.
    $this->getSession()->getPage()->fillField('wirecard-payment-bic', 'too-short');
    $this->buttonExists(t('Pay'))->press();
    $this->getSession()->wait(3000, "jQuery('#wirecard-payment-form .error').length > 0");
    $this->debugPage();
    $this->assertText('BIC is invalid');

    // Make sure payment afterwards still works.
    $this->getSession()->getPage()->fillField('wirecard-payment-bic', 'RLNWATWWBRL');
    $this->pay();
    $this->assertText(t('Your payment was successfully completed.'));

    // Trigger an wirecard error by paying for amount "0".
    $this->createPaymentNode(0);
    $this->drupalGet('node/' . $this->node->nid);
    $this->pay();

    $this->assertText('Amount is invalid.');
    $this->assertText(t('Your payment failed.'));
  }
}
