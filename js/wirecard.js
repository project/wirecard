(function($) {

  // Name of selected payment type.
  var paymentType = '';
  // Payment form.
  var paymentForm;
  // Indicates whether PCI3 DSS SAQ-A mode is enabled.
  var pci3IsEnabled;
  // Indicates success of wirecard data storage:
  //  - null: request was not sent.
  //  - false: request is with errors.
  //  - true: request is without errors.
  var success = null;
  // Wirecard data storage.
  var dataStorage = null;

  /**
   * Enable the payment method select element.
   */
  Drupal.behaviors.wirecard = {
    attach: function(context, settings) {
      settings.wirecardMethodData = settings.wirecardMethodData || {};
      if (settings.wirecardMethodData.methodName === undefined) {
        // Ignore payment methods which do not require data storage.
        paymentType = '';
        return;
      }
      paymentType = settings.wirecardMethodData.methodName;

      // Check if PCI3 DSS SAQ-A mode is enabled.
      pci3IsEnabled = settings.wirecardMethodData[paymentType] !== undefined
        && settings.wirecardMethodData[paymentType].pci3DssSaqEnable !== undefined
        && settings.wirecardMethodData[paymentType].pci3DssSaqEnable == true;

      // Dynamically load javascript from Wirecard.
      $.getScript(settings.wirecardScriptSrc, function(data, textStatus, jqxhr) {
        if (pci3IsEnabled) {
          // Embed credit card form from Wirecard.
          $('#' + settings.wirecardMethodData[paymentType].containerId).once(function () {
            dataStorage = new WirecardCEE_DataStorage();
            var containerId = settings.wirecardMethodData[paymentType].containerId;
            dataStorage.buildIframeCreditCard(containerId, '100%', '150px');
          });
        }
      });

      paymentForm = $('#wirecard-payment-form').closest('form');
      var paymentSubmitSelector = 'input:submit:visible:not(#edit-cancel)';
      // Get payment button selector from settings if it is set.
      if (settings.wirecardPaymentSubmitSelector !== undefined) {
        paymentSubmitSelector = settings.wirecardPaymentSubmitSelector;
      }
      var paymentSubmit = paymentForm.find(paymentSubmitSelector).first();

      // Process checkout submit.
      paymentSubmit.once('wirecard').click(function(e){
        // Ignore payment methods which do not require data storage.
        if (!paymentType) {
          return true;
        }
        // Validates payment data before submission.
        if (payment_validate() === false) {
          // Validation failed. Stop all possible event handling.
          e.preventDefault();
          e.stopPropagation();
          e.stopImmediatePropagation();
          return false;
        }
        // Handle Wirecard success response.
        if (success === true) {
          return true;
        }
        // Wirecard response is not yet received.
        if (success === null) {
          // Dynamically load javascript from Wirecard.
          $.getScript(settings.wirecardScriptSrc, function(data, textStatus, jqxhr) {
            // Store sensitive data to the Wirecard data storage.
            storeData(paymentType);
            // Set a timer and wait for the Wirecard response.
            setTimeout(function(){
              paymentSubmit.click();
              success = null;
            }, 500);
          });
        }
        // Wirecard response isn't successful. Stop all possible event handling.
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
      });

      /**
       * Stores sensitive data to the Wirecard data storage.
       *
       * @param {string} aPaymentType
       *   Payment type string.
       */
      function storeData(aPaymentType) {
        // If pci3 is enabled, dataStorage was already initialized before.
        if (!pci3IsEnabled) {
          // Create a new JavaScript object containing the Wirecard data storage
          // functionality.
          dataStorage = new WirecardCEE_DataStorage();
        }
        // Initialize the JavaScript object containing the payment specific
        // information and data.
        var paymentInformation = {};

        // Collect payment specific information and data.
        var selectors = settings.wirecardMethodSelectors;
        for(var key in selectors) {
          if (selectors.hasOwnProperty(key)) {
            // We need to check if element exists. The 'selectors' array can
            // contain elements from other payment methods if payment method is
            // switched via ajax call. Javascript settings are always merged on
            // ajax request.
            // @see ajax_render()
            if (document.getElementById(selectors[key])) {
              paymentInformation[key] = document.getElementById(selectors[key]).value;
            }
          }
        }

        // Store sensitive data to the Wirecard data storage.
        switch (aPaymentType) {
          case 'CCARD':
          case 'CCARD-MOTO':
            if (pci3IsEnabled) {
              // For pci3 payment information is stored on Wirecard side.
              paymentInformation = null;
            }
            dataStorage.storeCreditCardInformation(paymentInformation, callbackFunction);
            break;
          case 'SEPA-DD':
            dataStorage.storeSepaDdInformation(paymentInformation, callbackFunction);
            break;
          case 'GIROPAY':
            dataStorage.storeGiropayInformation(paymentInformation, callbackFunction);
            break;
          case 'PBX':
            dataStorage.storePayboxInformation(paymentInformation, callbackFunction);
            break;
          case 'VOUCHER':
            dataStorage.storeVoucherInformation(paymentInformation, callbackFunction);
            break;
          default:
            callbackFunction(null);
            break;
        }
      }

      /**
       * Validates payment data before submission.
       *
       * @returns {boolean}
       *   True if validation was successful. Otherwise false.
       */
      function payment_validate() {
        // Collect payment specific information and data.
        var errors = [], selectors = settings.wirecardMethodSelectors;
        for (var key in selectors) {
          // Validate IBAN number.
          if (key == 'bankAccountIban') {
            if (selectors.hasOwnProperty(key) && document.getElementById(selectors[key])) {
              if (document.getElementById(selectors[key]).value === '') {
                errors.push(Drupal.t('!name field is required.', {'!name': 'IBAN'}));
                continue;
              }
              var ibanField = document.getElementById(selectors[key]);
              // Trim and remove spaces first.
              var iban = $.trim(ibanField.value);
              iban = iban.replace(/\s+/g, '');
              // Change the IBAN to uppercase letters.
              iban = iban.toUpperCase();
              ibanField.value = iban;

              // First two symbols should be characters. All other symbols must
              // be numbers.
              if (!iban.match(/^[a-zA-Z]{2}\d+$/g)) {
                errors.push(Drupal.t('!name is invalid', {'!name': 'IBAN'}));
                continue;
              }

              var exactLength;
              var countryCode = iban.substring(0, 2);
              switch (countryCode) {
                case 'DE':
                  exactLength = 22;
                  break;
                case 'AT':
                  exactLength = 20;
                  break;
              }
              // Validate IBAN length.
              if (exactLength) {
                if (ibanField.value.length != exactLength) {
                  errors.push(Drupal.t('!name is invalid', {'!name': 'IBAN'}));
                }
              }
              // For all the other cases IBAN should be no longer than 34.
              else if (ibanField.value.length > 34) {
                errors.push(Drupal.t('!name is invalid', {'!name': 'IBAN'}));
              }
            }
          }
        }
        if (errors.length > 0) {
          renderErrors(errors);
          return false;
        }
        return true;
      }

      /**
       * Callback function; indicates success of wirecard data storage.
       *
       * Displays the errors of storing the sensitive data to the Wirecard data
       * storage. Handles the 'success' flag.
       *
       * @param {object} response
       *   Wirecaed response.
       */
      function callbackFunction(response) {
        success = true;
        // Check if response status is without errors.
        if (response === undefined || response.getStatus() !== 0) {
          success = false;
          // Collects all occurred errors and adds them to the result string.
          var errors = response.getErrors();
          var message = [];
          for (var e in errors) {
            if (errors[e].consumerMessage !== undefined) {
              message.push(errors[e].consumerMessage);
            }
            else {
              message.push(errors[e].message);
            }
          }
          renderErrors(message);
        }
      }

      /**
       * Renders errors in drupal_set_message way.
       *
       * @param {string[]} errors
       *   Strings array with error messages.
       */
      function renderErrors(errors) {
        var message = errors.join("\n");
        // Presents errors on the page.
        var messagesBox;
        if (paymentForm.find('.messages').length !== 0) {
          messagesBox = paymentForm.find('.messages');
        }
        else {
          messagesBox = $('<div/>', {
            'class': 'messages error'
          }).prependTo('#wirecard-payment-form');
        }
        messagesBox.html(message);
      }
    }
  }
})(jQuery);
